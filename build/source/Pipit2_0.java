import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.awt.Rectangle; 
import java.util.HashSet; 
import java.util.Iterator; 
import java.util.Map; 
import java.util.Collections; 
import java.util.Arrays; 
import processing.pdf.*; 
import processing.opengl.*; 

import de.looksgood.ani.*; 
import de.looksgood.ani.easing.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Pipit2_0 extends PApplet {









// import controlP5.*;

static final int _WIDTH = 1200;
static final int _HEIGHT = 700;

String[] _H_CHROMOSOMES = {"chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14",
        "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrx", "chry"};
String[] _M_CHROMOSOMES = {"chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14",
        "chr15", "chr16", "chr17", "chr18", "chr19", "chrx", "chry", "chrm"};    
String[] _CHROMOSOMES;
String[] _H_EVENTS = {"deletion","inversion","tandem_duplication", "copy_number_gain", "copy_number_variation","complex_structural_alteration"}; //,"insertion",  "mobile_element_insertion"};
String[] _M_EVENTS = {"deletion","inversion","tandem_duplication", "copy_number_gain", "copy_number_variation","complex_structural_alteration"}; //,"insertion",  "mobile_element_insertion"};
String[] _EXCLUDED_EVENTS = {"insertion",  "mobile_element_insertion"};
String[] _EVENTS;
ArrayList<String> _FOUND_EVENTS;
boolean[] _VALID_EVENT;
boolean _show_multi_event = true;
// String mouseFile = "estd185_Yalcin_et_al_2012.2012-08-30.MGSCv37.gvf";//"mouseData.gvf";
String mouseFile = "mouseData.gvf";
File _DATA_FILE;
String _CYTOBAND_FILE;
String _GENETRACK_FILE;
String _GO_FILE;

int _PROMOTER_LENGTH = 10; // basepair length of promoter
boolean _ONLY_EXOMES = true; //consider only the structural variation that involves Exomes, and located within a gene track

int _VIEW_MODE = 0;   //0= GeneBlock, 1=UnitPlot, 2=BpView, 3 =Expanded, 4=UnitPlot2
String[] _VIEW_MODE_LABEL ={"Collapsed", "Expanded", "Chromosome", "Unit"};
Rectangle[] _VIEW_MODE_RECT = null;
int _C_CHR_INDEX = 0;

String _LINK;
String _SPECIES;


DataSet data;
PFont font;
PShape full_circle, right_circle, left_circle, middle_circle;
// PGraphics topView, bottomView;
PGraphics geneView;
PGraphics legendView;

ArrayList<Gene> _selected_genes;
HashSet<Go> _selected_goTerms = new HashSet<Go>(); /// 
Go _selected_go = null; //searching gene from go

String typedString = "";

//genes selected by go terms
ArrayList<Gene> _selected_genes_by_go;


//optional
ArrayList<Gene> selected_genes_by_category;

//Animation
AniSequence animation;
//saving PDF
boolean saveFrame = false;

//scrollbar interaction
boolean isScrolling = false;
int scrolling_y  = 0;
int beforeScrollIndex = 0;

//flags
boolean isLoadingData = false; // while loading data
boolean dataLoaded = false; //check if the data is loaded
boolean hasOptionalTable = false;//whether user is loading external csv

boolean isTestMode = false;


public void setup(){
    // println("setup() ---------");
	// size(_WIDTH, _HEIGHT, OPENGL);
    size(_WIDTH, _HEIGHT);
	font = createFont("Monospaced", 10);
	textFont(font);
    smooth();
    frameRate(30);
    frame.setIconImage( getToolkit().getImage("sketch.icns") );

    //Animation
    Ani.init(this);
    animation = new AniSequence(this);
    animation_geneView = new AniSequence(this);
    animation_legendView = new AniSequence(this);	

    if(isTestMode){
        // isHuman = false;
        // isFirstBuild = true;
        // _DATA_FILE = new File("/Users/Ryo/Desktop/DataSet/DGVa/mouse/estd118_Keane_et_al_2011_MGSCv37-2011_10_19.129P2_OlaHsd.gvf");
        // selectedOptional = new File("/Users/Ryo/Desktop/DataSet/DGVa/mouse/mouseGeneFunction.csv");
        isHuman = true;
        isFirstBuild = true;
        _DATA_FILE = new File("/Users/Ryo/Desktop/DataSet/DGVa/human/estd180_Pang_et_al_2010.2012-04-18.NCBI36.gvf");
        selectedOptional = new File("/Users/Ryo/Desktop/DataSet/DGVa/human/human_37_haploinsufficiency_score.csv");
        drawStartUpPage();
        startLoading();
    }

}

public void startLoading(){
    isLoadingData = true;
    Runnable loadingFile  = new FileLoader();
    new Thread(loadingFile).start();   
}


//find the chromosome index from a text label
public int _getChromosomeIndex(String chr){
	for (int i = 0; i < _CHROMOSOMES.length; i++) {
        String s = _CHROMOSOMES[i];
        if (s.equals(chr)) {
            return i;
        }
    }
    //debug
    // println("debug: chrIndex = -1:"+chr);
    return -1;
}

//find the event index from the event id
public int _getEventIndex(String eventID) {
    // for (int i = 0; i < _M_EVENTS.length; i++) {
    //     String e = _M_EVENTS[i];
    //     if (e.equals(eventID)) {
    //         return i;
    //     }
    // }
    // return -1;

    //first time
    if(_FOUND_EVENTS == null){
        _FOUND_EVENTS = new ArrayList<String>();
    }
    //check if the new event is already included
    if(_FOUND_EVENTS.contains(eventID)){
        return _FOUND_EVENTS.indexOf(eventID);
    }else{
        //check if it is _EXCLUDED_EVENTS
        for(int i = 0; i< _EXCLUDED_EVENTS.length; i++ ){
            if(eventID.equals(_EXCLUDED_EVENTS[i])){
                return -1;
            }
        }   
        //new event
        _FOUND_EVENTS.add(eventID);

        //debug
        // println("debug: eventID="+eventID);

        return _FOUND_EVENTS.indexOf(eventID);
    }
}

//check if the sv type is to show
public boolean _isValidSvEvent(String status){
    for(int i = 0 ; i<_EVENTS.length; i++){
        if(status.startsWith(_EVENTS[i])){
            return _VALID_EVENT[i];
        }
    }
    return false;
}



public void keyPressed(){
    if(dataLoaded){
        if(textBoxActive){
            //typing gene name
            if((key == BACKSPACE)||(key == DELETE)){
                if(typedString.length() >0){
                    typedString = typedString.substring(0, typedString.length()-1);
                }
            }else if((key >='.') && (key <= 'z')){
                typedString += key;
            }else if((key == ENTER)||(key==RETURN)){
                if(typedString.length() >0){
                    //search for the gene
                    searchGene(typedString);
                }
            }
            updateLegendView();
            // image(legendView, legendView_x, legendView_y);
            loop();
        }else{
            if(key == '1'){
                _VIEW_MODE = 0;
            }else if(key == '2'){
                _VIEW_MODE = 3;
            }else if(key == '3'){
                _VIEW_MODE =2;
            }else if(key == '4'){
                _VIEW_MODE =1;
            }else if(key == '5'){
                _VIEW_MODE =4;
            }else if (key == 'p') {
                println("print");
                saveFrame = true;
            }else if(key == CODED){
                //change bubble alpha
                if(keyCode == UP){
                    _bubble_alpha = min(255, (_bubble_alpha +40));
                }else if(keyCode == DOWN){
                    _bubble_alpha = max(0 , (_bubble_alpha -40));
                }
                println("debug: bubble_alpha ="+_bubble_alpha);
            }   
            updateBubbles();
        }
    }else{
        if(promoter_input_active){
            if((key  >= '0')&&(key <= '9')){
                promoter_input += key;
            }else if((key == BACKSPACE)||(key==DELETE)){
                if(promoter_input.length() >0){
                    promoter_input = promoter_input.substring(0, promoter_input.length()-1);
                }
            }else if((key == ENTER)||(key == RETURN)){
                promoter_input_active = false;
                _PROMOTER_LENGTH = Integer.parseInt(promoter_input);
            }
        }
    }
}

//---------------- mouse interaction -------------------------
public void mousePressed(){
    if(dataLoaded){
        //main area
        if(mouseX < legendView_x && mouseY < geneView_y){
            //Main area
            if(mouseY  < view_mode_y+ _margin){ // _VIEW_MODE
                // println("top bar area");
                //check for buttons
                for(int i = 0; i < _VIEW_MODE_RECT.length; i++){
                    Rectangle r = _VIEW_MODE_RECT[i];
                    if(r.contains(mouseX, mouseY)){
                        // println("click: "+i);
                        if(i == 0){
                            _VIEW_MODE = 0;
                        }else if(i == 1){
                            _VIEW_MODE = 3;
                        }else if(i == 2){
                            _VIEW_MODE = 2;
                        }else if(i == 3){
                            _VIEW_MODE = 1;
                        }
                        updateBubbles();
                        return;
                    }
                }
            }

            if(_VIEW_MODE == 0){  //GeneBlock view
                //check for GeneGroup Selection
                boolean selected = false;
                outloop:
                for(int i = 0; i<data.affectedGeneGroup.size(); i++){
                    GeneGroup gg = data.affectedGeneGroup.get(i);
                    if(gg.rect.contains(mouseX, mouseY)){
                        // println("debug: hit! "+gg.genes.get(0).geneSymbol +" "+gg.genes.size());
                        //add or remove genes
                        if(_selected_genes.contains(gg.genes.get(0))){
                            //unselect
                            _selected_genes.removeAll(gg.genes);
                        }else{
                            //select
                            if(keyPressed && keyCode  == SHIFT){
                                //check Chromosome
                                if(_C_CHR_INDEX != gg.genes.get(0).chrIndex){
                                    _selected_genes.clear();
                                }
                            }else{
                                //clear array
                                _selected_genes.clear();
                            }
                            for(Gene g:gg.genes){
                                if(g.isSelectable){
                                    _selected_genes.add(g);
                                    selected = true;
                                    // println(g.geneSymbol +" is selectable!");
                                }else{
                                    // println(g.geneSymbol +" is NOT selectable!");
                                }

                            }
                            // _selected_genes.addAll(gg.genes);
                        }
                        if(selected){
                            _C_CHR_INDEX = gg.genes.get(0).chrIndex; // for geneView
                            break  outloop;
                        }
                    }
                }
                //check unaffectedGeneGroup if none of affected genegroup was selected
                //update view 
                if(selected){
                    //update gene view
                    updateCurrentBP();
                    updateGeneView();
                    //search Go Term
                    searchGoTerm();
                    updateLegendView();

                }else{
                    //unselect all
                    _selected_genes.clear();
                    _selected_goTerms.clear();
                    updateGeneView();
                    updateLegendView();
                }
            }else if(_VIEW_MODE == 1){ //Unit Plot view
                boolean selected = false;
                outloop:
                for(int i = 0; i<data.affectedGeneGroup.size(); i++){
                    GeneGroup gg = data.affectedGeneGroup.get(i);
                    for(int j = 0; j < gg.genes.size(); j++){
                        Gene g = gg.genes.get(j);
                        for(int k = 0; k < g.bubbles.size(); k++){
                            Bubble b = g.bubbles.get(k);
                            if(mouseX > b.newPosition.x && mouseX < (b.newPosition.x+_bubble_d)){
                                if(mouseY >b.newPosition.y && mouseY < (b.newPosition.y +_bubble_d)){
                                    if(_VALID_EVENT[b.event.svIndex]){
                                        //hit
                                        selected = true;
                                        if(_selected_genes.contains(g)){
                                            _selected_genes.remove(g);
                                        }else{
                                            _selected_genes.clear();
                                            _selected_genes.add(g);
                                        }
                                        _C_CHR_INDEX = gg.genes.get(0).chrIndex; // for geneView
                                        break  outloop;
                                    }
                                }
                            }
                        }
                    }
                }
                //update view 
                if(selected){
                    //update main view
                    //update gene view
                    updateCurrentBP();
                    updateGeneView();
                    //search Go Term
                    searchGoTerm();
                    updateLegendView();
                }else{
                    //unselect all
                    _selected_genes.clear();
                    _selected_goTerms.clear();
                    updateGeneView();
                    updateLegendView();
                }
            }else if(_VIEW_MODE == 2){ //BP view
                boolean selected = false;
                if (keyPressed && keyCode == SHIFT) {
                }else{
                    _selected_genes.clear();
                }
                // outloop:
                for(int i = 0; i<data.affectedGeneGroup.size(); i++){
                    GeneGroup gg = data.affectedGeneGroup.get(i);
                    for(int j = 0; j < gg.genes.size(); j++){
                        Gene g = gg.genes.get(j);
                        if(g.isSelectable){
                            Bubble b = g.bubbles.get(0);
                            if(mouseX > b.bpView.x && mouseX < (b.bpView.x+_bubble_d)){
                                if(mouseY >b.bpView.y && mouseY < (b.bpView.y +_bubble_d)){
                                    //hit
                                    selected = true;
                                    if(_selected_genes.contains(g)){
                                        _selected_genes.remove(g);
                                    }else{
                                        if(_C_CHR_INDEX != gg.genes.get(0).chrIndex){
                                            _selected_genes.clear();
                                        }
                                        _selected_genes.add(g);
                                    }
                                    _C_CHR_INDEX = gg.genes.get(0).chrIndex; // for geneView
                                    // break  outloop;
                                   
                                }
                            }   
                        }
                    }
                }
                //update view 
                if(selected){
                    //update main view
                    //update gene view
                    updateCurrentBP();
                    updateGeneView();
                    //search Go Term
                    searchGoTerm();
                    updateLegendView();
                }else{
                    //unselect all
                    _selected_genes.clear();
                    _selected_goTerms.clear();
                    updateGeneView();
                    updateLegendView();
                }
            }else if(_VIEW_MODE == 3){ //Expanded view
                boolean selected = false;
                outloop:
                for(int i = 0; i<data.affectedGeneGroup.size(); i++){
                    GeneGroup gg = data.affectedGeneGroup.get(i);
                    for(int j = 0; j < gg.genes.size(); j++){
                        Gene g = gg.genes.get(j);
                        if(g.isSelectable){
                            Bubble b = g.bubbles.get(0);
                            if(mouseX > b.newPosition.x && mouseX < (b.newPosition.x+_bubble_d)){
                                if(mouseY >b.newPosition.y && mouseY < (b.newPosition.y +_bubble_d)){
                                    //hit
                                    selected = true;
                                    if(_selected_genes.contains(g)){
                                        _selected_genes.remove(g);
                                    }else{
                                        if (keyPressed && keyCode == SHIFT) {
                                            if(_C_CHR_INDEX != gg.genes.get(0).chrIndex){
                                                _selected_genes.clear();
                                            }
                                        }else{
                                            _selected_genes.clear();
                                        }
                                        _selected_genes.add(g);
                                    }
                                    _C_CHR_INDEX = gg.genes.get(0).chrIndex; // for geneView
                                    break  outloop;
                                }
                            }   
                        }
                    }
                }
                //update view 
                if(selected){
                    //update main view
                    //update gene view
                    updateCurrentBP();
                    updateGeneView();
                    //search Go Term
                    searchGoTerm();
                    updateLegendView();
                }else{
                    //unselect all
                    _selected_genes.clear();
                    _selected_goTerms.clear();
                    updateGeneView();
                    updateLegendView();
                }
            }
        }else if(mouseX > legendView_x && mouseY < geneView_y){
            //Legend area
            //legendView
            if(legendView_x < mouseX && (legendView_y+legend_h) > mouseY){
                if(legend_close_rect.contains(mouseX, mouseY)){
                    // println("legend rectangle!");
                    showLegend = !showLegend;
                    if(showLegend){
                        showLegendView();
                    }else{
                        hideLegendView();
                    }
                }else{
                    if(showLegend){ //when legend is visible
                        //radio button
                        int mapped_x =( mouseX - legendView_x);
                        int mapped_y = (mouseY - legendView_y);
                        for(int i = 0; i < radio_btns.length; i++){
                            if(radio_btns[i].contains(mapped_x, mapped_y)){
                                //toggle
                                _VALID_EVENT[i] = !_VALID_EVENT[i];
                                updateLegendView();     //update legend look
                                updateGeneGroups();     //assign new positions based on _VIEW_MODE
                                updateBubbles();
                                break;
                            }else if(multi_event_btn.contains(mapped_x, mapped_y)){
                                // println("debug show multi event click!");
                                _show_multi_event = !_show_multi_event;
                                updateLegendView();     //update legend look
                                updateBubbles();
                                break;
                            }
                        }

                        //optional table
                        if(hasOptionalTable){
                            //check if any box clicked
                            ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(data.optionalTable.values());
                            for(int i = 0; i<categories.size(); i++){
                                OptionalCategory oc = categories.get(i);
                                if(oc.rect.contains(mapped_x, mapped_y)){
                                    // println("!!!!!!!!!!!!!OC click: "+oc.name);
                                    oc.isSelected = !oc.isSelected;
                                    updateLegendView();

                                    if(oc.isSelected){
                                        //add to selected gene list
                                        if(selected_genes_by_category == null){
                                            selected_genes_by_category = new ArrayList<Gene>();
                                        }
                                        selected_genes_by_category.addAll(oc.genes);
                                    }else{
                                        //remove from seleged gene
                                        selected_genes_by_category.removeAll(oc.genes);
                                    }



                                    // println("Debug: selected gene count = "+selected_genes_by_category.size());

                                    break;
                                }
                            }
                        }

                        //textBox
                        if(textBox.contains(mapped_x, mapped_y)){
                            textBoxActive = !textBoxActive;
                            updateLegendView();
                            return;
                        }
                        textBoxActive = false;

                        //check if it is scrollbar
                        if(scrollBar != null && scrollBar.contains(mapped_x, mapped_y)){
                            // println("scroll bar!");
                            isScrolling = true;
                            scrolling_y = mouseY;
                            beforeScrollIndex = goTermIndex;

                            // println("degbug: index before ="+goTermIndex);
                        }else if(goTermRect.contains(mapped_x, mapped_y)){ //go term
                            //find the index
                            int index = findGoRectIndex(mouseX, mouseY);
                            if(index == -1){
                                println("out side of goTermRectangle area");
                            }else{
                                Go selected = goToDisplay[index];
                                if(selected != null){
                                    // println("selected:"+selected.toString());
                                    if(selected.isSuperclass){
                                        //type
                                        GoType gs = (GoType) selected;
                                        int typeIndex = getGoTypeIndex(gs.type);
                                        foldedGoTypes[typeIndex] =  !foldedGoTypes[typeIndex];
                                        _selected_genes_by_go.clear();//reset
                                        // _selected_goTerms.clear();
                                        _selected_go = null;
                                    }else{
                                        //selecting go term
                                        if(_selected_go != selected){
                                            _selected_genes_by_go.clear();//reset
                                            _selected_genes_by_go.addAll(selected.genes);
                                            if(_selected_genes.size() >0){
                                                //nothing
                                            }else{
                                                _selected_goTerms.clear();
                                                
                                            }
                                            
                                            _selected_go = selected;
                                        }else{
                                            //unselect
                                            _selected_genes_by_go.clear();//reset
                                            // _selected_goTerms.clear();
                                            _selected_go = null;
                                        }
                                        // _selected_goTerms.add(selected);
                                    }
                                }
                            }
                            updateLegendView();
                        }
                    }
                }
            }else{
                textBoxActive = false;
                updateLegendView();
            }
        }else if(showGeneView && mouseY > geneView_y){
            //check if it is over gene_rects
            if(gene_rects != null){
                for(int i = 0; i<gene_rects.length; i++){
                    Rectangle rect = gene_rects[i];
                    if(rect.contains(mouseX, (mouseY - geneView_y))){
                        // get gene and pop on browser
                        Gene gene = _selected_genes.get(i);
                        String chr = gene.chromosome.toUpperCase().replace("CHR", "");
                        // println("pop up for "+gene.geneSymbol);
                        open(_LINK+_SPECIES+"/Location/View?r="+chr+":"+gene.tsp+"-"+gene.tep);
                        // new BrowserLauncher().openURLinBrowser("http://www.google.com");
                        return;
                    }
                }
                //no mouse over gene names
            }
            if(gene_view_left_btn.contains(mouseX, mouseY)){
                // println("debug: to the left!");
                c_bp_start -= 1000;
                updateGeneViewGenes();
                updateGeneView();

            }else if(gene_view_right_btn.contains(mouseX, mouseY)){
                c_bp_end += 1000;
                updateGeneViewGenes();
                updateGeneView();
            }

        }else{
            //the rest
        }
    }else{
        //check mouse positions
        if(model_one_rect.contains(mouseX, mouseY)){
            isHuman = true;
            isUserDefinedBuild = false;
        }else if( model_two_rect.contains(mouseX, mouseY)){
            isHuman = false;
            isUserDefinedBuild = false;
        }else if(build_one_rect.contains(mouseX, mouseY)){
            isFirstBuild = true;
        }else if(build_two_rect.contains(mouseX, mouseY)){
            isFirstBuild = false;
        }else if(file_btn.contains(mouseX, mouseY)){
            println("Select a file");
            selectInput("Select a GVF file:", "gvfSelection");
            // loop();
        }else if(optional_btn.contains(mouseX, mouseY)){
            println("select a optionalfile");
            selectInput("Select an optional csv file:", "optionalSelection");

        }else if(load_btn.contains(mouseX, mouseY)){
            //start loading
            if(selectedGVF != null){
                _DATA_FILE = selectedGVF;
                isLoadingData = true;
                startLoading();
            }
        }else if(promoter_length_text_box.contains(mouseX, mouseY)){
            promoter_input_active = !promoter_input_active;

            if(promoter_input_active){
                //input mode
                if(promoter_input.equals("0")){
                    promoter_input = "";
                }
            }else{
                //done
                if(promoter_input.equals("")){
                    promoter_input = "0";
                    _PROMOTER_LENGTH = Integer.parseInt(promoter_input);
                }else{
                    _PROMOTER_LENGTH = Integer.parseInt(promoter_input);
                }
            }
            loop();
        }else if(userDefined_rect.contains(mouseX, mouseY)){
            isUserDefinedBuild = !isUserDefinedBuild;
            selectInput("Select a gene track file:", "userGenetrack");
            loop();

        }else{
            promoter_input_active = false;
            if(promoter_input.equals("")){
                promoter_input = "0";
                _PROMOTER_LENGTH = Integer.parseInt(promoter_input);
            }else{
                _PROMOTER_LENGTH = Integer.parseInt(promoter_input);
            }
            loop();
        }


    }
}

public void mouseDragged(){
    if(isScrolling){
        int difference = mouseY - scrolling_y;// - mouseY;
        int change = round(map(difference, -1*(goTerm_h - scrollBar.height), goTerm_h - scrollBar.height, -1*(totalGoCount-1), totalGoCount-1)); //-1*(maxGoRowCount-1), (maxGoRowCount-1)));
        
        goTermIndex = constrain(beforeScrollIndex + change, 0, abs(totalGoCount - maxGoRowCount));
        // println("drag action difference= "+difference+"  change="+change +"  goTerm index="+goTermIndex);
        //update goTermIndex;
        updateLegendView();
        loop();
    }
}

public void mouseReleased(){
    if(dataLoaded){
        isScrolling = false;
        loop();
    }
}
public void mouseMoved(){
    if(dataLoaded){
        if(showLegend && legendView_rect.contains(mouseX, mouseY)){
            //updateLegendView();
        }else if(showGeneView && mouseY > geneView_y){
            if(gene_view_right_btn.contains(mouseX, mouseY) || gene_view_left_btn.contains(mouseX, mouseY)){
                cursor(HAND);
                updateGeneView();
                loop();
                return;
            }
            //check if it is over gene_rects
            if(gene_rects != null){
                for(int i = 0; i<gene_rects.length; i++){
                    Rectangle rect = gene_rects[i];
                    if(rect.contains(mouseX, (mouseY - geneView_y))){
                        cursor(HAND);
                        updateGeneView();// draw the view
                        loop();
                        return;
                    }
                }
                //no mouse over gene names
                cursor(ARROW);
                updateGeneView();
                loop();
                return;
            }
        }
    }else{
        if(model_one_rect.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if( model_two_rect.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(build_one_rect.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(build_two_rect.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(file_btn.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(load_btn.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(promoter_length_text_box.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(userDefined_rect.contains(mouseX, mouseY)){
            cursor(HAND);
        }else{
            cursor(ARROW);
        }
    }

}
//called once at the beginning
public void initBubbles(){
    for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
        GeneGroup gg = data.affectedGeneGroup.get(i);
        for(int j = 0; j< gg.genes.size(); j++){
            Gene gene = gg.genes.get(j);
            for(int k = 0; k < gene.bubbles.size(); k++){
                Bubble b = gene.bubbles.get(k);
                b.display_x = b.newPosition.x;
                b.display_y = b.newPosition.y;
            }
        }
    }
}

//called when the _VIEW_MODE changes
public void updateBubbles(){
    updateGeneGroups(); //
    animation  = new AniSequence(this);
    animation.beginSequence();
    animation.beginStep();
    for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
        GeneGroup gg = data.affectedGeneGroup.get(i);
        for(int j = 0; j< gg.genes.size(); j++){
            Gene gene = gg.genes.get(j);
            for(int k = 0; k < gene.bubbles.size(); k++){
                Bubble b = gene.bubbles.get(k);
                b.update();
            }
        }
    }
    animation.endStep();
    animation.endSequence();
    animation.start();
    loop();
}

public void searchGene(String geneName){
    println("searchGene(): "+geneName);
    for(int  i = 0; i<data.affectedGeneGroup.size(); i++){
        GeneGroup gg = data.affectedGeneGroup.get(i);
        for(int j = 0; j<gg.genes.size(); j++){
            Gene g = gg.genes.get(j);
            if(g.geneSymbol.equals(geneName)){
                //gene match
                _selected_genes.clear();
                _selected_genes.add(g);
                updateGeneView();
                textBoxActive = false;// make the text box inactive
                //search Go Term
                searchGoTerm();
                updateLegendView();
                break;
            }
        }
    }
    println("gene not found");

}


public void searchGoTerm(){
    //for all the selected Genes
    _selected_goTerms.clear();
    for(int i = 0; i<_selected_genes.size();i++){
        Gene gene = _selected_genes.get(i);
        ArrayList<Go> goArray = gene.goTerms;
        if(goArray != null){
            _selected_goTerms.addAll(goArray);
        }
    }
    // println("number of associated Go terms are "+_selected_goTerms.size());
}
//called when file is selected
public void gvfSelection(File selection){
    if(selection == null){
        // println("file was not selected");
        selectedGVF = null;
        _DATA_FILE = selectedGVF;
    }else{
        _DATA_FILE = selection;
        selectedGVF = _DATA_FILE;
        file_textbox_w = round(textWidth(_DATA_FILE.getName())+_margin);
        file_name_rect.width = file_textbox_w;
        //check genome assembly
        checkGenomeAssembly();
    }
}
//called when optinal file is selected
public void optionalSelection(File selection){
    if(selection == null){
        // println("file was not selected");
        selectedOptional = null;
    }else{
        selectedOptional = selection;
    }
}
//called when user selects GVF file
public void checkGenomeAssembly(){
    // println("checkGenomeAssembly()");
    try{
        //Get the file from the data folder
        BufferedReader reader = createReader(_DATA_FILE);
        //Loop to read the file one line at a time
        String line = null;
        while((line = reader.readLine()) != null) {
            if(line.startsWith("##")){
                String[] contents = splitTokens(line);
                if(contents[0].equals("##genome-build")){
                    // println("genomebuild:" + Arrays.toString(contents));
                    if(contents[2].equals("NCBI36")){
                        isHuman = true;
                        isFirstBuild = true;
                    }else if(contents[2].equals("NCBI37")){
                        isHuman = true;
                        isFirstBuild = false;
                    }else if(contents[2].equals("MGSCv37")){
                        isHuman = false;
                        isFirstBuild = true;
                    }else if(contents[2].equals("NCBI38")){
                        isHuman = false;
                        isFirstBuild = false;
                    }else{
                        println("Error: unknown genome build:"+contents[2]);
                    }
                }
            } else {
                break;
            }
        }
    }catch(IOException e){
        e.printStackTrace();
    }
}

//user Defined function
public void userGenetrack(File selection){
    if(selection == null){
        isUserDefinedBuild = false;
    }else{
        _GENETRACK_FILE = selection.getAbsolutePath();
        selectInput("Select a cytoband file:", "userCytoband");
    }
}
public void userCytoband(File selection){
    if(selection == null){
        isUserDefinedBuild = false;
    }else{
        _CYTOBAND_FILE = selection.getAbsolutePath();
        selectInput("Select a go term file:", "userGo");
    }
}

public void userGo(File selection){
    if(selection == null){
        isUserDefinedBuild = false;
    }else{
        _GO_FILE = selection.getAbsolutePath();
    }
}


class Band{
	int start, end, stain;
	String name;
	boolean isCentromere = false;

	Band(int bStart, int bEnd, String n, int stainValue){
		start = bStart;
		end = bEnd;
		name = n;
		stain = stainValue;
		if(stain == -1){
			//centromere
			isCentromere = true;
			stain = 0; 
		}
	}
}
class Bubble{
	float display_x, display_y;
	PVector bpView;
	PVector newPosition;
	Event event;
	Gene parentGene;
	String type;
	// PShape pshape;

	//constructor
	Bubble(Gene gene, Event e, String t){
		event = e;
		parentGene = gene;
		type = t;
	}
	
	public void render(){
		// set color
		int svIndex = event.svIndex;
		fill(color_sv_array3[svIndex], _bubble_alpha);
		noStroke();
		if(type.endsWith("all")){
			_drawCircle(display_x, display_y, _bubble_d, _bubble_d);
		}else if(type.endsWith("left")){
			_drawHalfCircle(display_x, display_y, _bubble_d, _bubble_d, true);
		}else if(type.endsWith("middle")){
			_drawMiddleOfCircle(display_x, display_y, _bubble_d, _bubble_d);
		}else if(type.endsWith("right")){
			_drawHalfCircle(display_x, display_y, _bubble_d, _bubble_d, false);
		}else{
			println("debug:GUI: unexpected bubble type: "+type);
		}

		// _drawCircle(display_x, display_y, _bubble_d, _bubble_d);
		// shape(pshape, display_x, display_y, _bubble_d, _bubble_d);

		//outline
		// if(_VIEW_MODE != 2 && !parentGene.isMultiEvent){
		if(_VIEW_MODE != 2 ){

			stroke(color_gray);
			strokeWeight(0.5f);
			noFill();
			ellipse( display_x, display_y, _bubble_d, _bubble_d);
		}
	}
	//highlight selected bubble
	public void highlight(){
		stroke(color_pink);
		strokeWeight(2f);
		noFill();
		ellipse( display_x, display_y, _bubble_d, _bubble_d);
	}
	public void highlight_soft(){
		stroke(color_cyan, 80);
		strokeWeight(8f);
		noFill();
		ellipse( display_x, display_y, _bubble_d, _bubble_d);
	}

	//highlight selected bubble by category
	public void highlight_by_category(){
		stroke(color_dark_dark_gray);
		// stroke(color_pink);
		strokeWeight(3f);
		noFill();
		ellipse( display_x-1, display_y-1, _bubble_d+2, _bubble_d+2);
	}

	public void drawBackground(){
		// println("drawbackground()");
		noStroke();
		fill(color_bubble_background);
		ellipse( display_x, display_y, _bubble_d, _bubble_d);
	}

	public void update(){
		float targetX =0;
		float targetY = 0;
		if(_VIEW_MODE == 0){
			// targetX = geneBlock.x;
			// targetY = geneBlock.y;
			targetX = newPosition.x;
			targetY = newPosition.y;
		}else if(_VIEW_MODE == 1){
			// targetX = unitPlot.x;
			// targetY = unitPlot.y;
			targetX = newPosition.x;
			targetY = newPosition.y;
		}else if(_VIEW_MODE == 2){
			targetX = bpView.x;
			targetY = bpView.y;
		}else if(_VIEW_MODE == 3){
			targetX = newPosition.x;
			targetY = newPosition.y;
			// targetX = expanded.x;
			// targetY = expanded.y;
		}else if(_VIEW_MODE == 4){
			//unit plot 2
			targetX = newPosition.x;
			targetY = newPosition.y;
		}


		animation.add(Ani.to(this, 1.5f, "display_x", targetX, Ani.EXPO_IN_OUT));
		animation.add(Ani.to(this, 1.5f, "display_y", targetY, Ani.EXPO_IN_OUT));
	}
	//multievent
	public void renderMultiEvent(){
		fill(60);
		noStroke();
		// dot in the middle
		// ellipseMode(CENTER);
		// float cx = display_x +((float)_bubble_d/2f);
		// float cy = display_y +((float)_bubble_d/2f);
		// ellipse(cx, cy, 4, 4);
		// ellipseMode(CORNER);

		// ellipseMode(CORNER);
		float cx = display_x +((float)_bubble_d/2f) -2;
		float cy = display_y +((float)_bubble_d/2f) -2;
		ellipse(cx, cy, 4, 4);
	}

}
private int color_deletion = color(117, 112, 179);
private int color_amplification = color(217, 95, 2);
private int color_variation = color(27, 158, 119);
private int color_gray = 120;//180;
private int color_gray_trans = color(180, 180, 180, 120);
private int color_lightGray = 240;
private int color_dark_gray = 120; //120;
private int color_dark_dark_gray = 80;
private int color_white = 255;
private int transparency = 150;
private int trans2 = 10;
private int color_trans_deletion = color(117, 112, 179, transparency);//color(117, 112, 179, transparency);
private int color_trans_amplification = color(217, 95, 2, transparency);
private int color_trans_variation = color(27, 158, 119, transparency);
private int color_trans2_deletion = color(117, 112, 179, trans2);
private int color_trans2_amplification = color(217, 95, 2, trans2);
private int color_trans2_variation = color(27, 158, 119, trans2);
private int color_green = color(27, 158, 119);
private int color_orange = color(217, 95, 2);
private int color_blue = color(117, 112, 179);
private int color_pink = color(231, 41, 138);
private int color_pink_trans = color(231, 41, 138, transparency);
private int color_cyan_trans =color(0, 174, 237, transparency);
private int color_cyan = color(0, 174, 237);
private int color_limegreen = color(102, 166, 30);
private int color_yellow = color(230, 171, 2);
private int color_brown = color(166, 118, 29);
private int color_grey = color(120);

//colorbrewer
int color_g = color(27, 158,119);
int color_o = color(217,95,2);
int color_bl = color(117,112,179);
int color_p = color(231,41,138);
int color_lg = color(102,166,30);
int color_y = color(230,171,2);
int color_br = color(166,118,29);

int color_g_trans_1 = color(27, 158,119, transparency);
int color_o_trans_1 = color(217,95,2, transparency);
int color_bl_trans_1 = color(117,112,179, transparency);
int color_p_trans_1 = color(231,41,138, transparency);
int color_lg_trans_1 = color(102,166,30, transparency);
int color_y_trans_1 = color(230,171,2, transparency);
int color_br_trans_1 = color(166,118,29, transparency);
int color_grey_trans_1 = color(120, transparency);

int color_g_trans_2 = color(27, 158,119, trans2);
int color_o_trans_2 = color(217,95,2, trans2);
int color_bl_trans_2 = color(117,112,179, trans2);
int color_p_trans_2 = color(231,41,138, trans2);
int color_lg_trans_2 = color(102,166,30, trans2);
int color_y_trans_2 = color(230,171,2, trans2);
int color_br_trans_2 = color(166,118,29, trans2);
int color_grey_trans_2 = color(120, trans2);



int[] color_sv_array = {color_bl_trans_1, color_g_trans_1, color_o_trans_1, color_y_trans_1, color_lg_trans_1, color_br_trans_1, color_grey_trans_1};
int[] color_sv_array2 = {color_bl_trans_2, color_g_trans_2, color_o_trans_2, color_y_trans_2, color_lg_trans_2, color_br_trans_2, color_grey_trans_2};
int[] color_sv_array3 ={color_bl, color_g, color_o,color_y, color_lg, color_br, color_grey};


// private int[] color_array = {this.color_pink, this.color_orange, this.color_yellow, this.color_blue, this.color_green, this.color_green, this.color_green, this.color_orange};
//"deletion","inversion","tandem_duplication", "copy_number_gain", "copy_number_variation","complex_structural_alteration"
// private int[] color_sv_array = {color_trans_deletion, color_trans_variation, color_trans_amplification, color_trans_amplification, color_trans_variation, color_trans_variation};
// private int[] color_sv_array2 = {color_trans2_deletion, color_trans2_variation, color_trans2_amplification, color_trans2_amplification, color_trans2_variation, color_trans2_variation};
// private int[] color_sv_array3 ={color_deletion, color_variation, color_amplification, color_amplification, color_variation, color_variation};

private int color_bubble_selected = 0;


private int color_main_background = 255;
private int color_bubble_background =  240;
private int color_geneView_background = 240;


String mouseCytoband = "mouseCytoband.txt";
String mouseGeneTrack = "mousegenes.txt";

String[] _CONSEQUENCES = {"all", "left", "middle", "right", "both_sides"};/////////
String[] _UNIT_PLOT_TYPES;


String _optional_category = "";


class DataSet{
	int[] svTypeCounter;
	int[] maxLength;
	ArrayList<Event> events;
	HashMap<Integer, ArrayList<Band>> cytobands;
	HashMap<Integer, ArrayList<Event>> eventChrMap;
	HashMap<Integer, ArrayList<Gene>> geneChrMap;
	ArrayList<GeneGroup> geneGroupArrays[];

	HashMap<String, ArrayList<Bubble>> bubbleBySvType; //store bubble by 
	ArrayList<GeneGroup> affectedGeneGroup, unaffectedGeneGroup;

	HashMap<String, Gene> affectedGeneMap; //indexing affected Gene
	HashMap<String, Gene> affectedGeneMap_ensembl; //ensembl gene id

	//Gene Ontology terms
	HashMap<String, ArrayList<Go>> all_goMap_type; //type, array of Go
	HashMap<String, Go> all_goMap; 
	ArrayList<String> goTypes;


	//optional category
	HashMap<String, OptionalCategory> optionalTable;


	int match = 0;


	//constructor
	DataSet(){
		events = new ArrayList<Event>();
		cytobands = new HashMap<Integer, ArrayList<Band>>();
		eventChrMap = new HashMap<Integer, ArrayList<Event>>();
		geneChrMap = new HashMap<Integer, ArrayList<Gene>>();
		geneGroupArrays = new ArrayList[_CHROMOSOMES.length];

		affectedGeneGroup = new ArrayList<GeneGroup>();
		unaffectedGeneGroup = new ArrayList<GeneGroup>();
		bubbleBySvType = new HashMap<String, ArrayList<Bubble>>();
		//goterms
		all_goMap_type = new HashMap<String, ArrayList<Go>>();
		all_goMap = new HashMap<String, Go> ();
		goTypes = new ArrayList<String>();


 		svTypeCounter = new int [10];		
	}
	public void loadData(){
		loadGVF(_DATA_FILE);
		setupSV_EVENTS();

		loadCytoband(_CYTOBAND_FILE);
		loadGeneTrack(_GENETRACK_FILE);

		//optional file
		// loadOptionalCSV();
		//debug events counts
		for(int i = 0; i<_EVENTS.length; i++){
			println(_EVENTS[i]+":"+svTypeCounter[i]+" events");
		}

		Iterator i = geneChrMap.entrySet().iterator();  // Get an iterator
		while (i.hasNext()) {
		  Map.Entry me = (Map.Entry)i.next();
		  //String key = (String) me.getKey();
		  int chrIndex = (Integer) me.getKey();
		  String chromosomeName =_CHROMOSOMES[chrIndex];
		  print( "debug:"+chromosomeName + " has ");
		  ArrayList<Gene> geneList = (ArrayList<Gene>) me.getValue();
		  println(geneList.size()+" genes");
		}
	}

	//index affected genes into HashMap 
	public void indexAffectedGene(){
		affectedGeneMap = new HashMap<String, Gene>();
		//ensembl id specific
		affectedGeneMap_ensembl = new HashMap<String, Gene>();

		for(int j = 0; j<affectedGeneGroup.size(); j++){
			GeneGroup gg = affectedGeneGroup.get(j);
			for(int k = 0; k <gg.genes.size(); k++){
				Gene g = gg.genes.get(k);
				affectedGeneMap.put(g.geneSymbol, g);
				//Ensembl id
				if(!g.ensemblID.equals("NA")){
					affectedGeneMap_ensembl.put(g.ensemblID, g);
				}
			}
		}
		println("---indexed genes count="+affectedGeneMap.size());
	}

	public void loadGoTerms(){
		println("---start go terms ----");
		File f;
		if(isUserDefinedBuild){
			f = new File(_GO_FILE);
		}else{
			f = new File(dataPath(_GO_FILE));
		}	
		try{
			//Get the file from the data folder
			BufferedReader reader = createReader(f);
			//Loop to read the file one line at a time
			// File f = new File(dataPath(_GO_FILE));
			//get file size
			_total_data = f.length();
			_current_data = 0;
			_current_load_message = "loading GO:"+f.getName()+" ...";


			String line = null;
			while((line = reader.readLine()) != null) {
				_current_data  += line.length();
				String[] contents = split(line, TAB);
				if (contents[0].startsWith("#")) {  //header line
            		println("HEADER#:" + line);
        		} else {
            		if (contents[0] != null) {
                		//parse data
                		parseGo(contents);
            		} else {
                		println("string content is null");
            		}
        		}
			}
		}catch(IOException e){
			e.printStackTrace();
		}

		println("---end of go terms loading----");
	}
	public void parseGo(String[] s){
		//check if it contains geneSymbol
		if(!s[4].equals("n/a")){
			String[] genes = splitTokens(s[4], ",");
			//create a go object
			String name = s[1].trim();
			String type = s[2].trim(); //superclass
			Go go = new Go(name);
			boolean matchFound = false;
			//iterate all assocated gene
			for(int i = 0; i<genes.length; i++){
				Gene gene = affectedGeneMap.get(genes[i]);
				if(gene != null){
					//match found
					matchFound = true;
					//add Gene to Go object
					go.genes.add(gene);
					//add Go to Gene
					gene.addGoTerm(go);
					//get ArrayList //super class
					ArrayList<Go> goArray = (ArrayList<Go>)all_goMap_type.get(type);
					if(goArray == null){
						goArray = new ArrayList<Go>();
						all_goMap_type.put(type, goArray);
						goTypes.add(type);
					}
					if(!goArray.contains(go)){
						goArray.add(go);
					}
				}else{
					// non_match++;
				}
			}
			if(matchFound){
				//save in all go term map
				all_goMap.put(name, go);
			}
		}
	}
	//sort Go objects by its names
	public void sortGoTerms(){
		Iterator ite = all_goMap_type.entrySet().iterator();  // Get an iterator
	    while (ite.hasNext()) {
	      Map.Entry me = (Map.Entry)ite.next();
	      String type = (String) me.getKey();
	      ArrayList<Go> array = (ArrayList<Go>)me.getValue();
	      Collections.sort(array);
	    }
	}


	//load .gvf file
	public void loadGVF(File file){
		// println("debug: loadGVF():"+ file.getName());
		try{
			//get file size
			_total_data = file.length();
			_current_data = 0;
			_current_load_message = "loading GVF:"+file.getName()+" ...";
			//Get the file from the data folder
			BufferedReader reader = createReader(file);
			//Loop to read the file one line at a time
			String line = null;
			while((line = reader.readLine()) != null) {
				_current_data  += line.length();
				String[] contents = split(line, TAB);
				if (contents[0].startsWith("#")) {  //header line
            		println("#:" + line);
        		} else {
            		if (contents[0] != null) {
                		//parse data
                		parseSV(contents);
            		} else {
                		println("string content is null");
            		}
        		}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	//parse gvf data lines
	public void parseSV(String[] s){
		String chromosome = s[0].trim().toLowerCase();
        int chrIndex = _getChromosomeIndex(chromosome);
        String svType = s[2].trim().toLowerCase();
       	//check syntax
       	if(svType.contains(" ")){
       		println("debug:*** need to implement to parse :"+svType);
       	}

        int svIndex = _getEventIndex(svType);
        int s_pos = Integer.parseInt(s[3]);
        int e_pos = Integer.parseInt(s[4]);
        //String[] metaData = s[8].split(";");

        if(svIndex == -1){
        	// println("debug *** sv type that has not yet implementd:"+svType);
        }else if (chrIndex == -1){
        	println("debug: Event chrIndex = -1:"+chromosome);
        }else{
	        //create Event object
	        Event newEvent = new Event(chrIndex, svIndex, s_pos, e_pos);
	        events.add(newEvent);
	        svTypeCounter[svIndex]++;
	        // Integer count = svTypeCounter.svIndex);
	        // count = new Integer(count.intValue()+1);


	        //sort by chromosome
	        ArrayList<Event> eventList = eventChrMap.get(newEvent.chrIndex);
	        if(eventList == null){
	        	eventList = new ArrayList<Event>();
	        	eventChrMap.put(newEvent.chrIndex, eventList);
	        }
	        eventList.add(newEvent);
        }
	}
	//load cytoband file
	public void loadCytoband(String file){
		File f;
		if(isUserDefinedBuild){
			f = new File(file);
		}else{
			f = new File(dataPath(file));
		}	
		try{
			_current_load_message = "loading Cytoband:"+file+" ...";
			//Get the file from the data folder
			BufferedReader reader = createReader(f);
			//Loop to read the file one line at a time
			String line = null;
			while((line = reader.readLine()) != null) {

				String[] contents = split(line, TAB);
				if (contents[0].startsWith("#")) {  //header line
            		println("#:" + line);
        		} else {
            		if (contents[0] != null) {
                		//parse data
                		parseCytoband(contents);
            		} else {
                		println("string content is null");
            		}
        		}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	//parse cytoband file
	public void parseCytoband(String[] s){
		String chr = s[0].trim().toLowerCase();
		int chrIndex = _getChromosomeIndex(chr);
		int bStart = Integer.parseInt(s[1]);
		int bEnd = Integer.parseInt(s[2]);
		String name = s[3].trim();
		String gStain = s[4].trim().substring(4);
		
        int stain;
        if(s[4].equals("acen")){
        	//centromere
        	stain = -1;
        }else if(s[4].equals("gvar")){
        	stain = 100;
        }else if(s[4].equals("stalk")){
        	stain = 0;
        }else if(gStain.equals("")){
	        stain = 0;
        }else{
            stain = Integer.parseInt(gStain);
        }
        Band band = new Band(bStart, bEnd, name, stain);
        
        //get Array of bands
        ArrayList<Band> bands = this.cytobands.get(chrIndex);
        if(bands == null){
            bands = new ArrayList<Band>();
            cytobands.put(chrIndex, bands);
        }
        bands.add(band);
	}

	//load gene track file
	public void loadGeneTrack(String file){
		try{
			File f;
			if(isUserDefinedBuild){
				f = new File(file);
			}else{
				f = new File(dataPath(file));
			}			 
			//get file size
			_total_data = f.length();
			_current_data = 0;
			_current_load_message = "loading Gene:"+file+" ...";

			//Get the file from the data folder
			BufferedReader reader = createReader(file);
			//Loop to read the file one line at a time
			String line = null;
			while((line = reader.readLine()) != null) {
				_current_data += line.length();

				String[] contents = split(line, TAB);
				// println("loadGeneTrack():debug:"+Arrays.toString(contents));
				if (contents[0].startsWith("#") ){  //header line
            		println("geneTrack header line:" + line);
        		} else {
            		if (contents[0] != null) {
                		//parse data
                		parseGeneTrack(contents);
            		} else {
                		println("string content is null");
            		}
        		}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	//parse genetrack file
	public void parseGeneTrack(String[] s){
		Gene gene = new Gene(s);
		if(gene.chrIndex != -1){
			ArrayList<Gene> geneList =  geneChrMap.get(gene.chrIndex);
			if(geneList == null){
				geneList = new ArrayList<Gene>();
				geneChrMap.put(gene.chrIndex, geneList);
			}

			//check if the gene already exisit
			Gene exist = checkGeneSymbol(geneList, gene);
			if(exist == null){
				//new gene
				geneList.add(gene);
			}else{
				//exist
				exist.addTranscript(gene);
				//println("debug: gene "+exist.geneSymbol+" already exist! -- "+exist.transcripts.size());
			}
		}
	}
	// checks if a Gene with the same GeneSymbol already exist
	public Gene checkGeneSymbol(ArrayList<Gene> array, Gene g){
		for(int i = 0; i<array.size(); i++){
			Gene gene = array.get(i);
			if(gene.geneSymbol.equals(g.geneSymbol)){
				return gene;
			}
		}
		return null;
	}

	//determine the size of chromosome by looking at the end positions of all the events.
	public void findMaxChrLength(){
		maxLength = new int[_CHROMOSOMES.length];
        for (int i = 0; i < events.size(); i++) {
            Event e = events.get(i);
            int end = e.getEndPosition();
            int chrIndex = e.getChrIndex();
            if (maxLength[chrIndex] < end) {
                maxLength[chrIndex] = end;
            }
        }
        System.out.println("Debug: max Length=" + Arrays.toString(maxLength));
	}
	//sort ArrayList in HashMap of events and genes
	public void sortArrayList(){
		//eventChrMap
		for (Iterator it = eventChrMap.values().iterator(); it.hasNext();) {
            ArrayList<Event> eventArray = (ArrayList<Event>) it.next();
            Collections.sort(eventArray);
        }

		//geneChrMap
		for (Iterator it = geneChrMap.values().iterator(); it.hasNext();) {
            ArrayList<Gene> geneArray = (ArrayList<Gene>) it.next();
            Collections.sort(geneArray);
        }

        println(" ---- end of sortArrayList()");
	}

	//go through all events to see if they hit any genes
	public void checkEventHitGene(){
		for(int i=0; i<events.size(); i++){
			Event e = events.get(i);
			int eStart = e.start;
			int eEnd = e.end;
			ArrayList<Gene> chrGenes = (ArrayList<Gene>) geneChrMap.get(e.chrIndex);
			for(int j = 0; j<chrGenes.size(); j++){
				Gene g = chrGenes.get(j);
				int gStart = g.tsp;
				int gEnd = g.tep;
				//consider promoter region
				if(g.strand){
					//forward strand
					gStart = max(0, gStart - _PROMOTER_LENGTH);
				}else{
					//reverse strand
					gEnd = gEnd +_PROMOTER_LENGTH;
				}

				//check where the event is relative to the gene location.		
				if (eStart < gStart) {
                    if (eEnd < gStart) {
                        // next gene
                    } else if (eEnd <= gEnd) {
                        //left cut
                        g.isHit = true;
                        g.addEvent(e, "left");
                    } else if (eEnd > gEnd) {
                        //whole g
                        g.isHit = true;
                        g.addEvent(e, "all");
                    } else {
                        println("Debug: checkIfHit() error");
                    }
                } else if (eStart <= gEnd) {
                    if (eEnd >= gEnd) {
                        //right cut
                        g.isHit = true;
                        g.addEvent(e, "right");
                    } else if (eEnd < gEnd) {
                        //mid cut
                        if (_ONLY_EXOMES) {
                            if (g.containsExons(eStart, eEnd)) {
                                g.isHit = true;
                                g.addEvent(e, "middle");
                            } else {
                                //introns
                            }
                        } else {
                            g.isHit = true;
                            g.addEvent(e, "middle");
                        }

                    } else {
                        println("Dubuyg: checkIfHit() error2");
                    }
                } else if (eStart > gEnd) {
                    //out of range
                } else {
                    println("Debug: checkIfHit() error 3:e:" );//+ eStart % rounding + "-" + eEnd % rounding + "  g:" + gStart % rounding + "-" + gEnd % rounding + "  type=" + e.svIndex + " " + g.getGeneSymbol());
                }
			}
		}
		println("---- end of checkEventHitGene()");
	}

	public void groupGenes(){
		//per chromosome
		for (int i = 0; i<_CHROMOSOMES.length; i++){
			GeneGroup geneGroup = null;
			ArrayList<Gene> genes = (ArrayList<Gene>)geneChrMap.get(i);
			ArrayList<GeneGroup> geneGroupArray = new ArrayList<GeneGroup>();
			//go through the genes
			for(int j = 0; j<genes.size(); j++){
				Gene g = genes.get(j);

				if(geneGroup == null){
					//first gene in the chromosome
					geneGroup = new GeneGroup(g);
					geneGroupArray.add(geneGroup);
				}else{
					String[] status = g.getStatus();
					//check if this gene status is the same as the current geneDisplay

					if(geneGroup.hasSameStatus(status)){
						geneGroup.addGene(g);
					}else{
						//create new gene group
						geneGroup = new GeneGroup(g);
						geneGroupArray.add(geneGroup);
					}
				}
			}
			//record arrayList
			geneGroupArrays[i] = geneGroupArray;
		}
	}
	//clear array content
	public void resetGeneGroup(){
		affectedGeneGroup.clear();
		unaffectedGeneGroup.clear();
	}
	//check all the loaded event types and assign colors
	public void setupSV_EVENTS(){
		_EVENTS = _FOUND_EVENTS.toArray(new String[_FOUND_EVENTS.size()]);
		 _VALID_EVENT = new boolean[_EVENTS.length];
	    Arrays.fill(_VALID_EVENT, Boolean.TRUE); //set all the flag to TRUE

	    //all variation of unit plot types
		int typeCount = _EVENTS.length * (_CONSEQUENCES.length);
		// println("debug: type count ="+typeCount);
		_UNIT_PLOT_TYPES = new String[typeCount];
		for(int i = 0; i< _EVENTS.length; i++){
			for(int j = 0; j<_CONSEQUENCES.length; j++){
				int index = (i*(_CONSEQUENCES.length)+j);
				_UNIT_PLOT_TYPES[index] = _EVENTS[i]+"_"+_CONSEQUENCES[j];
			}
		}

	}
	//loading optional csv
	public void loadOptionalCSV(){
		if(selectedOptional != null){
			hasOptionalTable = true;
			File file = selectedOptional;
			try{
				//get file size
				_total_data = file.length();
				_current_data = 0;
				_current_load_message = "loading csv:"+file.getName()+" ...";
				//Get the file from the data folder
				BufferedReader reader = createReader(file);
				//Loop to read the file one line at a time
				String line = null;
				while((line = reader.readLine()) != null) {
					_current_data  += line.length();
					String[] contents = split(line, ',');
            		if (contents[0] != null) {
                		//parse data
                		parseCSV(contents);
            		} else {
                		println("string content is null");
            		}
				}
			}catch(IOException e){
				e.printStackTrace();
			}

		}
		// //debug
		// ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(optionalTable.values());
		// Collections.sort(categories);
		// for(int i = 0; i<categories.size(); i++){
		// 	OptionalCategory oc = categories.get(i);
		// 	println(oc.name+" -- "+ oc.genes.size()+" genes");
		// }


		// Iterator ite = optionalTable.entrySet().iterator();
		// while(ite.hasNext()){
		// 	Map.Entry entry = (Map.Entry)ite.next();
		// 	String category = (String) entry.getKey();
		// 	OptionalCategory group = (OptionalCategory) entry.getValue();
		// 	println("debug: "+category+" --- "+group.genes.size()+" genes");
		// }

	}

	public void parseCSV(String[] s){
		String ensemblID = s[0].trim();
		String category = s[1].trim();

		if(ensemblID.startsWith("#")){
			//header line
			_optional_category = category;
		}else{
			//fdind maching gene
			Gene match = (Gene) affectedGeneMap_ensembl.get(ensemblID);
			if(match == null){
				//no match found
			}else{
				//if match found, store the data, create group of them
				if(optionalTable == null){
					//first time
					optionalTable = new HashMap<String, OptionalCategory>();
				}
				//get arraylist
				OptionalCategory list = (OptionalCategory)optionalTable.get(category);
				if(list == null){
					list = new OptionalCategory(category);
					optionalTable.put(category, list);
				}
				//add to the list
				list.genes.add(match);
			}
		}
	}


}
class Event implements Comparable{
	int start, end;
	int chrIndex, svIndex;

	//constructor
	Event(int chrIndex, int svIndex, int s_pos, int e_pos){
		start = s_pos;
		end = e_pos;
		this.chrIndex = chrIndex;
		this.svIndex = svIndex;
	}

	public int getStartPosition(){
		return start;
	}
	public int getEndPosition(){
		return end;
	}
	public int getChrIndex(){
		return chrIndex;
	}



	 public int compareTo(Object obj) {
        Event e = (Event) obj;
        if(chrIndex < e.getChrIndex()){
        	return -1;
        }else if(chrIndex > e.getChrIndex()){
        	return 1;
        }else{
	        if (start < e.getStartPosition()) {
	            return -1;
	        } else if (start > e.getStartPosition()) {
	            return 1;
	        }
	        return 0;
        }
    }
}
long _total_data = 0;
long _current_data = 0;
String _current_load_message = "";


class FileLoader implements Runnable{
	public void run(){
		if(isUserDefinedBuild){
			_SPECIES = "User_defined";
			_LINK = "http://www.ensembl.org/";

			_CHROMOSOMES = getChromosomeInfo(); //reom cytoband file
		}else{
			if(isHuman){
		        _CHROMOSOMES = _H_CHROMOSOMES;
		        // _EVENTS = _H_EVENTS;
		        if(isFirstBuild){
		            _GENETRACK_FILE = "human_36_gene.txt";
		            _CYTOBAND_FILE = "human_36_cytoband.txt";
		            _GO_FILE = "human_36_goterm.txt";
		            _SPECIES = "Homo_sapiens";
		            _LINK = "http://may2012.archive.ensembl.org/";


		        }else{
		            _GENETRACK_FILE = "human_37_gene.txt";
		            _CYTOBAND_FILE = "human_37_cytoband.txt";
		            _GO_FILE = "human_37_goterm.txt";
		            _SPECIES = "Homo_sapiens";
		            _LINK = "http://www.ensembl.org/";
		        }
		    }else{
		        _CHROMOSOMES = _M_CHROMOSOMES;
		        // _EVENTS = _M_EVENTS;
		        if(isFirstBuild){
		            _GENETRACK_FILE = "mouse_37_gene.txt";
		            _CYTOBAND_FILE = "mouse_37_cytoband.txt";
		            _GO_FILE = "mouse_37_goterm.txt";
		            _SPECIES = "Mus_musculus";
		            _LINK = "http://may2012.archive.ensembl.org/";
		        }else{
		            _GENETRACK_FILE = "mouse_38_gene.txt";
		            _CYTOBAND_FILE = "mouse_38_cytoband.txt";
		            _GO_FILE = "mouse_38_goterm.txt";
		            _SPECIES = "Mus_musculus";
		            _LINK = "http://www.ensembl.org/";
		        }
		    }
			
		}
	    //mouse data
	    // _VALID_EVENT = new boolean[_EVENTS.length];
	    // Arrays.fill(_VALID_EVENT, Boolean.TRUE); //set all the flag to TRUE
	    // _DATA_FILE = selectedGVF;

	    
	    _selected_genes = new ArrayList<Gene>();
	    _selected_genes_by_go = new ArrayList<Gene>();

	    data = new DataSet();
	    data.loadData();            //loads external files
	    data.sortArrayList();       //sort Gene and Event array in HashMap based on their start position
	    data.findMaxChrLength();    //find the very last position of event per chromosome
	    data.checkEventHitGene();   //go through all events and see if they affect any gene
	    data.groupGenes();


	    geneView = createGraphics(_WIDTH, _HEIGHT);
	    updateGeneView();
	    legendView = createGraphics(legend_w, legend_h);
	    initLegendView();
	    assignDisplayPositions(); //assign initial position
	    initBubbles();

	    //affected and unaffected GeneGroup should be sorted
	    println("****debug: affected ="+data.affectedGeneGroup.size()+"  unaffected="+data.unaffectedGeneGroup.size());
	    //index Gene
	    data.indexAffectedGene();
	    data.loadGoTerms();         //loading go terms
	    data.sortGoTerms();
	    // data.createGeneToGoTable();

	    //optional talbe
	    data.loadOptionalCSV();

	    //debug
	    println("number of all the go terms ="+data.all_goMap.size());
	    Iterator ite = data.all_goMap_type.entrySet().iterator();  // Get an iterator
	    while (ite.hasNext()) {
			Map.Entry me = (Map.Entry)ite.next();
			String type = (String) me.getKey();
			ArrayList<Go> array = (ArrayList<Go>)me.getValue();
			println("goTable:"+type+" --- has "+array.size()+" Go terms");
	    }

	    //update LegendView
	    //reinitialize based on if the option table is loaded
	    reinitialiseLegendView();

	    //initiate AniSequence
	    updateLegendView();
	    showLegendView();
	    updateBubbles();


	    //preprocessing
	    //check for multi events
	    checkMultiEvents(); 


	    //end of loading
	    isLoadingData = false;
	    dataLoaded = true;
	}


	//preprocessing
	public void checkMultiEvents(){

		int multieventcounter = 0;

		for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
            GeneGroup gg = data.affectedGeneGroup.get(i);
            for(int j = 0; j< gg.genes.size(); j++){
                Gene gene = gg.genes.get(j);
                if(gene.affectingEvents.size() > 1){
		            int firstEventIndex = gene.affectingEvents.get(0).svIndex;
		            for(int k = 1; k<gene.affectingEvents.size(); k++){
		                int newIndex = gene.affectingEvents.get(k).svIndex;
		                if(firstEventIndex != newIndex){
		                    gene.isMultiEvent = true;
		                    multieventcounter ++;
		                    break;
		                }
		            }
		        }

		        if(gene.isMultiEvent){
		        	gg.isMultiEvent = true;
		        }
            }
        }
        println("number of multi event = "+multieventcounter);
	}

	public String[] getChromosomeInfo(){
		ArrayList<String> chr_names = new ArrayList<String>();
		String[] lines = loadStrings(_CYTOBAND_FILE);
		for(String line :lines){
			String[] s = split(line, TAB);
			if(s[0].startsWith("#")){
				//ignore
			}else{
				String chr = s[0].trim().toLowerCase();
				if(!chr_names.contains(chr)){
					chr_names.add(chr);
				}
			}
		}
		String[] results = new String[chr_names.size()];
		results = chr_names.toArray(results);
		println("Debug: chr:"+Arrays.toString(results));
		// exit();
		return results;
	}
}

int _margin = 10;
int title_x = _margin*3;
int title_y = _margin*3;
int title_height = _margin*3;

int view_mode_x;
int view_mode_y = title_y;


int label_x = _margin;
int label_y = title_height + _margin*2;
int label_width = _margin * 5;
int[] label_y_pos; //keep track of label positions
int[] label_y_pos_expanded; //expanded view
int[] unit_plot_label_y;//keep track of label positions for unit plot

int chr_x = label_x+label_width+_margin;
int chr_x_end = _WIDTH - _margin;
int chr_y = label_y;


int _bubble_d = _margin;
int _connector = _bubble_d/2;
int _bubble_gap = _margin;
int _bubble_left_end = _WIDTH -_margin*25; //how far bubble goes to left
int _bubble_alpha = 255;

String[] unitplot2_labels;


Rectangle[] chromosomeDisplaySpace; //keep track of chromosome length for bp display


// Assign positions for each view
public void assignDisplayPositions(){
	updateGeneGroupArray(); //sort GeneGroup to affected and unaffected
	createBubbles();
	assignGeneBlockViewPositions();
	initBpViewPositions(); //this value does not change dynamically
}

//sorts to arrays based on if the GeneGroup is affected
public void updateGeneGroupArray(){
	data.resetGeneGroup(); //reset
	for(int i = 0; i < data.geneGroupArrays.length; i++){
		//per chromosome
		ArrayList<GeneGroup> geneGroups = data.geneGroupArrays[i];
		for(int j = 0; j<geneGroups.size(); j++){
			GeneGroup gg = geneGroups.get(j);
			//check status
			if(gg.status != null){
					data.affectedGeneGroup.add(gg); //add to ArrayList
			}else{
				//unaffected genes
				data.unaffectedGeneGroup.add(gg); // add to ArrayList
			}
		}
	}
}
//create bubble for all affected GeneGroup
public void createBubbles(){
	for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
		GeneGroup gg = data.affectedGeneGroup.get(i);
		for(int j = 0; j< gg.genes.size(); j++){
			Gene gene = gg.genes.get(j);
			gene.bubbles = new ArrayList<Bubble>();
			for(int k = 0; k<gene.affectingEvents.size(); k++){
				Event event = gene.affectingEvents.get(k);
				//sort bubble by sv type
				String sv = _EVENTS[event.svIndex];
				String consequence = gene.eventConsequence.get(k);
				String type = sv+"_"+consequence;
				Bubble b = new Bubble(gene, event, type);
				//record Bubble in Gene
				gene.bubbles.add(b);
				//hashmap for unit plot
				ArrayList<Bubble> bubbles =(ArrayList<Bubble>) data.bubbleBySvType.get(type);
				if(bubbles == null){
					bubbles = new ArrayList<Bubble>();
					data.bubbleBySvType.put(type, bubbles);
				}
				bubbles.add(b);
			}
		}
	}
}
//BP positions are assigned only once
public void initBpViewPositions(){
	//BpPlot coordinates -------------------------------
	//find chromosome display space
	chromosomeDisplaySpace = new Rectangle[_CHROMOSOMES.length];
	for(int i = 0; i< _CHROMOSOMES.length; i++){
		int chr_start = 0; 
		int chr_end = data.maxLength[i];
		int display_x1 = round(map(chr_start, 0, data.maxLength[0], chr_x, _bubble_left_end));
		int display_x2 = round(map(chr_end, 0, data.maxLength[0], chr_x, _bubble_left_end)); 
		int display_w = display_x2 - display_x1;
		chromosomeDisplaySpace[i] = new Rectangle(display_x1, label_y_pos[i], display_w, _bubble_d);
	}
	//per chromosome
	int running_x = chr_x;
	int running_y = label_y;  // use same y coordinates as GeneBlock view
	for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
		GeneGroup gg = data.affectedGeneGroup.get(i);
		for(int j = 0; j< gg.genes.size(); j++){
			Gene gene = gg.genes.get(j);
			int gene_start = gene.tsp;
			int gene_end = gene.tep;
			float gene_mid = (gene_end+gene_start)/2;
			int chr_index = gene.chrIndex;
			int chr_start = 0; 
			int chr_end = data.maxLength[chr_index];
			Rectangle chr_rect = chromosomeDisplaySpace[chr_index];
			int x_pos = round(map(gene_mid, chr_start, chr_end, chr_rect.x, chr_rect.x+chr_rect.width));

			//set posisiton for all bubbles
			for(int k = 0; k< gene.bubbles.size(); k++){
				Bubble b = gene.bubbles.get(k);
				b.bpView = new PVector(x_pos, chr_rect.y);
			}
		}
	}
}

//called when the view changes
public void updateGeneGroups(){
    if(_VIEW_MODE == 0){
        assignGeneBlockViewPositions();
    }else if(_VIEW_MODE == 1){
        assignUnitPlotViewPositions();
    }else if(_VIEW_MODE == 2){
    	//position is already in memory
    }else if(_VIEW_MODE == 3){
        assignGeneExpandedViewPositions();
    }else if(_VIEW_MODE == 4){
    	assignUnitPlot2ViewPositions();
    }
}
//dynamically set positions
public void assignGeneBlockViewPositions(){
	// println("assignGeneBlockViewPositions----");
	//GeneBlock view -----------------------------------
	int running_x = chr_x; 
	int running_y = label_y;
	label_y_pos = new int[_CHROMOSOMES.length];
	for(int i = 0; i < data.geneGroupArrays.length; i++){
		//per chromosome
		label_y_pos[i] = running_y;
		running_x = chr_x;
		GeneGroup previousUnaffected = null;// check if the previous is unaffected
		ArrayList<GeneGroup> geneGroups = data.geneGroupArrays[i];
		for(int j = 0; j<geneGroups.size(); j++){
			GeneGroup gg = geneGroups.get(j);
			//check status
			if(gg.status != null){
				if(checkValidGeneGroup(gg.status)){
					Rectangle rect = new Rectangle(running_x, running_y, _bubble_d, _bubble_d);
					gg.rect  = rect;
					for(int k = 0; k < gg.genes.size(); k++){
						Gene gene = gg.genes.get(k);
						ArrayList<Bubble> bubbles = gene.bubbles;
						//assgin position for the bubbles representing the gene group
						for(Bubble b:bubbles){
							b.newPosition = new PVector(running_x, running_y);
						}
					}
					running_x += _bubble_d;
					previousUnaffected = null;
					gg.isShown = true;
				}else{
					//hidden chromosome
					gg.isShown = false;
				}
			}else if(gg.status == null){
				//unaffected genes
				if(previousUnaffected == null){
					Rectangle rect = new Rectangle(running_x, running_y, _connector, _bubble_d);
					gg.rect = rect;
					running_x += _connector; //shorter than full length
					previousUnaffected = gg;
				}else{
					//stack unaffected ones
					gg.rect = previousUnaffected.rect;
				}	
			}
			//check the current running_x
			if(running_x >= _bubble_left_end){	
				running_x = chr_x;
				running_y += _bubble_d;	//go to next line
			}
		}
		//next chromosome
		running_y += _bubble_d+_bubble_gap;
	}
}
public void assignUnitPlotViewPositions(){
	//UnitPlot coordinates ------------------------------
	//per sv type
	int running_x = chr_x;
	int running_y = label_y;
	unit_plot_label_y = new int[_EVENTS.length];
	int currentEventCounter = 0;
	unit_plot_label_y[0] = running_y; //set the first one

	// int midDeletionCounter  = 0;
	for(int i = 0; i < _UNIT_PLOT_TYPES.length; i++){
		running_x = chr_x;
		String type = _UNIT_PLOT_TYPES[i];
		//check for label y position
		if(type.startsWith(_EVENTS[currentEventCounter])){
			//same
		}else{
			currentEventCounter ++;
			unit_plot_label_y[currentEventCounter] = running_y;
		}
		ArrayList<Bubble> bubbles = (ArrayList<Bubble>) data.bubbleBySvType.get(type);
		if(bubbles == null){
			//no bubble for this type of event
			//println("no bubble for "+type +" i ="+i);
		}else{
			String prevGeneSymbol ="";
			for(int j = 0; j<bubbles.size(); j++){
				Bubble b = bubbles.get(j);
				String newGeneSymbol = b.parentGene.geneSymbol;
				//check if it overlaps with the previous one
				boolean isOverlapping = false;
				if(prevGeneSymbol.equals("")){
					//first time
					prevGeneSymbol = newGeneSymbol;
				}else if(newGeneSymbol.equals(prevGeneSymbol)){
					prevGeneSymbol = newGeneSymbol;
					isOverlapping = true;
				}else{
					prevGeneSymbol = newGeneSymbol;
				}
				//assign position
				if(isOverlapping){
					//previous position
					b.newPosition = new PVector(running_x, running_y);
				}else{
					//check the current running_x
					if(running_x >= _bubble_left_end){
						//go to next line
						running_x = chr_x;
						running_y += _bubble_d;
					}
					running_x += _bubble_d;
					b.newPosition = new PVector(running_x, running_y);
				}
			}
			//next type
			running_y += _bubble_d +_bubble_gap;
		}
	}
}
public void assignUnitPlot2ViewPositions(){
	//create hashmap table first
	HashMap<String, ArrayList<Gene>> unitplot2 = new HashMap<String, ArrayList<Gene>>(); //status, array
	for(int i = 0; i<data.affectedGeneGroup.size(); i++){
		GeneGroup gg = data.affectedGeneGroup.get(i);
		ArrayList<Gene> genes = gg.genes;
		for(int j = 0; j<genes.size(); j++){
			Gene gene = genes.get(j);
			ArrayList<String> status = new ArrayList<String>();
			status.addAll(gene.status);
			if(status.size() == 1){
				//one event
				String svName = status.get(0);
				ArrayList<Gene> geneArray = unitplot2.get(svName);
				if(geneArray == null){
					geneArray = new ArrayList<Gene>();
					unitplot2.put(svName, geneArray);
				}
				geneArray.add(gene);
			}else{
				//multiple event
				String svName = "multi_events";
				ArrayList<Gene> geneArray = unitplot2.get(svName);
				if(geneArray == null){
					geneArray = new ArrayList<Gene>();
					unitplot2.put(svName, geneArray);
				}
				geneArray.add(gene);
			}
		}
	}

	//debug
	Iterator ite = unitplot2.entrySet().iterator();

	while(ite.hasNext()){
		Map.Entry entry = (Map.Entry)ite.next();
		String svType = (String)entry.getKey();
		ArrayList<Gene> genes = (ArrayList<Gene>)entry.getValue();
		println(svType+":"+genes.size()+" genes");
	}



	//UnitPlot2 coordinates ------------------------------
	//per sv type
	int running_x = chr_x;
	int running_y = label_y;
	unit_plot_label_y = new int[unitplot2.size()];
	unitplot2_labels = new String[unitplot2.size()];
	int currentEventCounter = 0;
	// unit_plot_label_y[0] = running_y; //set the first one
	for(int i = 0; i < _UNIT_PLOT_TYPES.length; i++){
		String type = _UNIT_PLOT_TYPES[i];
		//check if this type is in the table
		ArrayList<Gene> geneArray = (ArrayList<Gene>)unitplot2.get(type);
		if(geneArray == null){
			//no gene of this event type
			println("-- no match:"+type);
		}else{
			println("type:"+type);
			running_x = chr_x;
			unit_plot_label_y[currentEventCounter] = running_y;
			unitplot2_labels[currentEventCounter] = type; 
			//iterate though the array
			for(int j = 0; j<geneArray.size(); j++){
				Gene gene = geneArray.get(j);
				//assign position to all bubbles
				ArrayList<Bubble> bubbles = gene.bubbles;

				for(Bubble b: bubbles){
					b.newPosition = new PVector(running_x, running_y);
				}
				//check the current running_x
				running_x += _bubble_d;
				if(running_x >= _bubble_left_end){
					//go to next line
					running_x = chr_x;
					running_y += _bubble_d;
				}
			}
			//next type
			running_y += _bubble_d +_bubble_gap;
			currentEventCounter ++;
		}
	}
	//mixed one
	ArrayList<Gene> geneArray = (ArrayList<Gene>)unitplot2.get("multi_events");
	if(geneArray == null){
		//no gene of this event type
	}else{
		running_x = chr_x;
		unit_plot_label_y[currentEventCounter] = running_y;
		unitplot2_labels[currentEventCounter] = "multi_events"; 
		//iterate though the array
		for(int j = 0; j<geneArray.size(); j++){
			Gene gene = geneArray.get(j);
			//assign position to all bubbles
			ArrayList<Bubble> bubbles = gene.bubbles;

			for(Bubble b: bubbles){
				b.newPosition = new PVector(running_x, running_y);
			}
			//check the current running_x
			running_x += _bubble_d;
			if(running_x >= _bubble_left_end){
				//go to next line
				running_x = chr_x;
				running_y += _bubble_d;
			}
		}
		//next type
		running_y += _bubble_d +_bubble_gap;
	}
}

public void assignGeneExpandedViewPositions(){
	int running_x = chr_x;
	int running_y = label_y;
	label_y_pos_expanded = new int[_CHROMOSOMES.length];
	for(int i = 0; i < data.geneGroupArrays.length; i++){
		//per chromosome
		label_y_pos_expanded[i] = running_y;
		running_x = chr_x;
		ArrayList<GeneGroup> geneGroups = data.geneGroupArrays[i];
		GeneGroup previousUnaffected = null;// check if the previous is unaffected
		for(int j = 0; j<geneGroups.size(); j++){
			GeneGroup gg = geneGroups.get(j);		
			//check status
			if(gg.status != null){
				if(checkValidGeneGroup(gg.status)){
					int startOfExpanded = running_x;
					for(int k = 0; k < gg.genes.size(); k++){
						Gene gene = gg.genes.get(k);
						ArrayList<Bubble> bubbles = gene.bubbles;
						for(int m = 0; m <bubbles.size(); m++){
							Bubble b = bubbles.get(m);
							b.newPosition = new PVector(running_x, running_y);
						//check the current running_x
						}
						//increment per bubble
						running_x += _bubble_d;
						if(running_x >= _bubble_left_end){
							//go to next line
							running_x = chr_x;
							running_y += _bubble_d;
						}
					}
					gg.rect = new Rectangle(startOfExpanded, running_y, (running_x- startOfExpanded), _bubble_d);
					previousUnaffected = null;
					gg.isShown = true;
				}else{
					gg.isShown = false;
				}

			}else{
				//unaffected genes
				if(previousUnaffected == null){
					Rectangle rect = new Rectangle(running_x, running_y, _connector, _bubble_d);
					gg.rect = rect;
					running_x += _connector; //shorter than full length
					previousUnaffected = gg;
				}else{
					//stack unaffected ones
					gg.rect = previousUnaffected.rect;
				}
			}
		}
		//next chromosome
		running_y += _bubble_d+_bubble_gap;
	}
}

public boolean checkValidGeneGroup(String[] status){
	//check if at least one of events is valid/true
	boolean isValidGeneGroup  = false;
	for(int m = 0; m < _EVENTS.length; m++){
		if(_VALID_EVENT[m]){
			// if(m == 1){
			// 	println("***  inversion event:"+Arrays.toString(status));
			// }
			for(int k = 0; k < status.length; k++){
				if(status[k].startsWith(_EVENTS[m])){
					//valid
					isValidGeneGroup = true;

					return isValidGeneGroup;
				}
			}
		}
	}
	return isValidGeneGroup;
}



public void draw(){
    if (saveFrame) {
        println("start of saving");
        beginRecord(PDF, "Plot-####.pdf");
    }    
    background(color_main_background);
    ellipseMode(CORNER);
    rectMode(CORNER);

    if(dataLoaded){
        if(animation.isPlaying()|| animation_geneView.isPlaying() || animation_legendView.isPlaying()){
            // println("anaimation playing:"+animation.getSeek() + " "+animation_geneView.getSeek()+" "+ animation_legendView.getSeek() );
            if(animation.getSeek() == 1.0f  && animation_geneView.getSeek() == 1.0f && animation_legendView.getSeek() == 1.0f){
                animation.pause();
                animation_geneView.pause();
                animation_legendView.pause();
                noLoop();
                // println("Stop looping");
            }
        }else if (animation.isEnded() ){//&& animation_geneView.isEnded() && animation_legendView.isEnded()){
            // println("animation ended");
            noLoop();
        }else{
            // noLoop();
            if(animation.getSeek() == 1.0f  && animation_geneView.getSeek() == 1.0f && animation_legendView.getSeek() == 1.0f){
                animation.pause();
                animation_geneView.pause();
                animation_legendView.pause();
                noLoop();
                // println("Stop looping");
            }
            // println("draw() else:"+animation.getSeek()+" "+animation_geneView.getSeek()+" "+ animation_legendView.getSeek());
        }

        drawBaseLine(); //draw baseline, un affected genes

        //highlight selected genes by go term
        for(int i = 0; i < _selected_genes_by_go.size(); i++){
            Gene gene = _selected_genes_by_go.get(i);
            if(gene.bubbles != null){
                for(int k = 0; k < gene.bubbles.size(); k++){
                    Bubble b = gene.bubbles.get(k);
                    if(_VALID_EVENT[b.event.svIndex]){
	                    b.highlight_soft();
	                }
                } 
            }
        }


        //draw bubbles
        for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
            GeneGroup gg = data.affectedGeneGroup.get(i);
            if(gg.isShown){
                if(_VIEW_MODE == 0){ 
                    //draw one overall background
                    gg.genes.get(0).bubbles.get(0).drawBackground();
                }
                //draw individual bubbles
                for(int j = 0; j< gg.genes.size(); j++){
                    Gene gene = gg.genes.get(j);
                    gene.isSelectable = false;
                    //0= GeneBlock, 1=UnitPlot, 2=BpView, 3 =Expanded
                    for(int k = 0; k < gene.bubbles.size(); k++){
                        Bubble b = gene.bubbles.get(k);
                        if(_VALID_EVENT[b.event.svIndex]){
                        	//valid event
                        	gene.isSelectable = true;

                            if(_VIEW_MODE == 0){
                                // if(animation.isPlaying()){
                                //     // b.drawBackground();
                                // }
                            }else if(_VIEW_MODE == 1){
                                if(animation.isPlaying()){
                                    //transition to unit plot
                                    b.drawBackground();
                                }else{
                                    if(k == 0){
                                        b.drawBackground();
                                    }
                                }
                            }else if(_VIEW_MODE == 2){
                                if(animation.isPlaying()){
                                    //transition to BP
                                }else{
                                }
                            }else if(_VIEW_MODE == 3){
                                //expanded view
                                if(k== 0){
                                    b.drawBackground();
                                }
                            }
                            //drawing bubble
                            b.render();
                        }
                    }

                }    
            }else{
            	for(int j = 0; j< gg.genes.size(); j++){
            	    Gene gene = gg.genes.get(j);
            	    gene.isSelectable = false;
            	    //0= GeneBlock, 1=UnitPlot, 2=BpView, 3 =Expanded
            	    for(int k = 0; k < gene.bubbles.size(); k++){
            	        Bubble b = gene.bubbles.get(k);
            	        if(_VALID_EVENT[b.event.svIndex]){
            	        	//valid event
            	        	gene.isSelectable = true;
            	        }
            	    }
            	}

            }
             //end of if
        }

        //draw multievents
        // println("debug_____drawMultievents!!!");
        if(_show_multi_event){
	        for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
	            GeneGroup gg = data.affectedGeneGroup.get(i);
	            if(gg.isShown){
	                for(int j = 0; j< gg.genes.size(); j++){
	                    Gene gene = gg.genes.get(j);
	                    if(_VIEW_MODE == 0){
	                        //GeneBlock
	                        if(gene.isMultiEvent){
	                        	gene.renderMultiEvent();
	                        }
	                    }else if(_VIEW_MODE == 1){
	                    	//UnitPlot
	                        if(gene.isMultiEvent){
	                        	gene.renderMultiEvent_all();
	                        }
	                    }else if(_VIEW_MODE == 2){
	                    	//BpView
	                        if(animation.isPlaying()){
	                        }else{
	                        }
	                    }else if(_VIEW_MODE == 3){
	                        //expanded view
	                        if(gene.isMultiEvent){
	                        	gene.renderMultiEvent();
	                        }
	                    }       
	                }    
	            }
	        }
        }

        if(hasOptionalTable && selected_genes_by_category != null){
            //highlight selected genes by category
            for(int i = 0; i < selected_genes_by_category.size(); i++){
                Gene gene = selected_genes_by_category.get(i);
                if(gene.bubbles != null){
                    for(int k = 0; k < gene.bubbles.size(); k++){
                        Bubble b = gene.bubbles.get(k);
                        if(_VALID_EVENT[b.event.svIndex]){
                            b.highlight_by_category();
                        }
                    }
                    
                }
            }
        }

        // //highlight selected genes by go term
        // for(int i = 0; i < _selected_genes_by_go.size(); i++){
        //     Gene gene = _selected_genes_by_go.get(i);
        //     if(gene.bubbles != null){
        //         for(int k = 0; k < gene.bubbles.size(); k++){
        //             Bubble b = gene.bubbles.get(k);
        //             b.highlight_soft();
        //         } 
        //     }
        // }

        //highlight selected genes
        for(int i = 0; i < _selected_genes.size(); i++){
            Gene gene = _selected_genes.get(i);
            if(gene.bubbles != null){
                for(int k = 0; k < gene.bubbles.size(); k++){
                    Bubble b = gene.bubbles.get(k);
                    // if(_VALID_EVENT[b.event.svIndex]){
                    	b.highlight();
                	// }
                }
                
            }
        }
        drawLabels(); //draw Labels
        drawTitle();

        drawViewModeBtns();

        if (saveFrame) {
            drawLegendViewPDF();
            drawGeneViewdPDF();
        }else{
            //show legend view
            image(legendView, legendView_x, legendView_y); //when the data is loaded
            //show gene view
            image(geneView, 0, geneView_y);
        }

    }else{ //choosing file, start up page
        if(isLoadingData){
            drawLoadingPage();
        }else{
            drawStartUpPage();
        }
    }

    if (saveFrame) {
        // this.control.draw();
        endRecord();
        saveFrame = false;
        println("end of saving");
    }
}

//how each bubble is drawn
public void _drawCircle(float x1,float  y1, int w, int h){
	ellipseMode(CORNER);
    ellipse(x1, y1, w, h);
}
public void _drawHalfCircle(float x1, float y1, int w, int h, boolean isLeft){
	float ang1 = isLeft ? PI / 2 : 3 * PI / 2;
    float ang2 = isLeft ? 3 * PI / 2 : 5 * PI / 2;
    arc(x1, y1, w, h, ang1, ang2);
}
public void _drawMiddleOfCircle(float x1, float y1, int w, int h){
	float x2 = x1 +w;
	float y2 = y1 +h;
	float gx = (x1 + x2) / 2;
    float gy = (y1 + y2) / 2;
    float r = _bubble_d / 2;
    float mr = 1f;//mid line thickness
    float d = (r * 0.55f);
    float d2 = d;

    beginShape();
    vertex(gx, gy - r);
    bezierVertex(gx + mr, gy - r, gx + mr, gy - r + d - d2, gx + mr, gy - r + d);
    vertex(gx + mr, gy + r - d);
    bezierVertex(gx + mr, gy + r - d + d2, gx + mr, gy + r, gx, gy + r);
    bezierVertex(gx - mr, gy + r, gx - mr, gy + r - d + d2, gx - mr, gy + r - d);
    vertex(gx - mr, gy - r + d);
    bezierVertex(gx - mr, gy - r + d - d2, gx - mr, gy - r, gx, gy - r);
    endShape();
}
public void _drawMultiEvent(float x1, float y1, int w, int h){
	float ang1 = 3*PI/2;
	float ang2 = TWO_PI;
	float ang3 = PI/2;
	float ang4 = PI;
	arc(x1, y1, w, h, ang1, ang2);
	arc(x1, y1, w, h, ang3, ang4);

}
//draw base lines
public void drawBaseLine(){
	noFill();
	strokeWeight(1);
	stroke(color_gray);
	strokeCap(SQUARE);
	if(_VIEW_MODE == 0){
		//GeneBlock view
		//draw unaffected GeneGroups
		for (int i = 0; i<data.unaffectedGeneGroup.size(); i++){
			GeneGroup gg = data.unaffectedGeneGroup.get(i);
			Rectangle rect = gg.rect;
			float mid_y = rect.y + (rect.height/2);
			line(rect.x, mid_y, rect.x+rect.width, mid_y);
		}

	}else if(_VIEW_MODE == 1){
		//UnitPlot
	}else if(_VIEW_MODE == 2){
		//bpView
		for (int i = 0; i<chromosomeDisplaySpace.length; i++){
			Rectangle rect = chromosomeDisplaySpace[i];
			float mid_y = rect.y + (rect.height/2);
			line(rect.x, mid_y, rect.x+rect.width, mid_y);
		}
	}else if(_VIEW_MODE == 3){
		//GeneBlock Expandedview
		//draw unaffected GeneGroups
		for (int i = 0; i<data.unaffectedGeneGroup.size(); i++){
			GeneGroup gg = data.unaffectedGeneGroup.get(i);
			Rectangle rect = gg.rect;
			float mid_y = rect.y + (rect.height/2);
			line(rect.x, mid_y, rect.x+rect.width, mid_y);
		}
	}
}

//draw labels
public void drawLabels(){
	textAlign(RIGHT, TOP);
	textSize(10);
	fill(color_dark_gray);
	if(_VIEW_MODE == 0){
		//GeneBlock view
		//chromosome label
		for (int i = 0; i<_CHROMOSOMES.length; i++){
			text(_CHROMOSOMES[i], chr_x - _margin, label_y_pos[i]);
		}
	}else if(_VIEW_MODE == 1){
		//UnitPlot
		//sv types
		for(int i = 0; i< _EVENTS.length; i++){
			String label = _EVENTS[i];
			label = label.replace("_", "\n");
			textLeading(8);
			text(label, chr_x - _margin, unit_plot_label_y[i]);
		}
	}else if(_VIEW_MODE == 2){
		//bpView
		//chromosome label
		for (int i = 0; i<_CHROMOSOMES.length; i++){
			text(_CHROMOSOMES[i], chr_x - _margin, label_y_pos[i]);
		}
	}else if(_VIEW_MODE == 3){
		//GeneBlock expanded view
		//chromosome label
		for (int i = 0; i<_CHROMOSOMES.length; i++){
			text(_CHROMOSOMES[i], chr_x - _margin, label_y_pos_expanded[i]);
		}
	}else if(_VIEW_MODE == 4){
		// //unitplot2
		// println("labels "+ unitplot2_labels.length+" :"+Arrays.toString(unitplot2_labels));
		// println("ypos "+unit_plot_label_y.length+" : "+Arrays.toString(unit_plot_label_y));
		String currentType = "test";
		for(int i = 0; i<unit_plot_label_y.length; i++){
			String label = unitplot2_labels[i];
			if(label.startsWith(currentType)){
				//do not draw
			}else{
				String[] words = label.split("_");
				currentType = words[0];
				if(currentType.startsWith("tandem")){
					currentType = "tandem_duplication";
				}else if(currentType.startsWith("copy")){
					currentType = words[0]+"_"+words[1]+"_"+words[2];
				}
				label = currentType.replace("_", "\n");
				textLeading(8);
				text(label, chr_x- _margin, unit_plot_label_y[i]);
			}

			// label = label.replace("_", "\n");
			// text(label, chr_x- _margin, unit_plot_label_y[i]);
		}
	}
}

public void drawTitle(){
	textAlign(LEFT, BOTTOM);
	textSize(15);
	fill(color_dark_gray);
	text(_DATA_FILE.getName(), title_x, title_y);
	view_mode_x = (int)textWidth(_DATA_FILE.getName()) +title_x +_margin;   //gap betwen file name and buttons
}


public void drawViewModeBtns(){
	textSize(12);
	//setup rectangle
	if(_VIEW_MODE_RECT == null){
		_VIEW_MODE_RECT = new Rectangle[_VIEW_MODE_LABEL.length];
		float running_x = view_mode_x;
		for(int i = 0; i<_VIEW_MODE_RECT.length; i++){
			String label = _VIEW_MODE_LABEL[i];
			int w = round(textWidth(label));
			_VIEW_MODE_RECT[i] = new Rectangle(round(running_x), view_mode_y-_margin*2, w, _margin*2);
			running_x += w +_margin;
		}
	}
	//draw buttons
	textAlign(LEFT, BOTTOM);
	for(int i = 0; i<_VIEW_MODE_RECT.length; i++){
		String label = _VIEW_MODE_LABEL[i];
		Rectangle r = _VIEW_MODE_RECT[i];
		if(_VIEW_MODE == 0 && i == 0){
			fill(color_pink);
		}else if(_VIEW_MODE == 1 && i == 3){
			fill(color_pink);
		}else if(_VIEW_MODE == 2 && i == 2){
			fill(color_pink);
		}else if(_VIEW_MODE == 3 && i == 1){
			fill(color_pink);
		}else{
			fill(color_gray);
		}
		text(label, r.x, r.y+r.height);
	}
}
class Gene implements Comparable{
    String geneSymbol;
    String ensemblID;
    int chrIndex;
    String chromosome;
    boolean strand;  // + true,  - false
    int tsp; //transcription start position
    int tep; //transcription end position
    boolean isCoding; //coding or non-coding
    boolean isHit = false; //false by default
    ArrayList<Event> affectingEvents = null; 
    ArrayList<String> eventConsequence = null;
    ArrayList<Transcript> transcripts = null;
    ArrayList<Bubble> bubbles = null;
    Transcript transcript;

    String[] rawData;
    HashSet<String> status;

    int start, end;

    //multievent
    boolean isMultiEvent = false;

    //whether if they can be selected
    boolean isSelectable = true;

    ArrayList<Go> goTerms;

    //constructor
    Gene(String[] s){
        ensemblID = s[0].trim();
        geneSymbol = s[1].trim();
        chromosome = s[2].toLowerCase().trim();
        chrIndex = _getChromosomeIndex(chromosome);
        strand = (s[3].trim().equals("+") ? true : false);
        tsp = Integer.parseInt(s[4]);
        tep = Integer.parseInt(s[5]);

        //transcript information 
        int crs; //coding region start
        int cre; //coding region end
        int exonNum; //number of exons
        int[] exonStart;
        int[] exonEnd;
        crs = Integer.parseInt(s[6]);
        cre = Integer.parseInt(s[7]);
        exonNum = Integer.parseInt(s[8]);
        exonStart = parseExons(s[9]);
        exonEnd = parseExons(s[10]);
        transcript = new Transcript(ensemblID, tsp, tep, crs, cre, exonNum, exonStart, exonEnd);
        isCoding = (crs == cre ? false : true);

        start = tsp;
        end = tep;
    }

    public void addTranscript(Gene g){
        if(transcripts == null){
            //first time
            transcripts = new ArrayList<Transcript>();
            //add the first transcript
            transcripts.add(transcript);
        }
        transcripts.add(g.transcript);
        start = min(start, g.start);
        end = max(end, g.end);
    }

    public int[] parseExons(String s){
    	//splitTokens ignores the last comma
    	int[] result = PApplet.parseInt(splitTokens(s, ","));
    	return result;
    }

    public int compareTo(Object obj) {
        Gene e = (Gene) obj;
        //compare chromosome first
        int e_chr = e.chrIndex;
        if(this.chrIndex < e_chr){
            return -1;
        }else if(this.chrIndex > e_chr){
            return 1;
        }else{
            //if they are the same chromosome
            if (this.tsp < e.tsp ){
                return -1;
            } else if (this.tsp > e.tsp) {
                return 1;
            }
            return 0;
        }
    }

    //record events that affect this gene
    public void addEvent(Event e, String con) {
        if (affectingEvents == null) {
            affectingEvents = new ArrayList<Event>();
            eventConsequence = new ArrayList<String>();
        }
        affectingEvents.add(e);
        eventConsequence.add(con);
    }

    //check if there is an exon within this range
    public boolean containsExons(int start, int end) {
        boolean hasExons = false;
        //first check against the gene
        for (int i = 0; i < transcript.exonStart.length; i++) {
            int testStart = transcript.exonStart[i];
            int testEnd = transcript.exonEnd[i];
            //check
            if (testEnd < start) {
                //below range
                continue;
            }
            if (testStart > end) {
                //above range
                break;
            }
            //if it gets to point, there is over lap 
            return true;
        }
        //if more than one transcipts
        if(transcripts != null){
            for(int j = 1; j< transcripts.size(); j++){
                Transcript t = transcripts.get(j);
                for (int i = 0; i < t.exonStart.length; i++) {
                    int testStart = t.exonStart[i];
                    int testEnd = t.exonEnd[i];
                    //check
                    if (testEnd < start) {
                        //below range
                        continue;
                    }
                    if (testStart > end) {
                        //above range
                        break;
                    }
                    //if it gets to point, there is over lap 
                    return true;
                }            
            }
        }
        return hasExons;
    }
    //return the status of the gene
    public String[] getStatus(){
    	if (affectingEvents == null) {
    		//no event
            return null;
        }else{
            if(status == null){
            	//only done once
                setStatus();
            }
            if (status.isEmpty()) {
                //affecting events are not encoded yet, so the status is empty
                return null;
            } else {
                //compare against flags from radio buttons
                ArrayList<String> resultArray = new ArrayList<String>();
                Iterator ite = status.iterator();
                while(ite.hasNext()){
                    String s = (String) ite.next();
                    if(_isValidSvEvent(s)){
                        resultArray.add(s);
                    }
                }
                String[] result = resultArray.toArray(new String[resultArray.size()]);
                return result;
            }
        }
    }

    //check per sv events for all affecting events
    public void setStatus(){
        status = new HashSet<String>();
    	for (int i = 0; i<_EVENTS.length; i++){
    		boolean isLeft = false;
    		boolean isRight = false;
    		boolean isMiddle = false;
    		boolean isAll = false;

    		//iterate though affectingEvents
    		for(int j = 0; j<affectingEvents.size(); j++){
    			Event e = affectingEvents.get(j);
    			if(e.svIndex == i){
                    //same event
                    String conseq = eventConsequence.get(j);
                    if (conseq.equals("all")) {
	                    isAll = true;
	                } else if (conseq.equals("left")) {
	                    isLeft = true;
	                } else if (conseq.equals("right")) {
	                    isRight = true;
	                } else if (conseq.equals("middle")) {
	                    isMiddle = true;
	                }
    			}
    		}
    		//check flags and decide on its overall effect
    		if (isAll) {
	            //whole gene is affected
	            status.add(_EVENTS[i]+"_all");
	        } else if (isLeft && !isRight) {
	            //just left
	            status.add(_EVENTS[i]+"_left");
	        } else if (!isLeft && isRight) {
	            //just right
	            status.add(_EVENTS[i]+"_right");
	        } else if (!isLeft && !isRight && isMiddle) {
                //just middle
                status.add(_EVENTS[i]+"_middle");
	        } else if (isLeft && isRight) {
	            //check if there is a gap inbetween
	            int leftSide = tsp;
	            int rightSide = tep;
	            if (this.strand) {
	                leftSide = max(0, leftSide - _PROMOTER_LENGTH);
	            } else {
	                rightSide = rightSide+ _PROMOTER_LENGTH;
	            }

	            Span range = new Span(leftSide, rightSide);
	            //record the span of affecting events of sv type of the interest
	            for (int j = 0; j < this.affectingEvents.size(); j++) {
	                Event e = affectingEvents.get(j);
	                if (e.svIndex == i) {
	                    range.addCutSpan(new Span(e.start, e.end));
	                }
	            }
	            //check the result
	            boolean isAllDeleted = range.getCutResult();
	            if (isAllDeleted) {
	                status.add(_EVENTS[i]+"_all");
	            } else {
	                status.add(_EVENTS[i]+"_both_sides");
                    println("Debug:Gene: _both_sides deleted "+geneSymbol);
	            }
	        }

            // // // //inversion
            // if(i == 1){
            //     println("--inversion: all:"+isAll+"\tleft:"+isLeft+"\tright:"+isRight+"\tmiddle:"+isMiddle);
            // }
    	}
    }


    public void renderMultiEvent(){
        //get the first bubble
        Bubble b = bubbles.get(0);
        if(_VALID_EVENT[b.event.svIndex]){
            b.renderMultiEvent();
        }
    }
    // for unit plot
    public void renderMultiEvent_all(){
        for(Bubble b: bubbles){
            if(_VALID_EVENT[b.event.svIndex]){
                b.renderMultiEvent();
            }
        }
    }

    public void addGoTerm(Go go){
        if(goTerms == null){
            goTerms = new ArrayList<Go>();
        }
        goTerms.add(go);
    }

}
class GeneGroup{
	ArrayList<Gene> genes;
	Rectangle rect, rect_expanded;
	String[] status;
	int startPos, endPos;
    boolean isSelected = false;
    boolean isShown = true;

    boolean isMultiEvent = false;

	GeneGroup(Gene g){
		genes  = new ArrayList<Gene>();
		genes.add(g);
		status = g.getStatus();
		startPos = g.tsp;
		endPos = g.tep;
	}

	//check it it has the same status
	public boolean hasSameStatus(String[] s) {
        if (status == null || status.length == 0) { //current GeneDisplay is no hit
            if (s == null || s.length == 0) {
                //so as the next gene
                return true;
            } else {
                //new status
                return false;
            }
        } else {
            if (s == null) { 	//next gene is not hit
                return false;
            } else {
                if (status.length == s.length) {	//same length
                    for(int i = 0; i< status.length; i++){
                        String s1 = status[i];
                        boolean match = false;
                        for(int j = 0; j<s.length;j++){
                            if(s1.equals(s[j])){
                                match = true;
                            } 
                        }
                        if(!match){
                            return false;
                        }
                    }
                    return true;
                } else { 	//different length
                    String[] longArray, shortArray;
                    if(status.length > s.length){
                        longArray = status;
                        shortArray = s;
                    }else{
                        longArray = s;
                        shortArray = status;
                    }
                    for(int i = 0; i< longArray.length; i++){
                        String s1 = longArray[i];
                        boolean match = false;
                        for(int j = 0; j<shortArray.length;j++){
                            if(s1.equals(shortArray[j])){
                                match = true;
                            } 
                        } 
                        if(!match){
                            return false;
                        }
                    }
                    return true;  
                }
            }
        }
    }

    public void addGene(Gene g){
    	genes.add(g);
    	startPos = min(startPos, g.tsp);
    	endPos = max(endPos, g.tep);
    }


    public String toString(){
    	String output = "GeneGroup:";
    	if(status == null){
    		output +=" null";
    		return output;
    	}else{
    		output += Arrays.toString(status);
    		output += "\t"+this.genes.get(0).geneSymbol;
	    	return output;
    	}
    }

    public void saveRectangle(Rectangle rect){
        this.rect = rect;
    }

    public boolean containsGene(String gSymbol){
        for(int i = 0; i<genes.size(); i++){
            Gene g = genes.get(i);
            if(g.geneSymbol.equals(gSymbol)){
                return true;
            }
        }
        return false;
    }
    public Gene findGene(String gSymbol){
        for(int i = 0; i<genes.size(); i++){
            Gene g = genes.get(i);
            if(g.geneSymbol.equals(gSymbol)){
                return g;
            }
        }
        return null;
    }
	}
int geneView_height = _margin *15;
int geneView_y = _HEIGHT - geneView_height;

int chr_display_radius = _margin/2;
int chr_display_width = _WIDTH - (_margin*8);
int chr_display_x = _margin*4;
int chr_display_x2 = chr_display_x + chr_display_width;
int chr_display_mid_y = _margin*2 + chr_display_radius;
int chr_display_y = _margin*2;

int gene_display_y = chr_display_y + _margin*4;
int event_height = _margin;

Rectangle[] gene_rects = null; 

AniSequence animation_geneView;

boolean showGeneView = false;

//global variable for the range of bp displayed
int c_bp_start = 0;
int c_bp_end = 0;
//array of genes for the GeneView
ArrayList<Gene> _gene_view_genes;

int geneGap = 0;
//left and right button
Rectangle gene_view_left_btn = new Rectangle(0, geneView_y, _margin*4, geneView_height);
Rectangle gene_view_right_btn = new Rectangle(chr_display_x2, geneView_y, _margin*4, geneView_height);


//geneView
public void updateCurrentBP(){
    // println("--- updateCurrentBP()");
    if(!_selected_genes.isEmpty()){
        //find the first and end
        int gMin = Integer.MAX_VALUE;
        int gMax = 0;
        for (int i = 0; i < _selected_genes.size(); i++) {
            Gene gene = _selected_genes.get(i);
            if(gene.transcripts !=null){//more than one transcript
                for(int j = 0; j < gene.transcripts.size(); j++){
                    Transcript t = gene.transcripts.get(j);
                    gMin = min(gMin, t.tsp);
                    gMax = max(gMax, t.tep);
                }
            }else{ //only one transcript
                gMin = min(gMin, gene.tsp);
                gMax = max(gMax, gene.tep);
            }
        }
        //number of bp represented by 10 pixels
        float fraction = (float)(gMax - gMin) / (float)chr_display_width;
        geneGap = round(fraction * 10f);
        //add padding
        gMin = gMin - geneGap;
        gMax = gMax + geneGap;

        c_bp_start = gMin;
        c_bp_end = gMax;
        _gene_view_genes = _selected_genes;
    }
}
//called when the geneView range is changed
public void updateGeneViewGenes(){
    // println("--- updateGeneViewGenes()");
    if(!_selected_genes.isEmpty()){
        //get all the genes between c_bp_start and c_bp_end
        //_CHROMOSOMES[_C_CHR_INDEX]
        ArrayList<Gene> geneList =(ArrayList<Gene>) data.geneChrMap.get(_C_CHR_INDEX);
        //find the first gene covered 
        Gene first = _selected_genes.get(0);
        int first_index = geneList.indexOf(first);
        // if(first_index > 0){
        // println("debug: while loop start ---");
        while(first_index > 0){
            // println("debug: while loop --- -----------  "+first_index);
            Gene prev = geneList.get(first_index-1);
            if(isOverlapping(prev.start, prev.end, c_bp_start, c_bp_end)){
                //overlapping with display range
                first_index --;
            }else{
                //des not overlap
                break;
            }
        }
        // println("debug: while loop end ---");
        // }
        //end
        Gene end = _selected_genes.get((_selected_genes.size()-1));
        int end_index = geneList.indexOf(end);
        while(end_index  < geneList.size()){
            Gene next = geneList.get(end_index +1);
            if(isOverlapping(next.start, next.end, c_bp_start, c_bp_end)){
                end_index ++;
            }else{
                break;
            }
        }

        //add all genes
        if(_gene_view_genes == null){
            _gene_view_genes = new ArrayList<Gene>();
        }else{
            _gene_view_genes.clear();
        }
        for(int i = first_index; i <= end_index; i++){
            _gene_view_genes.add(geneList.get(i));
        }
    }
}

//checks if two ranges overlaps
public boolean isOverlapping(int s1, int e1, int s2, int e2){
    if(e2 < s1) return false;
    if(e1 < s2) return false;
    return true;
}



public void  updateGeneView(){

	geneView.textFont(font);
    geneView.smooth();
	geneView.beginDraw();
	geneView.background(color_geneView_background);
	//text
	geneView.fill(color_dark_gray);
	geneView.textAlign(LEFT, TOP);
	geneView.text(_CHROMOSOMES[_C_CHR_INDEX], chr_display_x, _margin/2);
	//draw chromosome
	//outline
    geneView.strokeWeight(11f);
    geneView.stroke(color_gray);
    geneView.strokeCap(ROUND);
    geneView.line(chr_display_x, chr_display_mid_y, chr_display_x2, chr_display_mid_y);
    //white inside
    geneView.strokeWeight(10f);
    geneView.stroke(255);
    geneView.line(chr_display_x, chr_display_mid_y, chr_display_x2, chr_display_mid_y);

    //draw genes and transcripts
    if(!_selected_genes.isEmpty()){
        showGeneView = true;
        //find the first and end
        int gMin = c_bp_start;// Integer.MAX_VALUE;
        int gMax = c_bp_end;// 0;
        // for (int i = 0; i < _selected_genes.size(); i++) {
        //     Gene gene = _selected_genes.get(i);
        //     if(gene.transcripts !=null){//more than one transcript
        //         for(int j = 0; j < gene.transcripts.size(); j++){
        //             Transcript t = gene.transcripts.get(j);
        //             gMin = min(gMin, t.tsp);
        //             gMax = max(gMax, t.tep);
        //         }
        //     }else{ //only one transcript
        //         gMin = min(gMin, gene.tsp);
        //         gMax = max(gMax, gene.tep);
        //     }
        // }
        // //number of bp represented by 10 pixels
        // float fraction = (float)(gMax - gMin) / (float)chr_display_width;
        // int geneGap = round(fraction * 10f);
        // //add padding
        // gMin = gMin - geneGap;
        // gMax = gMax + geneGap;
        int gWidth = gMax - gMin;

        //draw cytobands -------------------------------
        ArrayList<Band> bands = (ArrayList<Band>)data.cytobands.get(_C_CHR_INDEX);
    	int lastPos = bands.get(bands.size()-1).end;
        geneView.rectMode(CORNERS);
        for(int i = 0; i<bands.size(); i++){
            Band b = bands.get(i);
            float bx1 = map(b.start, 0, lastPos, chr_display_x, chr_display_x2);
            float bx2 = map(b.end, 0, lastPos,  chr_display_x, chr_display_x2);
            int fillColor = round(map(b.stain, 0, 100, 255, 150));
            geneView.stroke(color_gray);
            geneView.strokeWeight(1f);
            geneView.fill(fillColor);
            geneView.rect(bx1, chr_display_y, bx2, chr_display_y+_margin);
            //draw ends
            if(i==0 ){
                //beginning
                geneView.strokeWeight(10f);
                geneView.stroke(fillColor);
                geneView.strokeCap(ROUND);
                geneView.point(chr_display_x, chr_display_mid_y);
            }else if(i == bands.size()-1){
                //end
                geneView.strokeWeight(9.5f);
                geneView.stroke(fillColor);
                geneView.strokeCap(ROUND);
                geneView.point(chr_display_x+chr_display_width, chr_display_mid_y);
            }


            //check if they are currently selected rang
            if(b.end < gMin){
            }else{
                if(b.start > gMax){
                }else{
                    //overlap
                    geneView.fill(color_gray);
                    geneView.textAlign(CENTER, BOTTOM);
                    geneView.text(b.name, (bx1+bx2)/2, chr_display_y);
                }
            }
        }

        //draw centromere
        float cStart  = 0f;
        float cEnd  = 0f;
        for(int i = 0; i<bands.size(); i++){
            Band b = bands.get(i);
            if(b.isCentromere){
                float bx1 = map(b.start, 0,lastPos,  chr_display_x, chr_display_x2);
                float bx2 = map(b.end, 0, lastPos,  chr_display_x, chr_display_x2);
                if(cStart == 0f){
                    cStart = bx1;
                    cEnd = bx2;
                }else{
                    cEnd = max(cEnd, bx2);
                }
            }
        }
        float cMid = (cStart + cEnd)/2;
        geneView.stroke(color_gray);
        geneView.strokeWeight(1f);
        geneView.noFill();
        geneView.line(cStart, chr_display_y, cEnd, chr_display_y+_margin);
        geneView.line(cStart, chr_display_y+_margin, cEnd, chr_display_y);

        //draw genes -----------------------------------
        int y_pos;
        int y_gap = _margin;
        ArrayList<Integer> geneEnds = new ArrayList<Integer>();

        //initicalize the rect 
        if(_gene_view_genes.size() < 6){
            gene_rects = new Rectangle[_gene_view_genes.size()];
        }else{
            gene_rects = null;
        }
        //draw genes
        for(int i = 0; i <_gene_view_genes.size(); i++){
        	Gene gene = _gene_view_genes.get(i);
            int geneLabel_x = 0;
            int geneLabel_y = 0;
        	if(geneEnds.isEmpty()){
        		geneEnds.add(gene.end);
        		y_pos = gene_display_y; //initial y pos
        	}else{
        		//find y index
        		int y_index = -1;
        		for(int j = 0; j <geneEnds.size(); j++){
        			if((geneEnds.get(j)+geneGap) > gene.start){
        				//overlap
        			}else{
        				y_index = j;
        				break;
        			}
        		}
        		if(y_index == -1){
        			//new line
        			y_pos = gene_display_y + (geneEnds.size()*y_gap);
        			geneEnds.add(gene.end);
        		}else{
        			//define y_pos
        			y_pos = gene_display_y + (y_index*y_gap);
        			geneEnds.set(y_index, gene.end);
        		}
        	}
            geneLabel_y = y_pos;

        	//check if more than one transcripts
        	if(gene.transcripts == null || gene.transcripts.isEmpty()){
        		//only one trascripts
        		Transcript t = gene.transcript;
        		float t_start = t.tsp - gMin;
        		float t_end = t.tep- gMin;
        		//draw transcript
        		float t_x1 = map(t_start, 0, gWidth, chr_display_x, chr_display_x2);
        		float t_x2 = map(t_end, 0, gWidth, chr_display_x, chr_display_x2);
                geneLabel_x = round((t_x1+t_x2)/2);
        		geneView.stroke(180);
        		geneView.strokeWeight(2);
        		geneView.line(t_x1, y_pos, t_x2, y_pos);

        		//exons
        		int[] exonStart = t.exonStart;
        		int[] exonEnd = t.exonEnd;
	            geneView.stroke(this.color_dark_gray);
	            geneView.strokeWeight(3f);
	            geneView.strokeCap(SQUARE);
        		for (int j = 0; j < exonStart.length; j++) {
		            float eX1 = map(exonStart[j]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
		            float eX2 = map(exonEnd[j]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
		            //for really tiny exomes
		            if((eX2-eX1) < 1f){
		                eX2 = eX1 +1f;
		            }
		            geneView.line(eX1, y_pos, eX2, y_pos);
		        }
    		}else{
    			//multiple transcripts
	        	for(int j = 0; j < gene.transcripts.size(); j++ ){
	        		Transcript t = gene.transcripts.get(j);
	        		//draw transcript
	        		float t_start = t.tsp - gMin;
	        		float t_end = t.tep - gMin;
	        		//draw transcript
	        		float t_x1 = map(t_start, 0, gWidth, chr_display_x, chr_display_x2);
	        		float t_x2 = map(t_end, 0, gWidth, chr_display_x, chr_display_x2);
                    if(j==0){
                        geneLabel_x = round((t_x1+t_x2)/2);
                    }
	        		geneView.stroke(180);
	        		geneView.strokeWeight(2);
	        		geneView.line(t_x1, y_pos, t_x2, y_pos);

	        		//draw exomes
	        		//exons
	        		int[] exonStart = t.exonStart;
	        		int[] exonEnd = t.exonEnd;
		            geneView.stroke(this.color_dark_gray);
		            geneView.strokeWeight(3f);
		            geneView.strokeCap(SQUARE);
	        		for (int k = 0; k < exonStart.length; k++) {
			            float eX1 = map(exonStart[k]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
			            float eX2 = map(exonEnd[k]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
			            //for really tiny exomes
			            if((eX2-eX1) < 1f){
			                eX2 = eX1 +1f;
			            }
			            geneView.line(eX1, y_pos, eX2, y_pos);
			        }
			        y_pos += 3;//stack transcripts
	        	}
    		}
            //gene symbol
            //record gene_rect positions
            if(_gene_view_genes.size() < 6 ){
                //record rect
                int label_width = round(textWidth(gene.geneSymbol)+_margin);
                int label_x = geneLabel_x - (label_width/2);
                int label_y = geneLabel_y - _margin;
                gene_rects[i] = new Rectangle(label_x, label_y, label_width, _margin);


                geneView.textAlign(CENTER, BOTTOM);
                //hover
                if(gene_rects[i].contains(mouseX, (mouseY-geneView_y))){
                    geneView.fill(color_pink);
                }else{
                    geneView.fill(color_dark_dark_gray);
                }
                geneView.text(gene.geneSymbol, geneLabel_x, geneLabel_y);

            }           
        }

        //draw events
        int event_y = gene_display_y + (_margin*geneEnds.size() + _margin);
        //collect all unique events
        HashSet<Event> allEvents = new HashSet<Event>();
        for (int i = 0; i < _gene_view_genes.size(); i++) {
            Gene gene = _gene_view_genes.get(i);
            if(gene.affectingEvents != null){
                allEvents.addAll(gene.affectingEvents);
            }
        }
        ArrayList<Event> events = new ArrayList<Event>();
        events.addAll(allEvents);
        Collections.sort(events);
        // println("debug: events count ="+events.size());
        //start drawing events
        ArrayList<Integer> eventEnds = new ArrayList<Integer>();
        for(int i = 0; i < events.size(); i++){
        	Event e = events.get(i);
        	if(eventEnds.isEmpty()){
        		eventEnds.add(e.end);
        		y_pos = event_y;
        	}else{
        		//find y index
        		int y_index = -1;
        		for(int j = 0; j < eventEnds.size(); j++){
        			if(eventEnds.get(j)+geneGap > e.start){
        				//overlap
        			}else{
        				y_index = j;
        				break;
        			}
        		}
        		if(y_index == -1){
        			//new line
        			y_pos = event_y + (eventEnds.size()*event_height);
        			eventEnds.add(e.end);
        		}else{
        			y_pos = event_y + (y_index *event_height);
        			eventEnds.set(y_index, e.end);
        		}
        	}
        	//draw event at y_pos
        	float ex1 = map(e.start, gMin, gMax, chr_display_x, chr_display_x2);
        	float ex2 = map(e.end, gMin, gMax, chr_display_x, chr_display_x2);
        	geneView.stroke(color_sv_array3[e.svIndex]);
        	geneView.strokeWeight(1);
        	geneView.fill(color_sv_array[e.svIndex]);

        	geneView.rectMode(CORNERS);
        	geneView.rect(ex1, y_pos, ex2, y_pos+event_height);

            // println("debug: e.start="+e.start+"  e.end="+e.end);

            //positions
            // geneView.fill(255);
            // geneView.textAlign(LEFT, TOP);
            // geneView.text(e.start, ex1, y_pos);
            // geneView.textAlign(RIGHT, TOP);
            // geneView.text(e.end, ex2, y_pos);


            //

        }
        //hide event overflow
        geneView.noStroke();
        geneView.fill(color_geneView_background);
        geneView.rectMode(CORNERS);
        geneView.rect(0, gene_display_y - _margin, chr_display_x, _HEIGHT);  //left
        geneView.rect(chr_display_x2, gene_display_y - _margin, _WIDTH, _HEIGHT);//right

        //draw zoom area
        int veryBottomOfEvents = event_y + (eventEnds.size() *event_height) +_margin;
        //on chromosome representaiton
        float sx1 = map(gMin, 0, lastPos,  chr_display_x, chr_display_x2);
        float sx2 = map(gMax, 0, lastPos,  chr_display_x, chr_display_x2);
        if(sx2-sx1 <1){
            sx2 = sx1+1f;
        }
        geneView.stroke(color_cyan_trans);
        geneView.strokeWeight(1);
        geneView.noFill();
        // geneView.fill(240);
        // geneView.fill(color_main_background);
        geneView.beginShape();
        geneView.vertex(chr_display_x, veryBottomOfEvents);
        geneView.vertex(chr_display_x, gene_display_y - _margin);
        geneView.vertex(sx1, gene_display_y - _margin);
        geneView.vertex(sx1, chr_display_y);
        geneView.vertex(sx2, chr_display_y);
        geneView.vertex(sx2, gene_display_y - _margin);
        geneView.vertex(chr_display_x2, gene_display_y - _margin);
        geneView.vertex(chr_display_x2, veryBottomOfEvents);
        geneView.vertex(chr_display_x, veryBottomOfEvents);
        geneView.endShape();

        //draw selected region bp positions
        geneView.fill(color_cyan);
        geneView.textAlign(LEFT, BOTTOM);
        geneView.text(c_bp_start, chr_display_x, gene_display_y - _margin);
        geneView.textAlign(RIGHT, BOTTOM);
        geneView.text(c_bp_end, chr_display_x2, gene_display_y - _margin);

        //mouse over //left button
        if(gene_view_left_btn.contains(mouseX, mouseY)){
            geneView.fill(color_cyan_trans);
            geneView.noStroke();
        }else{
            geneView.noFill();
            geneView.stroke(color_cyan_trans);
            geneView.strokeWeight(1);
        }
        geneView.beginShape();
        geneView.vertex(chr_display_x - _margin, veryBottomOfEvents);
        geneView.vertex(chr_display_x - _margin, gene_display_y - _margin);
        geneView.vertex(chr_display_x - _margin*2, (veryBottomOfEvents + gene_display_y - _margin)/2);
        geneView.vertex(chr_display_x - _margin, veryBottomOfEvents);
        geneView.endShape();

        //right button
        if(gene_view_right_btn.contains(mouseX, mouseY)){
            geneView.fill(color_cyan_trans);
            geneView.noStroke();
        }else{
            geneView.noFill();
            geneView.stroke(color_cyan_trans);
            geneView.strokeWeight(1);
        }
        geneView.beginShape();
        geneView.vertex(chr_display_x2 + _margin, veryBottomOfEvents);
        geneView.vertex(chr_display_x2 + _margin, gene_display_y - _margin);
        geneView.vertex(chr_display_x2 + _margin*2, (veryBottomOfEvents + gene_display_y - _margin)/2);
        geneView.vertex(chr_display_x2 + _margin, veryBottomOfEvents);
        geneView.endShape();



        geneView_height = veryBottomOfEvents +_margin*2;
	}else{
		geneView_height = _margin;
        showGeneView = false;
	}
	geneView.endDraw();

	//update the position
	// Ani.to(this, 1.5, "geneView_y", (_HEIGHT-geneView_height), Ani.EXPO_IN_OUT);
    animation_geneView = new AniSequence(this);
    animation_geneView.beginSequence();
    animation_geneView.beginStep();
    animation_geneView.add(Ani.to(this, 1.5f, "geneView_y", (_HEIGHT-geneView_height), Ani.EXPO_IN_OUT));
    animation_geneView.endStep();
    animation_geneView.endSequence();
    animation_geneView.start();
}

public void drawGeneViewdPDF(){
    pushMatrix();
    translate(0, geneView_y);

    textFont(font);
    fill(color_geneView_background);
    noStroke();
    rect(0,0, _WIDTH, geneView_height);
    //test
    fill(color_dark_gray);
    textAlign(LEFT, TOP);
    text(_CHROMOSOMES[_C_CHR_INDEX], chr_display_x, _margin/2);
    //draw chromosome
    //outline
    strokeWeight(11f);
    stroke(color_gray);
    strokeCap(ROUND);
    line(chr_display_x, chr_display_mid_y, chr_display_x2, chr_display_mid_y);
    //white inside
    strokeWeight(10f);
    stroke(255);
    line(chr_display_x, chr_display_mid_y, chr_display_x2, chr_display_mid_y);

    //draw genes and transcripts
    if(!_selected_genes.isEmpty()){
        showGeneView = true;
        //find the first and end
        int gMin = c_bp_start;// Integer.MAX_VALUE;
        int gMax = c_bp_end;// 0;
        int gWidth = gMax - gMin;

        //draw cytobands -------------------------------
        ArrayList<Band> bands = (ArrayList<Band>)data.cytobands.get(_C_CHR_INDEX);
        int lastPos = bands.get(bands.size()-1).end;
        rectMode(CORNERS);
        for(int i = 0; i<bands.size(); i++){
            Band b = bands.get(i);
            float bx1 = map(b.start, 0, lastPos, chr_display_x, chr_display_x2);
            float bx2 = map(b.end, 0, lastPos,  chr_display_x, chr_display_x2);
            int fillColor = round(map(b.stain, 0, 100, 255, 150));
            stroke(color_gray);
            strokeWeight(1f);
            fill(fillColor);
            rect(bx1, chr_display_y, bx2, chr_display_y+_margin);
            //draw ends
            if(i==0 ){
                //beginning
                strokeWeight(10f);
                stroke(fillColor);
                strokeCap(ROUND);
                point(chr_display_x, chr_display_mid_y);
            }else if(i == bands.size()-1){
                //end
                strokeWeight(9.5f);
                stroke(fillColor);
                strokeCap(ROUND);
                point(chr_display_x+chr_display_width, chr_display_mid_y);
            }


            //check if they are currently selected range
            if(b.end < gMin){
            }else{
                if(b.start > gMax){
                }else{
                    //overlap
                    fill(color_gray);
                    textAlign(CENTER, BOTTOM);
                    text(b.name, (bx1+bx2)/2, chr_display_y);
                }
            }
        }

        //draw centromere
        float cStart  = 0f;
        float cEnd  = 0f;
        for(int i = 0; i<bands.size(); i++){
            Band b = bands.get(i);
            if(b.isCentromere){
                float bx1 = map(b.start, 0,lastPos,  chr_display_x, chr_display_x2);
                float bx2 = map(b.end, 0, lastPos,  chr_display_x, chr_display_x2);
                if(cStart == 0f){
                    cStart = bx1;
                    cEnd = bx2;
                }else{
                    cEnd = max(cEnd, bx2);
                }
            }
        }
        float cMid = (cStart + cEnd)/2;
        stroke(color_gray);
        strokeWeight(1f);
        noFill();
        line(cStart, chr_display_y, cEnd, chr_display_y+_margin);
        line(cStart, chr_display_y+_margin, cEnd, chr_display_y);

        //draw genes -----------------------------------
        int y_pos;
        int y_gap = _margin;
        ArrayList<Integer> geneEnds = new ArrayList<Integer>();

        //initicalize the rect 
        if(_selected_genes.size() < 6){
            gene_rects = new Rectangle[_selected_genes.size()];
        }else{
            gene_rects = null;
        }
        //draw genes
        for(int i = 0; i <_selected_genes.size(); i++){
            Gene gene = _selected_genes.get(i);
            int geneLabel_x = 0;
            int geneLabel_y = 0;
            if(geneEnds.isEmpty()){
                geneEnds.add(gene.end);
                y_pos = gene_display_y; //initial y pos
            }else{
                //find y index
                int y_index = -1;
                for(int j = 0; j <geneEnds.size(); j++){
                    if((geneEnds.get(j)+geneGap) > gene.start){
                        //overlap
                    }else{
                        y_index = j;
                        break;
                    }
                }
                if(y_index == -1){
                    //new line
                    y_pos = gene_display_y + (geneEnds.size()*y_gap);
                    geneEnds.add(gene.end);
                }else{
                    //define y_pos
                    y_pos = gene_display_y + (y_index*y_gap);
                    geneEnds.set(y_index, gene.end);
                }
            }
            geneLabel_y = y_pos;

            //check if more than one transcripts
            if(gene.transcripts == null || gene.transcripts.isEmpty()){
                //only one trascripts
                Transcript t = gene.transcript;
                float t_start = t.tsp - gMin;
                float t_end = t.tep- gMin;
                //draw transcript
                float t_x1 = map(t_start, 0, gWidth, chr_display_x, chr_display_x2);
                float t_x2 = map(t_end, 0, gWidth, chr_display_x, chr_display_x2);
                geneLabel_x = round((t_x1+t_x2)/2);
                stroke(180);
                strokeWeight(2);
                line(t_x1, y_pos, t_x2, y_pos);

                //exons
                int[] exonStart = t.exonStart;
                int[] exonEnd = t.exonEnd;
                stroke(this.color_dark_gray);
                strokeWeight(3f);
                strokeCap(SQUARE);
                for (int j = 0; j < exonStart.length; j++) {
                    float eX1 = map(exonStart[j]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
                    float eX2 = map(exonEnd[j]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
                    //for really tiny exomes
                    if((eX2-eX1) < 1f){
                        eX2 = eX1 +1f;
                    }
                    line(eX1, y_pos, eX2, y_pos);
                }
            }else{
                //multiple transcripts
                for(int j = 0; j < gene.transcripts.size(); j++ ){
                    Transcript t = gene.transcripts.get(j);
                    //draw transcript
                    float t_start = t.tsp - gMin;
                    float t_end = t.tep - gMin;
                    //draw transcript
                    float t_x1 = map(t_start, 0, gWidth, chr_display_x, chr_display_x2);
                    float t_x2 = map(t_end, 0, gWidth, chr_display_x, chr_display_x2);
                    if(j==0){
                        geneLabel_x = round((t_x1+t_x2)/2);
                    }
                    stroke(180);
                    strokeWeight(2);
                    line(t_x1, y_pos, t_x2, y_pos);

                    //draw exomes
                    //exons
                    int[] exonStart = t.exonStart;
                    int[] exonEnd = t.exonEnd;
                    stroke(this.color_dark_gray);
                    strokeWeight(3f);
                    strokeCap(SQUARE);
                    for (int k = 0; k < exonStart.length; k++) {
                        float eX1 = map(exonStart[k]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
                        float eX2 = map(exonEnd[k]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
                        //for really tiny exomes
                        if((eX2-eX1) < 1f){
                            eX2 = eX1 +1f;
                        }
                        line(eX1, y_pos, eX2, y_pos);
                    }
                    y_pos += 3;//stack transcripts
                }
            }
            //gene symbol
            //record gene_rect positions
            if(_selected_genes.size() < 6 ){
                //record rect
                int label_width = round(textWidth(gene.geneSymbol)+_margin);
                int label_x = geneLabel_x - (label_width/2);
                int label_y = geneLabel_y - _margin;
                gene_rects[i] = new Rectangle(label_x, label_y, label_width, _margin);


                textAlign(CENTER, BOTTOM);
                //hover
                if(gene_rects[i].contains(mouseX, (mouseY-geneView_y))){
                    fill(color_pink);
                }else{
                    fill(color_dark_dark_gray);
                }
                text(gene.geneSymbol, geneLabel_x, geneLabel_y);

            }           
        }

        //draw events
        int event_y = gene_display_y + (_margin*geneEnds.size() + _margin);
        //collect all unique events
        HashSet<Event> allEvents = new HashSet<Event>();
        for (int i = 0; i < _selected_genes.size(); i++) {
            Gene gene = _selected_genes.get(i);
            if(gene.affectingEvents != null){
                allEvents.addAll(gene.affectingEvents);
            }
        }
        ArrayList<Event> events = new ArrayList<Event>();
        events.addAll(allEvents);
        Collections.sort(events);
        // println("debug: events count ="+events.size());
        //start drawing events
        ArrayList<Integer> eventEnds = new ArrayList<Integer>();
        for(int i = 0; i < events.size(); i++){
            Event e = events.get(i);
            if(eventEnds.isEmpty()){
                eventEnds.add(e.end);
                y_pos = event_y;
            }else{
                //find y index
                int y_index = -1;
                for(int j = 0; j < eventEnds.size(); j++){
                    if(eventEnds.get(j)+geneGap > e.start){
                        //overlap
                    }else{
                        y_index = j;
                        break;
                    }
                }
                if(y_index == -1){
                    //new line
                    y_pos = event_y + (eventEnds.size()*event_height);
                    eventEnds.add(e.end);
                }else{
                    y_pos = event_y + (y_index *event_height);
                    eventEnds.set(y_index, e.end);
                }
            }
            //draw event at y_pos
            float ex1 = map(e.start, gMin, gMax, chr_display_x, chr_display_x2);
            float ex2 = map(e.end, gMin, gMax, chr_display_x, chr_display_x2);
            stroke(color_sv_array3[e.svIndex]);
            strokeWeight(1);
            fill(color_sv_array[e.svIndex]);

            rectMode(CORNERS);
            rect(ex1, y_pos, ex2, y_pos+event_height);

            // println("debug: e.start="+e.start+"  e.end="+e.end);

            //positions
            // geneView.fill(255);
            // geneView.textAlign(LEFT, TOP);
            // geneView.text(e.start, ex1, y_pos);
            // geneView.textAlign(RIGHT, TOP);
            // geneView.text(e.end, ex2, y_pos);


            //

        }
        //hide event overflow
        noStroke();
        fill(color_geneView_background);
        rectMode(CORNERS);
        // rect(0, event_y, chr_display_x, _HEIGHT);  //left
        // rect(chr_display_x2, event_y, _WIDTH, _HEIGHT);//right gene_display_y
        rect(0, gene_display_y - _margin, chr_display_x, _HEIGHT);  //left
        rect(chr_display_x2, gene_display_y - _margin, _WIDTH, _HEIGHT);

        //draw zoom area
        int veryBottomOfEvents = event_y + (eventEnds.size() *event_height) +_margin;
        //on chromosome representaiton
        float sx1 = map(gMin, 0, lastPos,  chr_display_x, chr_display_x2);
        float sx2 = map(gMax, 0, lastPos,  chr_display_x, chr_display_x2);
        if(sx2-sx1 <1){
            sx2 = sx1+1f;
        }
        stroke(color_cyan_trans);
        strokeWeight(1);
        noFill();
        //  fill(240);
        //  fill(color_main_background);
        beginShape();
        vertex(chr_display_x, veryBottomOfEvents);
        vertex(chr_display_x, gene_display_y - _margin);
        vertex(sx1, gene_display_y - _margin);
        vertex(sx1, chr_display_y);
        vertex(sx2, chr_display_y);
        vertex(sx2, gene_display_y - _margin);
        vertex(chr_display_x2, gene_display_y - _margin);
        vertex(chr_display_x2, veryBottomOfEvents);
        vertex(chr_display_x, veryBottomOfEvents);
        endShape();

        //mouse over //left button
        if(gene_view_left_btn.contains(mouseX, mouseY)){
            fill(color_cyan_trans);
            noStroke();
        }else{
            noFill();
            stroke(color_cyan_trans);
            strokeWeight(1);
        }
        beginShape();
        vertex(chr_display_x - _margin, veryBottomOfEvents);
        vertex(chr_display_x - _margin, gene_display_y - _margin);
        vertex(chr_display_x - _margin*2, (veryBottomOfEvents + gene_display_y - _margin)/2);
        vertex(chr_display_x - _margin, veryBottomOfEvents);
        endShape();

        //right button
        if(gene_view_right_btn.contains(mouseX, mouseY)){
            fill(color_cyan_trans);
            noStroke();
        }else{
            noFill();
            stroke(color_cyan_trans);
            strokeWeight(1);
        }
        beginShape();
        vertex(chr_display_x2 + _margin, veryBottomOfEvents);
        vertex(chr_display_x2 + _margin, gene_display_y - _margin);
        vertex(chr_display_x2 + _margin*2, (veryBottomOfEvents + gene_display_y - _margin)/2);
        vertex(chr_display_x2 + _margin, veryBottomOfEvents);
        endShape();

        //draw selected region bp positions
        fill(color_cyan);
        textAlign(LEFT, BOTTOM);
        text(c_bp_start, chr_display_x, gene_display_y - _margin);
        textAlign(RIGHT, BOTTOM);
        text(c_bp_end, chr_display_x2, gene_display_y - _margin);

        geneView_height = veryBottomOfEvents +_margin*2;
    }else{
        geneView_height = _margin;
        showGeneView = false;
    }

    popMatrix();
}
class Go implements Comparable{
	String name;
	ArrayList<Gene> genes;
	boolean isSuperclass = false;


	Go(String n){
		name = n;
		genes = new ArrayList<Gene>();
	}

	public void addGene(Gene g){
		if(genes == null){
			genes = new ArrayList<Gene>();
		}
		genes.add(g);
	}

	public String toString(){
		if(genes != null){
			return name+"("+genes.size()+")";
		}else{
			return name+"(1)";		
		}
	}

	public int compareTo(Object obj){
		Go go2 = (Go) obj;
		String goName1 = name.toUpperCase();
		String goName2 = go2.name.toUpperCase();
		return goName1.compareTo(goName2);
	}
}
class GoType extends Go{
	String type;

	GoType(String s){
		super(null);
		type = s;
		isSuperclass = true;
	}
}


Rectangle goTermRect;
int maxGoRowCount = 30;
int goRowHeight = 10;
int goTerm_w = _margin*19;
int goTerm_h = maxGoRowCount*goRowHeight;// _margin*50;
int goTermScroller_w = _margin;
Rectangle scrollBar = null;

boolean[] foldedGoTypes = null; //flags for go terms
// String[] goTypes = null;


int legend_x = _margin;
int legend_label_x = legend_x+_margin*2;
int legend_top_margin = _margin;
int legend_w = _margin*22;
int legend_h = _HEIGHT - _margin*15;//_margin*15 + goTerm_h;
int legend_label_gap = _margin/2;

int legendView_x = _WIDTH - legend_w;// - _margin; //position on actual display
int legendView_y = 0;						
Rectangle legend_close_rect = new Rectangle(legendView_x, 0, legend_w, _margin);
Rectangle legendView_rect = new Rectangle(legendView_x, 0, legend_w, legend_h);
boolean showLegend = true;
Rectangle[] radio_btns;

//multi event toggle
Rectangle multi_event_btn;

Rectangle textBox;
boolean textBoxActive = false;

//optional categories
int category_label_y = 0;

int goTermIndex = 0; //starting point of Go
int totalGoCount = 0;
Rectangle[] goRectangles = new Rectangle[maxGoRowCount];
Go[] goToDisplay = new Go[maxGoRowCount];

// PFont goTermFont;
PFont legendFont = createFont("Monospaced", 10);

AniSequence animation_legendView;


//setup coordinates
public void initLegendView(){
	radio_btns = new Rectangle[_EVENTS.length];
	int running_y = legend_top_margin;
	for(int i = 0; i<_EVENTS.length; i++){
		radio_btns[i] = new Rectangle(legend_x, running_y, _margin,_margin);
		running_y += _margin+legend_label_gap;
	}
	//multi event toggle
	multi_event_btn = new Rectangle(legend_x, running_y, _margin, _margin);
	running_y +=  _margin+legend_label_gap;
	//textBox
	running_y+= _margin*2;
	textBox = new Rectangle(legend_x, running_y, _margin*18, 15);
	running_y+= 15 +_margin;

	//optional
	//categories
	if(hasOptionalTable){
		category_label_y = running_y;
		running_y += _margin;
		ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(data.optionalTable.values());
		Collections.sort(categories);
		for(int i = 0; i<categories.size(); i++){
			OptionalCategory oc = categories.get(i);
			oc.rect = new Rectangle(legend_x, running_y, _margin, _margin);
			running_y+= _margin + legend_label_gap;
		}
		running_y += _margin;
	}

	//remaining height
	int remainingHegiht = legend_h - running_y;
	maxGoRowCount = remainingHegiht / goRowHeight -1;
	goTerm_h = goRowHeight * maxGoRowCount;
	goTermRect = new Rectangle(legend_x, running_y, goTerm_w, goTerm_h);

	goRectangles = new Rectangle[maxGoRowCount];
	goToDisplay = new Go[maxGoRowCount];


	// // //calculate how much space left
	// int remaining_h = legend_h - running_y;
	// maxGoRowCount = remaining_h / goRowHeight -1;
	// goTerm_h = maxGoRowCount * goRowHeight;

	// println("debug: GoRowCount ="+maxGoRowCount+"  running_y ="+running_y+" legend_h="+legend_h);

	// goTermRect = new Rectangle(legend_x, running_y, goTerm_w, goTerm_h);

	//goTermFont
	// goTermFont = loadFont("XLMonoAlt-10.vlw");
}

public void reinitialiseLegendView(){
	if(hasOptionalTable){
		//reorganize the position
		radio_btns = new Rectangle[_EVENTS.length];
		int running_y = legend_top_margin;
		for(int i = 0; i<_EVENTS.length; i++){
			radio_btns[i] = new Rectangle(legend_x, running_y, _margin,_margin);
			running_y += _margin+legend_label_gap;
		}
		//multi event toggle
		multi_event_btn = new Rectangle(legend_x, running_y, _margin, _margin);
		running_y +=  _margin+legend_label_gap;

		//textBox
		running_y+= _margin;
		textBox = new Rectangle(legend_x, running_y, _margin*18, 15);
		running_y+= 15 +_margin;

		if(hasOptionalTable){
			//categories
			category_label_y = running_y;
			running_y += _margin;
			ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(data.optionalTable.values());
			Collections.sort(categories);
			for(int i = 0; i<categories.size(); i++){
				OptionalCategory oc = categories.get(i);
				oc.rect = new Rectangle(legend_x, running_y, _margin, _margin);
				running_y+= _margin + legend_label_gap;
			}
			running_y += _margin;
			
		}

		//remaining height
		int remainingHegiht = legend_h - running_y;
		maxGoRowCount = remainingHegiht / goRowHeight -1;
		goTerm_h = goRowHeight * maxGoRowCount;
		goTermRect = new Rectangle(legend_x, running_y, goTerm_w, goTerm_h);

		goRectangles = new Rectangle[maxGoRowCount];
		goToDisplay = new Go[maxGoRowCount];


		println("debug: GoRowCount ="+maxGoRowCount+"  running_y ="+running_y+" legend_h="+legend_h);
	}
}

public void updateLegendView(){
	legendView.beginDraw();
	legendView.textFont(legendFont);
	legendView.textSize(10);
	legendView.smooth();
	legendView.background(240);
	//event toggle buttons
	for(int i = 0; i<_EVENTS.length; i++){
		//draw 
		Rectangle rect = radio_btns[i];
		if(_VALID_EVENT[i]){
			legendView.stroke(color_sv_array3[i]);
			legendView.fill(color_sv_array[i]);
		}else{
			legendView.stroke(color_sv_array3[i]);
			legendView.noFill();
		}
		legendView.rectMode(CORNER);
		legendView.rect(rect.x, rect.y, rect.width, rect.height);

		legendView.fill(color_dark_gray);	
		legendView.textAlign(LEFT, CENTER);
		legendView.text(_EVENTS[i], legend_label_x, rect.y +5);
	}

	//multi event
	//circle
	legendView.stroke(color_dark_gray);
	legendView.noFill();
	legendView.ellipseMode(CORNER);
	legendView.ellipse(multi_event_btn.x, multi_event_btn.y, multi_event_btn.width, multi_event_btn.height);
	
	if(_show_multi_event){
		legendView.strokeWeight(4);
		legendView.point((multi_event_btn.x +(multi_event_btn.width/2)), (multi_event_btn.y+(multi_event_btn.height/2)));
	}
	//text
	legendView.fill(color_dark_gray);	
	legendView.textAlign(LEFT, CENTER);
	legendView.text("multiple events", legend_label_x, multi_event_btn.y+5);


	//draw textBox_______________________________________________________
	legendView.rectMode(CORNER);
	if(textBoxActive){
		legendView.stroke(color_blue);
	}else{
		legendView.stroke(color_gray);
	}
	legendView.fill(color_white);
	legendView.strokeWeight(1);
	legendView.rect(textBox.x, textBox.y, textBox.width, textBox.height);
	//text inside box
	legendView.textAlign(LEFT, CENTER);
	legendView.fill(color_dark_gray);
	legendView.text(typedString, textBox.x +2, textBox.y+textBox.height/2);

	//drawing Go term
	updateGoDisplay();
	drawGoTerm();

	//optional table
	if(hasOptionalTable){
		//table name
		legendView.fill(color_dark_gray);	
		legendView.textAlign(LEFT, TOP);
		legendView.text(_optional_category, legend_x, category_label_y-1); //adjusting the position
		ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(data.optionalTable.values());
		for(int i = 0; i<categories.size(); i++){
			OptionalCategory oc = categories.get(i);
			Rectangle rect = oc.rect;
			//box
			legendView.stroke(color_dark_gray);
			if(oc.isSelected){
				legendView.fill(color_gray);
			}else{
				legendView.noFill();
			}
			legendView.rect(rect.x, rect.y, rect.width, rect.height);
			//text
			legendView.fill(color_dark_gray);	
			legendView.textAlign(LEFT, CENTER);
			legendView.text(oc.name+"("+oc.genes.size()+")", rect.x+rect.width+_margin, rect.y +5);
		}
	}
	legendView.endDraw();
}

//update which Go to dsiplay
public void updateGoDisplay(){
	//first time set rectangles
	if(foldedGoTypes == null){
		foldedGoTypes = new boolean[data.goTypes.size()];
		Arrays.fill(foldedGoTypes, Boolean.TRUE);
		int running_y = goTermRect.y;
		for(int i = 0; i< maxGoRowCount; i++){
			goRectangles[i] = new Rectangle(goTermRect.x, running_y, goTerm_w, goRowHeight);
			running_y+=goRowHeight;
		}
	}
	//find total count
	ArrayList<Go> totalGo = new ArrayList<Go>();

	for(int i = 0; i<data.goTypes.size(); i++){
		GoType goType = new GoType(data.goTypes.get(i));//superclass
		totalGo.add(goType);
		if(foldedGoTypes[i]){
			//folded
			//add selected go term
			ArrayList<Go> array = (ArrayList<Go>) data.all_goMap_type.get(data.goTypes.get(i));
			for(int j = 0; j<array.size(); j++){
				Go go = array.get(j);
				if(_selected_goTerms.contains(go)){
					totalGo.add(go);
				}
			}
		}else{
			//unfolded, add all Go terms
			ArrayList<Go> array = (ArrayList<Go>) data.all_goMap_type.get(data.goTypes.get(i));
			totalGo.addAll(array);
		}
	}	

	//add to goToDisplay
	goToDisplay = new Go[maxGoRowCount];
	for(int i = 0; i<maxGoRowCount; i++){
		int index = goTermIndex + i;
		if(index < totalGo.size() && index >= 0 ){
			goToDisplay[i] = totalGo.get(index);
		}else{
			goToDisplay[i] = null;
		}
	}
	totalGoCount = totalGo.size();
}

public void drawGoTerm(){
	legendView.rectMode(CORNER);
	for(int i = 0; i< maxGoRowCount; i++){
		Rectangle rect = goRectangles[i];
		Go go = goToDisplay[i];
		if(go != null){
			//check if it is style
			if(go.isSuperclass){
				GoType gs = (GoType)go;
				//draw style
				String typeLabel = gs.type;
				int typeIndex = getGoTypeIndex(gs.type);
				//draw the type 
				if(foldedGoTypes[typeIndex]){
					//folded
					typeLabel = "> "+gs.type;
				}else{
					//unfolded
					typeLabel = "v "+gs.type;
				}
				legendView.fill(color_dark_gray);
				legendView.noStroke();
				legendView.rect(rect.x, rect.y, rect.width, rect.height);
				legendView.fill(240);
				legendView.textAlign(LEFT, TOP);

				legendView.text(typeLabel, rect.x, rect.y);
				// println("debug: go style ="+gs.style);
			}else{
				// //draw Go
				if(_selected_go == go){
					legendView.fill(color_cyan);
				}else if(_selected_goTerms.contains(go)){
					legendView.fill(color_pink);
				}else{
					legendView.fill(color_dark_gray);
				}
				legendView.textAlign(LEFT, TOP);
				legendView.text(go.name, rect.x, rect.y);
			}
		}else{
			break;
		}
	}

	//hide over flow
	legendView.fill(240);
	legendView.noStroke();
	legendView.rectMode(CORNER);
	legendView.rect(goTermRect.x +goTerm_w, goTermRect.y, goTermScroller_w+_margin, goTerm_h);

	//draw scroller
	if(totalGoCount > maxGoRowCount){
		legendView.rectMode(CORNER);
		legendView.fill(255);
		legendView.stroke(color_dark_gray);
		legendView.rect((goTermRect.x+goTerm_w), goTermRect.y, goTermScroller_w, goTerm_h);

		//draw displayed region
		int startIndex = goTermIndex;
		int endIndex = goTermIndex +(maxGoRowCount-1);
		int dy = round(map(startIndex, 0, totalGoCount-1, goTermRect.y, goTermRect.y+goTerm_h));
		int dh = round(map(maxGoRowCount-1, 0, totalGoCount-1, 0, goTerm_h));
		scrollBar = new Rectangle(goTermRect.x + goTerm_w, dy, goTermScroller_w, dh);

		legendView.fill(color_gray);
		legendView.rect(scrollBar.x, scrollBar.y, scrollBar.width, scrollBar.height);
	}

	// legendView.textFont(legendFont);

}

public void drawGoTermPDF(){
	rectMode(CORNER);
	for(int i = 0; i< maxGoRowCount; i++){
		Rectangle rect = goRectangles[i];
		Go go = goToDisplay[i];
		if(go != null){
			//check if it is style
			if(go.isSuperclass){
				GoType gs = (GoType)go;
				//draw style
				String typeLabel = gs.type;
				int typeIndex = getGoTypeIndex(gs.type);
				//draw the type 
				if(foldedGoTypes[typeIndex]){
					//folded
					typeLabel = "> "+gs.type;
				}else{
					//unfolded
					typeLabel = "v "+gs.type;
				}
				fill(color_dark_gray);
				noStroke();
				rect(rect.x, rect.y, rect.width, rect.height);
				fill(240);
				textAlign(LEFT, TOP);

				text(typeLabel, rect.x, rect.y);
				// println("debug: go style ="+gs.style);
			}else{
				// //draw Go
				if(_selected_go == go){
					fill(color_cyan);
				}else if(_selected_goTerms.contains(go)){
					fill(color_pink);
				}else{
					fill(color_dark_gray);
				}
				textAlign(LEFT, TOP);
				text(go.name, rect.x, rect.y);
			}
		}else{
			break;
		}
	}

	//hide over flow
	fill(240);
	noStroke();
	rectMode(CORNER);
	rect(goTermRect.x +goTerm_w, goTermRect.y, goTermScroller_w+_margin, goTerm_h);

	//draw scroller
	if(totalGoCount > maxGoRowCount){
		rectMode(CORNER);
		fill(255);
		stroke(color_dark_gray);
		rect((goTermRect.x+goTerm_w), goTermRect.y, goTermScroller_w, goTerm_h);

		//draw displayed region
		int startIndex = goTermIndex;
		int endIndex = goTermIndex +(maxGoRowCount-1);
		int dy = round(map(startIndex, 0, totalGoCount-1, goTermRect.y, goTermRect.y+goTerm_h));
		int dh = round(map(maxGoRowCount-1, 0, totalGoCount-1, 0, goTerm_h));
		scrollBar = new Rectangle(goTermRect.x + goTerm_w, dy, goTermScroller_w, dh);

		fill(color_gray);
		rect(scrollBar.x, scrollBar.y, scrollBar.width, scrollBar.height);
	}

}

public void hideLegendView(){
	animation_legendView = new AniSequence(this);
    animation_legendView.beginSequence();
    animation_legendView.beginStep();
    animation_legendView.add(Ani.to(this, 1.5f, "legendView_y", (_margin - legend_h), Ani.EXPO_IN_OUT));
    animation_legendView.endStep();
    animation_legendView.endSequence();
    animation_legendView.start();
	// Ani.to(this, 1.5, "legendView_y", (_margin - legend_h), Ani.EXPO_IN_OUT);
}
public void showLegendView(){
	animation_legendView = new AniSequence(this);
    animation_legendView.beginSequence();
    animation_legendView.beginStep();
    animation_legendView.add(Ani.to(this, 1.5f, "legendView_y", 0, Ani.EXPO_IN_OUT));
    animation_legendView.endStep();
    animation_legendView.endSequence();
    animation_legendView.start();
	// Ani.to(this, 1.5, "legendView_y", 0, Ani.EXPO_IN_OUT);
}

public int findGoRectIndex(int x, int y){
	int relative_x = x - legendView_x;
	int relative_y = y - legendView_y;
	for(int i = 0; i < goRectangles.length; i++){
		if(goRectangles[i].contains(relative_x, relative_y)){
			return i;
		}
	}
	return -1;
}

public int getGoTypeIndex(String s){
	return data.goTypes.indexOf(s);
}

//pdf export
public void drawLegendViewPDF(){
	textFont(legendFont);
	fill(240);
	noStroke();
	pushMatrix();
	translate(legendView_x, legendView_y);
	rect(0, 0, legend_w, legend_h);
	strokeWeight(1);
	for(int i = 0; i<_EVENTS.length; i++){
		//draw 
		Rectangle rect = radio_btns[i];
		if(_VALID_EVENT[i]){
			stroke(color_sv_array3[i]);
			fill(color_sv_array[i]);
		}else{
			stroke(color_sv_array3[i]);
			noFill();
		}
		rectMode(CORNER);
		rect(rect.x, rect.y, rect.width, rect.height);

		fill(color_dark_gray);	
		textAlign(LEFT, CENTER);
		text(_EVENTS[i], legend_label_x, rect.y +5);
	}
	//multi event
	//circle
	stroke(color_dark_gray);
	noFill();
	ellipseMode(CORNER);
	ellipse(multi_event_btn.x, multi_event_btn.y, multi_event_btn.width, multi_event_btn.height);
	if(_show_multi_event){
		fill(color_dark_gray);
		noStroke();
		ellipseMode(CENTER);
		ellipse((multi_event_btn.x +(multi_event_btn.width/2)), (multi_event_btn.y+(multi_event_btn.height/2)), 4, 4);
		ellipseMode(CORNER);
		// point((multi_event_btn.x +(multi_event_btn.width/2)), (multi_event_btn.y+(multi_event_btn.height/2)));
	}
	//text
	fill(color_dark_gray);	
	textAlign(LEFT, CENTER);
	text("multiple events", legend_label_x, multi_event_btn.y+5);
	strokeWeight(1);

	//draw textBox
	rectMode(CORNER);
	if(textBoxActive){
		stroke(color_blue);
	}else{
		stroke(color_gray);
	}
	fill(color_white);
	rect(textBox.x, textBox.y, textBox.width, textBox.height);
	//text inside box
	textAlign(LEFT, CENTER);
	fill(color_dark_gray);
	text(typedString, textBox.x +2, textBox.y+textBox.height/2);

	//drawing Go term
	updateGoDisplay();
	drawGoTermPDF();

	//optional table
	if(hasOptionalTable){
		//table name
		fill(color_dark_gray);	
		textAlign(LEFT, TOP);
		text(_optional_category, legend_x, category_label_y-1); //adjusting the position
		ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(data.optionalTable.values());
		for(int i = 0; i<categories.size(); i++){
			OptionalCategory oc = categories.get(i);
			Rectangle rect = oc.rect;
			//box
			stroke(color_dark_gray);
			if(oc.isSelected){
				fill(color_gray);
			}else{
				noFill();
			}
			rect(rect.x, rect.y, rect.width, rect.height);
			//text
			fill(color_dark_gray);	
			textAlign(LEFT, CENTER);
			text(oc.name+"("+oc.genes.size()+")", rect.x+rect.width+_margin, rect.y +5);
		}
	}
	// translate(0, 0); //set back to origin
	popMatrix();
}


int loading_msg_x = _WIDTH/2;
int loading_msg_y = _HEIGHT/2 - _margin*5;

int loading_bar_w  = 300;
int loading_bar_h = _margin;
int loading_bar_x  = loading_msg_x -(loading_bar_w/2);
int loading_bar_y = loading_msg_y +_margin*3;


public void drawLoadingPage(){
	fill(80);
	textSize(14);
	textAlign(CENTER, BOTTOM);
	text(_current_load_message, loading_msg_x, loading_msg_y);


	//loading bar
	rectMode(CORNER);
	fill(80);
	rect(loading_bar_x, loading_bar_y, loading_bar_w, loading_bar_h);
    fill(160);
    float w = map(_current_data, 0, _total_data, 0, loading_bar_w);
    rect(loading_bar_x, loading_bar_y, w, loading_bar_h);
    textSize(14);
    textAlign(CENTER);



	//logo
	shapeMode(CORNER);
	shape(logo, logo_x, logo_y, logo_w, logo_h);

}
class OptionalCategory implements Comparable{
	ArrayList<Gene>  genes;
	String name;
	boolean isSelected = false;
	Rectangle rect;

	OptionalCategory(String name){
		genes = new ArrayList<Gene>();
		this.name = name;
	}

	public int compareTo(Object obj) {
        OptionalCategory e = (OptionalCategory) obj;
        //compare chromosome first
        return this.name.compareToIgnoreCase(e.name);
    }
}
class Span {
    int start, end;
    ArrayList<Span> cuts;
    
    Span(int leftSide, int rightSide) {
        start = leftSide;
        end = rightSide;
    }
    
    //cut this span by cutSpan
    public ArrayList<Span> cut(Span cutSpan){
        ArrayList<Span> spans= new ArrayList<Span>();
        int cutStart = cutSpan.getStart();
        int cutEnd = cutSpan.getEnd();
        
        
        if(cutStart < start){
            if(cutEnd < start){
                // no overlap
                Span newSpan = new Span(start, end);
                spans.add(newSpan);
            }else if(cutEnd <= end){
                //left cut
                Span newSpan = new Span(cutEnd+1, end);
                spans.add(newSpan);
            }else if(cutEnd > end){
                //whole gene
            }else{
                System.out.println("Debug: cut() error");
            }
        }else if(cutStart <= end){
            if(cutEnd >= end){
                //right cut
                Span newSpan = new Span(start, cutStart-1);
                spans.add(newSpan);
            }else if(cutEnd < end){
                //mid cut
                Span newSpanLeft = new Span(start, cutStart-1);
                Span newSpanRight = new Span(cutEnd+1, end);
                spans.add(newSpanLeft);
                spans.add(newSpanRight);
            }else{
                System.out.println("Dubuyg: cut() error2");
            }
        }else if(cutStart > end){
            //no overlap
            Span newSpan = new Span(start, end);
            spans.add(newSpan);
        }else{
            System.out.println("Debug: cut() error 3:");
        }
        return spans;
    }
    
    public int getStart(){
        return this.start;
    }
    public int getEnd(){
        return this.end;
    }


    public void addCutSpan(Span span) {
        if(cuts == null){
            cuts = new ArrayList<Span>();
        }
        cuts.add(span);
    }
    
    //checks if the entire span is affected.  affected = true, not affected = false
    public boolean getCutResult() {
        //cut the original range and see
        Span temp = new Span(this.start, this.end);
        ArrayList<Span> result = new ArrayList<Span>();
        result.add(temp);
        //iterate through cuts
        for(int  i= 0; i<cuts.size(); i++){
            Span cut = cuts.get(i);
            result = cut.applyToArray(result);
        }
        
        //analyzse result
        if(result.isEmpty()){
            //System.out.println("----Completely deleted.");
            return true;
        }else{
            for(int i = 0; i< result.size(); i++){
                Span s = result.get(i);
               // System.out.println("----Span.getCutResult"+i+":"+s.toString());
            }
            return false;
            
        }
    }

    //called from cut span, update Spans in the array
    private ArrayList<Span> applyToArray(ArrayList<Span> array) {
        ArrayList<Span> result = new ArrayList<Span>();
        for(int i = 0; i< array.size(); i++){
            Span toBeCut = array.get(i);
            ArrayList<Span> afterCut = toBeCut.cut(this);
            if(afterCut != null || !afterCut.isEmpty()){
                result.addAll(afterCut);
            }
        }
        return result;
    }
    
    public String toString(){
        return this.start+"-"+this.end+"("+(end - start)+")";
    }
    
}
String[] modelOrganisms = {"Homo sapiens", "Mus musculus"};
String[] human_build = {"NCBI build 36", "NCBI build 37"};
String[] mouse_build = {"NCBI build 37/mm9", "GRCm build 38/mm10"};

boolean isHuman = true;
boolean isFirstBuild = true;
boolean coordinateIsSet = false;

//optinal build
boolean isUserDefinedBuild = false;

File selectedGVF = null;
File selectedOptional = null;


//coordinates
int logo_x = _margin*10;
int logo_y = _margin*10;
int logo_w = 160;
int logo_h = 100;

int model_organism_text_x = logo_x +logo_w+_margin*5 ;
int model_organism_text_y =  _margin*12;
int model_btn_w = _margin*15;
int model_btn_h = _margin*2;
Rectangle model_one_rect;// = new Rectangle(model_organism_text_x, model_organism_text_y+model_organism_text_h, model_btn_w, model_btn_h);
Rectangle model_two_rect;// = new Rectangle
Rectangle build_one_rect, build_two_rect;
Rectangle file_name_rect, file_btn;
Rectangle optional_name_rect, optional_btn;
int file_textbox_w = _margin*30;
int file_btn_w = _margin*10;

PShape logo;
Rectangle load_btn;

Rectangle promoter_length_text_box;
String promoter_input = "0";
boolean promoter_input_active = false;


//user defined build
Rectangle userDefined_rect;
Rectangle ud_genetrack_rect, ud_cytoband_rect, ud_go_rect;



public void drawStartUpPage(){
	if(!coordinateIsSet){
		int runningX = model_organism_text_x;
		int runningY = model_organism_text_y;
		model_one_rect = new Rectangle(runningX,runningY, model_btn_w, model_btn_h);
		runningX += model_btn_w+_margin;
		model_two_rect = new Rectangle(runningX,runningY, model_btn_w, model_btn_h);
		runningX += model_btn_w+_margin;
		userDefined_rect = new Rectangle(runningX,runningY, file_btn_w, model_btn_h);
		
		//hidden optinal area
		int temp_y = runningY;
		int temp_x = runningX += file_btn_w +_margin*3;
		ud_genetrack_rect = new Rectangle(temp_x, temp_y, _margin*30, model_btn_h);
		temp_y += model_btn_h + _margin*2;
		ud_cytoband_rect = new Rectangle(temp_x, temp_y, _margin*30, model_btn_h);
		temp_y += model_btn_h + _margin*2;
		ud_go_rect = new Rectangle(temp_x, temp_y, _margin*30, model_btn_h);

		//genome build btns
		runningX = model_organism_text_x;
		runningY += model_btn_h+_margin*5;
		build_one_rect = new Rectangle(runningX,runningY, model_btn_w, model_btn_h);
		runningX += model_btn_w+_margin;
		build_two_rect = new Rectangle(runningX,runningY, model_btn_w, model_btn_h);
		//file
		runningX = model_organism_text_x;
		runningY += model_btn_h+_margin*5;
		file_name_rect = new Rectangle(runningX,runningY, file_textbox_w, model_btn_h);
		runningX += file_textbox_w+_margin;
		file_btn = new Rectangle(runningX,runningY, file_btn_w, model_btn_h);
		//optional csv
		runningX = model_organism_text_x;
		runningY += model_btn_h+_margin*5;
		optional_name_rect = new Rectangle(runningX, runningY, file_textbox_w, model_btn_h);
		runningX += file_textbox_w+_margin;
		optional_btn = new Rectangle(runningX, runningY, file_btn_w, model_btn_h);
		//promoter length
		runningX = model_organism_text_x;
		runningY += model_btn_h+_margin*5;
		promoter_length_text_box = new Rectangle(runningX, runningY, model_btn_w, model_btn_h);
		//load
		runningX = model_organism_text_x;
		runningY += model_btn_h+_margin*5;
		load_btn = new Rectangle(runningX, runningY, model_btn_w, model_btn_h);
		//load logo
		logo = loadShape("logo.svg");

	}

	fill(60);
	textAlign(LEFT, BOTTOM);
	textSize(14);
	//model organism
	text("Select an organism:", model_one_rect.x, model_one_rect.y);
	//genome build
	text("Select a build:", build_one_rect.x, build_one_rect.y);
	//file
	text("Select a GVF file:", file_name_rect.x, file_name_rect.y);
	//optional
	text("Optional: Select a csv file:", optional_name_rect.x, optional_name_rect.y);
	//promoter
	text("Define a promoter length:", promoter_length_text_box.x, promoter_length_text_box.y);

	//draw buttons background
	rectMode(CORNER);
	fill(240);
	noStroke();
	rect(model_one_rect.x, model_one_rect.y, model_one_rect.width, model_one_rect.height);
	rect(model_two_rect.x, model_two_rect.y, model_two_rect.width, model_two_rect.height);
	rect(build_one_rect.x, build_one_rect.y, build_one_rect.width, build_one_rect.height);
	rect(build_two_rect.x, build_two_rect.y, build_two_rect.width, build_two_rect.height);
	rect(file_btn.x, file_btn.y, file_btn.width, file_btn.height);
	rect(optional_btn.x, optional_btn.y, optional_btn.width, optional_btn.height);
	rect(load_btn.x, load_btn.y, load_btn.width, load_btn.height);
	//user defined rect
	rect(userDefined_rect.x, userDefined_rect.y, userDefined_rect.width, userDefined_rect.height);
	noFill();
	stroke(200);
	rect(file_name_rect.x, file_name_rect.y, file_name_rect.width, file_name_rect.height);
	rect(optional_name_rect.x, optional_name_rect.y, optional_name_rect.width, optional_name_rect.height);
	//promoter
	rect(promoter_length_text_box.x, promoter_length_text_box.y, promoter_length_text_box.width, promoter_length_text_box.height);

	//draw btn text
	fill(120);
	noStroke();
	textAlign(CENTER, CENTER);
	textSize(12);
	if(isUserDefinedBuild){
		stroke(80);
		text(modelOrganisms[0], (float)model_one_rect.getCenterX(), (float)model_one_rect.getCenterY());
		text(modelOrganisms[1], (float)model_two_rect.getCenterX(), (float)model_two_rect.getCenterY());
		fill(color_pink);
		text("Other", (float)userDefined_rect.getCenterX(), (float) userDefined_rect.getCenterY());
	}else{
		if(isHuman){
			fill(color_pink); 
			text(modelOrganisms[0], (float)model_one_rect.getCenterX(), (float)model_one_rect.getCenterY());
			fill(80);
			text(modelOrganisms[1], (float)model_two_rect.getCenterX(), (float)model_two_rect.getCenterY());
			text("Other", (float)userDefined_rect.getCenterX(), (float) userDefined_rect.getCenterY());
		}else{
			fill(80);
			text(modelOrganisms[0], (float)model_one_rect.getCenterX(), (float)model_one_rect.getCenterY());
			fill(color_pink); 
			text(modelOrganisms[1], (float)model_two_rect.getCenterX(), (float)model_two_rect.getCenterY());
			fill(80);
			text("Other", (float)userDefined_rect.getCenterX(), (float) userDefined_rect.getCenterY());
		}

	}
	if(isUserDefinedBuild){
		//external file info
		stroke(200);
		noFill();
		rect(ud_genetrack_rect.x, ud_genetrack_rect.y, ud_genetrack_rect.width, ud_genetrack_rect.height);
		rect(ud_cytoband_rect.x, ud_cytoband_rect.y, ud_cytoband_rect.width, ud_cytoband_rect.height);
		rect(ud_go_rect.x, ud_go_rect.y, ud_go_rect.width, ud_go_rect.height);

		//box name
		fill(60);
		textAlign(LEFT, BOTTOM);
		textSize(14);
		text("Gene track:", ud_genetrack_rect.x, ud_genetrack_rect.y);
		text("Cytobands:", ud_cytoband_rect.x, ud_cytoband_rect.y);
		text("Go terms:", ud_go_rect.x, ud_go_rect.y);

		textSize(12);
		textAlign(CENTER, CENTER);

	}else{
		if(isHuman){
			fill((isFirstBuild? color_pink:80));
			text(human_build[0], (float)build_one_rect.getCenterX(), (float)build_one_rect.getCenterY());
			fill((!isFirstBuild? color_pink:80));
			text(human_build[1], (float)build_two_rect.getCenterX(), (float)build_two_rect.getCenterY());
		}else{
			fill((isFirstBuild? color_pink:80));
			text(mouse_build[0], (float)build_one_rect.getCenterX(), (float)build_one_rect.getCenterY());
			fill((!isFirstBuild? color_pink:80));
			text(mouse_build[1], (float)build_two_rect.getCenterX(), (float)build_two_rect.getCenterY());
		}
	}

	//load button text
	if(selectedGVF == null){
		fill(100);
	}else{
		fill(load_btn.contains(mouseX, mouseY)? color_pink:80);
	}
	text("start loading", (float)load_btn.getCenterX(), (float)load_btn.getCenterY());

	//select button
	fill( file_btn.contains(mouseX, mouseY)? color_pink:80);
	text("select", (float)file_btn.getCenterX(), (float)file_btn.getCenterY());

	//opitonal select buttion
	fill( optional_btn.contains(mouseX, mouseY)? color_pink:80);
	text("select", (float)optional_btn.getCenterX(), (float)optional_btn.getCenterY());

	//draw selected files path
	if(selectedGVF != null){
		fill(80);
		textAlign(LEFT, CENTER);
		text(" "+selectedGVF.getName(), file_name_rect.x, (float)file_name_rect.getCenterY());
	}
	//draw the selected optional file path
	if(selectedOptional != null){
		fill(80);
		textAlign(LEFT, CENTER);
		text(" "+selectedOptional.getName(), optional_name_rect.x, (float)optional_name_rect.getCenterY());
	}


	//logo
	shapeMode(CORNER);
	shape(logo, logo_x, logo_y, logo_w, logo_h);

	//promoter
	if(promoter_input_active){
		stroke(color_pink);
		strokeWeight(2);
		noFill();
		rect(promoter_length_text_box.x, promoter_length_text_box.y, promoter_length_text_box.width, promoter_length_text_box.height);
		strokeWeight(1);
		//
		fill(80);
		textAlign(RIGHT, CENTER);
		text(promoter_input +" ", promoter_length_text_box.x+promoter_length_text_box.width, (float)promoter_length_text_box.getCenterY());
	}else{
		fill(160);
		textAlign(RIGHT, CENTER);
		text(promoter_input+" ", promoter_length_text_box.x+promoter_length_text_box.width, (float)promoter_length_text_box.getCenterY());
	}
	

}




















class Transcript{
	String kgID;
	int crs; //coding region start
    int cre; //coding region end
    int tsp;
    int tep;
    int exonNum; //number of exons
    int[] exonStart;
    int[] exonEnd;

    Transcript(String id, int t_start, int t_end, int start, int end, int count, int[]starts, int[] ends){
    	kgID = id;
    	crs = start;
    	cre = end;
    	exonNum = count;
    	exonStart = starts;
    	exonEnd = ends;
        tsp = t_start;
        tep = t_end;
    }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Pipit2_0" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
