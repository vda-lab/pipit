private int color_deletion = color(117, 112, 179);
private int color_amplification = color(217, 95, 2);
private int color_variation = color(27, 158, 119);
private int color_gray = 120;//180;
private int color_gray_trans = color(180, 180, 180, 120);
private int color_lightGray = 240;
private int color_dark_gray = 120; //120;
private int color_dark_dark_gray = 80;
private int color_white = 255;
private int transparency = 150;
private int trans2 = 10;
private int color_trans_deletion = color(117, 112, 179, transparency);//color(117, 112, 179, transparency);
private int color_trans_amplification = color(217, 95, 2, transparency);
private int color_trans_variation = color(27, 158, 119, transparency);
private int color_trans2_deletion = color(117, 112, 179, trans2);
private int color_trans2_amplification = color(217, 95, 2, trans2);
private int color_trans2_variation = color(27, 158, 119, trans2);
private int color_green = color(27, 158, 119);
private int color_orange = color(217, 95, 2);
private int color_blue = color(117, 112, 179);
private int color_pink = color(231, 41, 138);
private int color_pink_trans = color(231, 41, 138, transparency);
private int color_cyan_trans =color(0, 174, 237, transparency);
private int color_cyan = color(0, 174, 237);
private int color_limegreen = color(102, 166, 30);
private int color_yellow = color(230, 171, 2);
private int color_brown = color(166, 118, 29);
private int color_grey = color(120);

//colorbrewer
int color_g = color(27, 158,119);
int color_o = color(217,95,2);
int color_bl = color(117,112,179);
int color_p = color(231,41,138);
int color_lg = color(102,166,30);
int color_y = color(230,171,2);
int color_br = color(166,118,29);

int color_g_trans_1 = color(27, 158,119, transparency);
int color_o_trans_1 = color(217,95,2, transparency);
int color_bl_trans_1 = color(117,112,179, transparency);
int color_p_trans_1 = color(231,41,138, transparency);
int color_lg_trans_1 = color(102,166,30, transparency);
int color_y_trans_1 = color(230,171,2, transparency);
int color_br_trans_1 = color(166,118,29, transparency);
int color_grey_trans_1 = color(120, transparency);

int color_g_trans_2 = color(27, 158,119, trans2);
int color_o_trans_2 = color(217,95,2, trans2);
int color_bl_trans_2 = color(117,112,179, trans2);
int color_p_trans_2 = color(231,41,138, trans2);
int color_lg_trans_2 = color(102,166,30, trans2);
int color_y_trans_2 = color(230,171,2, trans2);
int color_br_trans_2 = color(166,118,29, trans2);
int color_grey_trans_2 = color(120, trans2);



int[] color_sv_array = {color_bl_trans_1, color_g_trans_1, color_o_trans_1, color_y_trans_1, color_lg_trans_1, color_br_trans_1, color_grey_trans_1};
int[] color_sv_array2 = {color_bl_trans_2, color_g_trans_2, color_o_trans_2, color_y_trans_2, color_lg_trans_2, color_br_trans_2, color_grey_trans_2};
int[] color_sv_array3 ={color_bl, color_g, color_o,color_y, color_lg, color_br, color_grey};


// private int[] color_array = {this.color_pink, this.color_orange, this.color_yellow, this.color_blue, this.color_green, this.color_green, this.color_green, this.color_orange};
//"deletion","inversion","tandem_duplication", "copy_number_gain", "copy_number_variation","complex_structural_alteration"
// private int[] color_sv_array = {color_trans_deletion, color_trans_variation, color_trans_amplification, color_trans_amplification, color_trans_variation, color_trans_variation};
// private int[] color_sv_array2 = {color_trans2_deletion, color_trans2_variation, color_trans2_amplification, color_trans2_amplification, color_trans2_variation, color_trans2_variation};
// private int[] color_sv_array3 ={color_deletion, color_variation, color_amplification, color_amplification, color_variation, color_variation};

private int color_bubble_selected = 0;


private int color_main_background = 255;
private int color_bubble_background =  240;
private int color_geneView_background = 240;
