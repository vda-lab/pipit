class Band{
	int start, end, stain;
	String name;
	boolean isCentromere = false;

	Band(int bStart, int bEnd, String n, int stainValue){
		start = bStart;
		end = bEnd;
		name = n;
		stain = stainValue;
		if(stain == -1){
			//centromere
			isCentromere = true;
			stain = 0; 
		}
	}
}
