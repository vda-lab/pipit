long _total_data = 0;
long _current_data = 0;
String _current_load_message = "";


class FileLoader implements Runnable{
	void run(){
		if(isUserDefinedBuild){
			_SPECIES = "User_defined";
			_LINK = "http://www.ensembl.org/";

			_CHROMOSOMES = getChromosomeInfo(); //reom cytoband file
		}else{
			if(isHuman){
		        _CHROMOSOMES = _H_CHROMOSOMES;
		        // _EVENTS = _H_EVENTS;
		        if(isFirstBuild){
		            _GENETRACK_FILE = "human_36_gene.txt";
		            _CYTOBAND_FILE = "human_36_cytoband.txt";
		            _GO_FILE = "human_36_goterm.txt";
		            _SPECIES = "Homo_sapiens";
		            _LINK = "http://may2012.archive.ensembl.org/";


		        }else{
		            _GENETRACK_FILE = "human_37_gene.txt";
		            _CYTOBAND_FILE = "human_37_cytoband.txt";
		            _GO_FILE = "human_37_goterm.txt";
		            _SPECIES = "Homo_sapiens";
		            _LINK = "http://www.ensembl.org/";
		        }
		    }else{
		        _CHROMOSOMES = _M_CHROMOSOMES;
		        // _EVENTS = _M_EVENTS;
		        if(isFirstBuild){
		            _GENETRACK_FILE = "mouse_37_gene.txt";
		            _CYTOBAND_FILE = "mouse_37_cytoband.txt";
		            _GO_FILE = "mouse_37_goterm.txt";
		            _SPECIES = "Mus_musculus";
		            _LINK = "http://may2012.archive.ensembl.org/";
		        }else{
		            _GENETRACK_FILE = "mouse_38_gene.txt";
		            _CYTOBAND_FILE = "mouse_38_cytoband.txt";
		            _GO_FILE = "mouse_38_goterm.txt";
		            _SPECIES = "Mus_musculus";
		            _LINK = "http://www.ensembl.org/";
		        }
		    }
			
		}
	    //mouse data
	    // _VALID_EVENT = new boolean[_EVENTS.length];
	    // Arrays.fill(_VALID_EVENT, Boolean.TRUE); //set all the flag to TRUE
	    // _DATA_FILE = selectedGVF;

	    
	    _selected_genes = new ArrayList<Gene>();
	    _selected_genes_by_go = new ArrayList<Gene>();

	    data = new DataSet();
	    data.loadData();            //loads external files
	    data.sortArrayList();       //sort Gene and Event array in HashMap based on their start position
	    data.findMaxChrLength();    //find the very last position of event per chromosome
	    data.checkEventHitGene();   //go through all events and see if they affect any gene
	    data.groupGenes();


	    geneView = createGraphics(_WIDTH, _HEIGHT);
	    updateGeneView();
	    legendView = createGraphics(legend_w, legend_h);
	    initLegendView();
	    assignDisplayPositions(); //assign initial position
	    initBubbles();

	    //affected and unaffected GeneGroup should be sorted
	    println("****debug: affected ="+data.affectedGeneGroup.size()+"  unaffected="+data.unaffectedGeneGroup.size());
	    //index Gene
	    data.indexAffectedGene();
	    data.loadGoTerms();         //loading go terms
	    data.sortGoTerms();
	    // data.createGeneToGoTable();

	    //optional talbe
	    data.loadOptionalCSV();

	    //debug
	    println("number of all the go terms ="+data.all_goMap.size());
	    Iterator ite = data.all_goMap_type.entrySet().iterator();  // Get an iterator
	    while (ite.hasNext()) {
			Map.Entry me = (Map.Entry)ite.next();
			String type = (String) me.getKey();
			ArrayList<Go> array = (ArrayList<Go>)me.getValue();
			println("goTable:"+type+" --- has "+array.size()+" Go terms");
	    }

	    //update LegendView
	    //reinitialize based on if the option table is loaded
	    reinitialiseLegendView();

	    //initiate AniSequence
	    updateLegendView();
	    showLegendView();
	    updateBubbles();


	    //preprocessing
	    //check for multi events
	    checkMultiEvents(); 


	    //end of loading
	    isLoadingData = false;
	    dataLoaded = true;
	}


	//preprocessing
	void checkMultiEvents(){

		int multieventcounter = 0;

		for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
            GeneGroup gg = data.affectedGeneGroup.get(i);
            for(int j = 0; j< gg.genes.size(); j++){
                Gene gene = gg.genes.get(j);
                if(gene.affectingEvents.size() > 1){
		            int firstEventIndex = gene.affectingEvents.get(0).svIndex;
		            for(int k = 1; k<gene.affectingEvents.size(); k++){
		                int newIndex = gene.affectingEvents.get(k).svIndex;
		                if(firstEventIndex != newIndex){
		                    gene.isMultiEvent = true;
		                    multieventcounter ++;
		                    break;
		                }
		            }
		        }

		        if(gene.isMultiEvent){
		        	gg.isMultiEvent = true;
		        }
            }
        }
        println("number of multi event = "+multieventcounter);
	}

	String[] getChromosomeInfo(){
		ArrayList<String> chr_names = new ArrayList<String>();
		String[] lines = loadStrings(_CYTOBAND_FILE);
		for(String line :lines){
			String[] s = split(line, TAB);
			if(s[0].startsWith("#")){
				//ignore
			}else{
				String chr = s[0].trim().toLowerCase();
				if(!chr_names.contains(chr)){
					chr_names.add(chr);
				}
			}
		}
		String[] results = new String[chr_names.size()];
		results = chr_names.toArray(results);
		println("Debug: chr:"+Arrays.toString(results));
		// exit();
		return results;
	}
}
