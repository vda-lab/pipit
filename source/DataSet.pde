

String mouseCytoband = "mouseCytoband.txt";
String mouseGeneTrack = "mousegenes.txt";

String[] _CONSEQUENCES = {"all", "left", "middle", "right", "both_sides"};/////////
String[] _UNIT_PLOT_TYPES;


String _optional_category = "";


class DataSet{
	int[] svTypeCounter;
	int[] maxLength;
	ArrayList<Event> events;
	HashMap<Integer, ArrayList<Band>> cytobands;
	HashMap<Integer, ArrayList<Event>> eventChrMap;
	HashMap<Integer, ArrayList<Gene>> geneChrMap;
	ArrayList<GeneGroup> geneGroupArrays[];

	HashMap<String, ArrayList<Bubble>> bubbleBySvType; //store bubble by 
	ArrayList<GeneGroup> affectedGeneGroup, unaffectedGeneGroup;

	HashMap<String, Gene> affectedGeneMap; //indexing affected Gene
	HashMap<String, Gene> affectedGeneMap_ensembl; //ensembl gene id

	//Gene Ontology terms
	HashMap<String, ArrayList<Go>> all_goMap_type; //type, array of Go
	HashMap<String, Go> all_goMap; 
	ArrayList<String> goTypes;


	//optional category
	HashMap<String, OptionalCategory> optionalTable;


	int match = 0;


	//constructor
	DataSet(){
		events = new ArrayList<Event>();
		cytobands = new HashMap<Integer, ArrayList<Band>>();
		eventChrMap = new HashMap<Integer, ArrayList<Event>>();
		geneChrMap = new HashMap<Integer, ArrayList<Gene>>();
		geneGroupArrays = new ArrayList[_CHROMOSOMES.length];

		affectedGeneGroup = new ArrayList<GeneGroup>();
		unaffectedGeneGroup = new ArrayList<GeneGroup>();
		bubbleBySvType = new HashMap<String, ArrayList<Bubble>>();
		//goterms
		all_goMap_type = new HashMap<String, ArrayList<Go>>();
		all_goMap = new HashMap<String, Go> ();
		goTypes = new ArrayList<String>();


 		svTypeCounter = new int [10];		
	}
	void loadData(){
		loadGVF(_DATA_FILE);
		setupSV_EVENTS();

		loadCytoband(_CYTOBAND_FILE);
		loadGeneTrack(_GENETRACK_FILE);

		//optional file
		// loadOptionalCSV();
		//debug events counts
		for(int i = 0; i<_EVENTS.length; i++){
			println(_EVENTS[i]+":"+svTypeCounter[i]+" events");
		}

		Iterator i = geneChrMap.entrySet().iterator();  // Get an iterator
		while (i.hasNext()) {
		  Map.Entry me = (Map.Entry)i.next();
		  //String key = (String) me.getKey();
		  int chrIndex = (Integer) me.getKey();
		  String chromosomeName =_CHROMOSOMES[chrIndex];
		  print( "debug:"+chromosomeName + " has ");
		  ArrayList<Gene> geneList = (ArrayList<Gene>) me.getValue();
		  println(geneList.size()+" genes");
		}
	}

	//index affected genes into HashMap 
	void indexAffectedGene(){
		affectedGeneMap = new HashMap<String, Gene>();
		//ensembl id specific
		affectedGeneMap_ensembl = new HashMap<String, Gene>();

		for(int j = 0; j<affectedGeneGroup.size(); j++){
			GeneGroup gg = affectedGeneGroup.get(j);
			for(int k = 0; k <gg.genes.size(); k++){
				Gene g = gg.genes.get(k);
				affectedGeneMap.put(g.geneSymbol, g);
				//Ensembl id
				if(!g.ensemblID.equals("NA")){
					affectedGeneMap_ensembl.put(g.ensemblID, g);
				}
			}
		}
		println("---indexed genes count="+affectedGeneMap.size());
	}

	void loadGoTerms(){
		println("---start go terms ----");
		File f;
		if(isUserDefinedBuild){
			f = new File(_GO_FILE);
		}else{
			f = new File(dataPath(_GO_FILE));
		}	
		try{
			//Get the file from the data folder
			BufferedReader reader = createReader(f);
			//Loop to read the file one line at a time
			// File f = new File(dataPath(_GO_FILE));
			//get file size
			_total_data = f.length();
			_current_data = 0;
			_current_load_message = "loading GO:"+f.getName()+" ...";


			String line = null;
			while((line = reader.readLine()) != null) {
				_current_data  += line.length();
				String[] contents = split(line, TAB);
				if (contents[0].startsWith("#")) {  //header line
            		println("HEADER#:" + line);
        		} else {
            		if (contents[0] != null) {
                		//parse data
                		parseGo(contents);
            		} else {
                		println("string content is null");
            		}
        		}
			}
		}catch(IOException e){
			e.printStackTrace();
		}

		println("---end of go terms loading----");
	}
	void parseGo(String[] s){
		//check if it contains geneSymbol
		if(!s[4].equals("n/a")){
			String[] genes = splitTokens(s[4], ",");
			//create a go object
			String name = s[1].trim();
			String type = s[2].trim(); //superclass
			Go go = new Go(name);
			boolean matchFound = false;
			//iterate all assocated gene
			for(int i = 0; i<genes.length; i++){
				Gene gene = affectedGeneMap.get(genes[i]);
				if(gene != null){
					//match found
					matchFound = true;
					//add Gene to Go object
					go.genes.add(gene);
					//add Go to Gene
					gene.addGoTerm(go);
					//get ArrayList //super class
					ArrayList<Go> goArray = (ArrayList<Go>)all_goMap_type.get(type);
					if(goArray == null){
						goArray = new ArrayList<Go>();
						all_goMap_type.put(type, goArray);
						goTypes.add(type);
					}
					if(!goArray.contains(go)){
						goArray.add(go);
					}
				}else{
					// non_match++;
				}
			}
			if(matchFound){
				//save in all go term map
				all_goMap.put(name, go);
			}
		}
	}
	//sort Go objects by its names
	void sortGoTerms(){
		Iterator ite = all_goMap_type.entrySet().iterator();  // Get an iterator
	    while (ite.hasNext()) {
	      Map.Entry me = (Map.Entry)ite.next();
	      String type = (String) me.getKey();
	      ArrayList<Go> array = (ArrayList<Go>)me.getValue();
	      Collections.sort(array);
	    }
	}


	//load .gvf file
	void loadGVF(File file){
		// println("debug: loadGVF():"+ file.getName());
		try{
			//get file size
			_total_data = file.length();
			_current_data = 0;
			_current_load_message = "loading GVF:"+file.getName()+" ...";
			//Get the file from the data folder
			BufferedReader reader = createReader(file);
			//Loop to read the file one line at a time
			String line = null;
			while((line = reader.readLine()) != null) {
				_current_data  += line.length();
				String[] contents = split(line, TAB);
				if (contents[0].startsWith("#")) {  //header line
            		println("#:" + line);
        		} else {
            		if (contents[0] != null) {
                		//parse data
                		parseSV(contents);
            		} else {
                		println("string content is null");
            		}
        		}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	//parse gvf data lines
	void parseSV(String[] s){
		String chromosome = s[0].trim().toLowerCase();
        int chrIndex = _getChromosomeIndex(chromosome);
        String svType = s[2].trim().toLowerCase();
       	//check syntax
       	if(svType.contains(" ")){
       		println("debug:*** need to implement to parse :"+svType);
       	}

        int svIndex = _getEventIndex(svType);
        int s_pos = Integer.parseInt(s[3]);
        int e_pos = Integer.parseInt(s[4]);
        //String[] metaData = s[8].split(";");

        if(svIndex == -1){
        	// println("debug *** sv type that has not yet implementd:"+svType);
        }else if (chrIndex == -1){
        	println("debug: Event chrIndex = -1:"+chromosome);
        }else{
	        //create Event object
	        Event newEvent = new Event(chrIndex, svIndex, s_pos, e_pos);
	        events.add(newEvent);
	        svTypeCounter[svIndex]++;
	        // Integer count = svTypeCounter.svIndex);
	        // count = new Integer(count.intValue()+1);


	        //sort by chromosome
	        ArrayList<Event> eventList = eventChrMap.get(newEvent.chrIndex);
	        if(eventList == null){
	        	eventList = new ArrayList<Event>();
	        	eventChrMap.put(newEvent.chrIndex, eventList);
	        }
	        eventList.add(newEvent);
        }
	}
	//load cytoband file
	void loadCytoband(String file){
		File f;
		if(isUserDefinedBuild){
			f = new File(file);
		}else{
			f = new File(dataPath(file));
		}	
		try{
			_current_load_message = "loading Cytoband:"+file+" ...";
			//Get the file from the data folder
			BufferedReader reader = createReader(f);
			//Loop to read the file one line at a time
			String line = null;
			while((line = reader.readLine()) != null) {

				String[] contents = split(line, TAB);
				if (contents[0].startsWith("#")) {  //header line
            		println("#:" + line);
        		} else {
            		if (contents[0] != null) {
                		//parse data
                		parseCytoband(contents);
            		} else {
                		println("string content is null");
            		}
        		}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	//parse cytoband file
	void parseCytoband(String[] s){
		String chr = s[0].trim().toLowerCase();
		int chrIndex = _getChromosomeIndex(chr);
		int bStart = Integer.parseInt(s[1]);
		int bEnd = Integer.parseInt(s[2]);
		String name = s[3].trim();
		String gStain = s[4].trim().substring(4);
		
        int stain;
        if(s[4].equals("acen")){
        	//centromere
        	stain = -1;
        }else if(s[4].equals("gvar")){
        	stain = 100;
        }else if(s[4].equals("stalk")){
        	stain = 0;
        }else if(gStain.equals("")){
	        stain = 0;
        }else{
            stain = Integer.parseInt(gStain);
        }
        Band band = new Band(bStart, bEnd, name, stain);
        
        //get Array of bands
        ArrayList<Band> bands = this.cytobands.get(chrIndex);
        if(bands == null){
            bands = new ArrayList<Band>();
            cytobands.put(chrIndex, bands);
        }
        bands.add(band);
	}

	//load gene track file
	void loadGeneTrack(String file){
		try{
			File f;
			if(isUserDefinedBuild){
				f = new File(file);
			}else{
				f = new File(dataPath(file));
			}			 
			//get file size
			_total_data = f.length();
			_current_data = 0;
			_current_load_message = "loading Gene:"+file+" ...";

			//Get the file from the data folder
			BufferedReader reader = createReader(file);
			//Loop to read the file one line at a time
			String line = null;
			while((line = reader.readLine()) != null) {
				_current_data += line.length();

				String[] contents = split(line, TAB);
				// println("loadGeneTrack():debug:"+Arrays.toString(contents));
				if (contents[0].startsWith("#") ){  //header line
            		println("geneTrack header line:" + line);
        		} else {
            		if (contents[0] != null) {
                		//parse data
                		parseGeneTrack(contents);
            		} else {
                		println("string content is null");
            		}
        		}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	//parse genetrack file
	void parseGeneTrack(String[] s){
		Gene gene = new Gene(s);
		if(gene.chrIndex != -1){
			ArrayList<Gene> geneList =  geneChrMap.get(gene.chrIndex);
			if(geneList == null){
				geneList = new ArrayList<Gene>();
				geneChrMap.put(gene.chrIndex, geneList);
			}

			//check if the gene already exisit
			Gene exist = checkGeneSymbol(geneList, gene);
			if(exist == null){
				//new gene
				geneList.add(gene);
			}else{
				//exist
				exist.addTranscript(gene);
				//println("debug: gene "+exist.geneSymbol+" already exist! -- "+exist.transcripts.size());
			}
		}
	}
	// checks if a Gene with the same GeneSymbol already exist
	Gene checkGeneSymbol(ArrayList<Gene> array, Gene g){
		for(int i = 0; i<array.size(); i++){
			Gene gene = array.get(i);
			if(gene.geneSymbol.equals(g.geneSymbol)){
				return gene;
			}
		}
		return null;
	}

	//determine the size of chromosome by looking at the end positions of all the events.
	void findMaxChrLength(){
		maxLength = new int[_CHROMOSOMES.length];
        for (int i = 0; i < events.size(); i++) {
            Event e = events.get(i);
            int end = e.getEndPosition();
            int chrIndex = e.getChrIndex();
            if (maxLength[chrIndex] < end) {
                maxLength[chrIndex] = end;
            }
        }
        System.out.println("Debug: max Length=" + Arrays.toString(maxLength));
	}
	//sort ArrayList in HashMap of events and genes
	void sortArrayList(){
		//eventChrMap
		for (Iterator it = eventChrMap.values().iterator(); it.hasNext();) {
            ArrayList<Event> eventArray = (ArrayList<Event>) it.next();
            Collections.sort(eventArray);
        }

		//geneChrMap
		for (Iterator it = geneChrMap.values().iterator(); it.hasNext();) {
            ArrayList<Gene> geneArray = (ArrayList<Gene>) it.next();
            Collections.sort(geneArray);
        }

        println(" ---- end of sortArrayList()");
	}

	//go through all events to see if they hit any genes
	void checkEventHitGene(){
		for(int i=0; i<events.size(); i++){
			Event e = events.get(i);
			int eStart = e.start;
			int eEnd = e.end;
			ArrayList<Gene> chrGenes = (ArrayList<Gene>) geneChrMap.get(e.chrIndex);
			for(int j = 0; j<chrGenes.size(); j++){
				Gene g = chrGenes.get(j);
				int gStart = g.tsp;
				int gEnd = g.tep;
				//consider promoter region
				if(g.strand){
					//forward strand
					gStart = max(0, gStart - _PROMOTER_LENGTH);
				}else{
					//reverse strand
					gEnd = gEnd +_PROMOTER_LENGTH;
				}

				//check where the event is relative to the gene location.		
				if (eStart < gStart) {
                    if (eEnd < gStart) {
                        // next gene
                    } else if (eEnd <= gEnd) {
                        //left cut
                        g.isHit = true;
                        g.addEvent(e, "left");
                    } else if (eEnd > gEnd) {
                        //whole g
                        g.isHit = true;
                        g.addEvent(e, "all");
                    } else {
                        println("Debug: checkIfHit() error");
                    }
                } else if (eStart <= gEnd) {
                    if (eEnd >= gEnd) {
                        //right cut
                        g.isHit = true;
                        g.addEvent(e, "right");
                    } else if (eEnd < gEnd) {
                        //mid cut
                        if (_ONLY_EXOMES) {
                            if (g.containsExons(eStart, eEnd)) {
                                g.isHit = true;
                                g.addEvent(e, "middle");
                            } else {
                                //introns
                            }
                        } else {
                            g.isHit = true;
                            g.addEvent(e, "middle");
                        }

                    } else {
                        println("Dubuyg: checkIfHit() error2");
                    }
                } else if (eStart > gEnd) {
                    //out of range
                } else {
                    println("Debug: checkIfHit() error 3:e:" );//+ eStart % rounding + "-" + eEnd % rounding + "  g:" + gStart % rounding + "-" + gEnd % rounding + "  type=" + e.svIndex + " " + g.getGeneSymbol());
                }
			}
		}
		println("---- end of checkEventHitGene()");
	}

	void groupGenes(){
		//per chromosome
		for (int i = 0; i<_CHROMOSOMES.length; i++){
			GeneGroup geneGroup = null;
			ArrayList<Gene> genes = (ArrayList<Gene>)geneChrMap.get(i);
			ArrayList<GeneGroup> geneGroupArray = new ArrayList<GeneGroup>();
			//go through the genes
			for(int j = 0; j<genes.size(); j++){
				Gene g = genes.get(j);

				if(geneGroup == null){
					//first gene in the chromosome
					geneGroup = new GeneGroup(g);
					geneGroupArray.add(geneGroup);
				}else{
					String[] status = g.getStatus();
					//check if this gene status is the same as the current geneDisplay

					if(geneGroup.hasSameStatus(status)){
						geneGroup.addGene(g);
					}else{
						//create new gene group
						geneGroup = new GeneGroup(g);
						geneGroupArray.add(geneGroup);
					}
				}
			}
			//record arrayList
			geneGroupArrays[i] = geneGroupArray;
		}
	}
	//clear array content
	void resetGeneGroup(){
		affectedGeneGroup.clear();
		unaffectedGeneGroup.clear();
	}
	//check all the loaded event types and assign colors
	void setupSV_EVENTS(){
		_EVENTS = _FOUND_EVENTS.toArray(new String[_FOUND_EVENTS.size()]);
		 _VALID_EVENT = new boolean[_EVENTS.length];
	    Arrays.fill(_VALID_EVENT, Boolean.TRUE); //set all the flag to TRUE

	    //all variation of unit plot types
		int typeCount = _EVENTS.length * (_CONSEQUENCES.length);
		// println("debug: type count ="+typeCount);
		_UNIT_PLOT_TYPES = new String[typeCount];
		for(int i = 0; i< _EVENTS.length; i++){
			for(int j = 0; j<_CONSEQUENCES.length; j++){
				int index = (i*(_CONSEQUENCES.length)+j);
				_UNIT_PLOT_TYPES[index] = _EVENTS[i]+"_"+_CONSEQUENCES[j];
			}
		}

	}
	//loading optional csv
	void loadOptionalCSV(){
		if(selectedOptional != null){
			hasOptionalTable = true;
			File file = selectedOptional;
			try{
				//get file size
				_total_data = file.length();
				_current_data = 0;
				_current_load_message = "loading csv:"+file.getName()+" ...";
				//Get the file from the data folder
				BufferedReader reader = createReader(file);
				//Loop to read the file one line at a time
				String line = null;
				while((line = reader.readLine()) != null) {
					_current_data  += line.length();
					String[] contents = split(line, ',');
            		if (contents[0] != null) {
                		//parse data
                		parseCSV(contents);
            		} else {
                		println("string content is null");
            		}
				}
			}catch(IOException e){
				e.printStackTrace();
			}

		}
		// //debug
		// ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(optionalTable.values());
		// Collections.sort(categories);
		// for(int i = 0; i<categories.size(); i++){
		// 	OptionalCategory oc = categories.get(i);
		// 	println(oc.name+" -- "+ oc.genes.size()+" genes");
		// }


		// Iterator ite = optionalTable.entrySet().iterator();
		// while(ite.hasNext()){
		// 	Map.Entry entry = (Map.Entry)ite.next();
		// 	String category = (String) entry.getKey();
		// 	OptionalCategory group = (OptionalCategory) entry.getValue();
		// 	println("debug: "+category+" --- "+group.genes.size()+" genes");
		// }

	}

	void parseCSV(String[] s){
		String ensemblID = s[0].trim();
		String category = s[1].trim();

		if(ensemblID.startsWith("#")){
			//header line
			_optional_category = category;
		}else{
			//fdind maching gene
			Gene match = (Gene) affectedGeneMap_ensembl.get(ensemblID);
			if(match == null){
				//no match found
			}else{
				//if match found, store the data, create group of them
				if(optionalTable == null){
					//first time
					optionalTable = new HashMap<String, OptionalCategory>();
				}
				//get arraylist
				OptionalCategory list = (OptionalCategory)optionalTable.get(category);
				if(list == null){
					list = new OptionalCategory(category);
					optionalTable.put(category, list);
				}
				//add to the list
				list.genes.add(match);
			}
		}
	}


}
