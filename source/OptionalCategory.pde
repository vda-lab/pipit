class OptionalCategory implements Comparable{
	ArrayList<Gene>  genes;
	String name;
	boolean isSelected = false;
	Rectangle rect;

	OptionalCategory(String name){
		genes = new ArrayList<Gene>();
		this.name = name;
	}

	public int compareTo(Object obj) {
        OptionalCategory e = (OptionalCategory) obj;
        //compare chromosome first
        return this.name.compareToIgnoreCase(e.name);
    }
}
