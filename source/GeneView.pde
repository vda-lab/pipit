int geneView_height = _margin *15;
int geneView_y = _HEIGHT - geneView_height;

int chr_display_radius = _margin/2;
int chr_display_width = _WIDTH - (_margin*8);
int chr_display_x = _margin*4;
int chr_display_x2 = chr_display_x + chr_display_width;
int chr_display_mid_y = _margin*2 + chr_display_radius;
int chr_display_y = _margin*2;

int gene_display_y = chr_display_y + _margin*4;
int event_height = _margin;

Rectangle[] gene_rects = null; 

AniSequence animation_geneView;

boolean showGeneView = false;

//global variable for the range of bp displayed
int c_bp_start = 0;
int c_bp_end = 0;
//array of genes for the GeneView
ArrayList<Gene> _gene_view_genes;

int geneGap = 0;
//left and right button
Rectangle gene_view_left_btn = new Rectangle(0, geneView_y, _margin*4, geneView_height);
Rectangle gene_view_right_btn = new Rectangle(chr_display_x2, geneView_y, _margin*4, geneView_height);


//geneView
void updateCurrentBP(){
    // println("--- updateCurrentBP()");
    if(!_selected_genes.isEmpty()){
        //find the first and end
        int gMin = Integer.MAX_VALUE;
        int gMax = 0;
        for (int i = 0; i < _selected_genes.size(); i++) {
            Gene gene = _selected_genes.get(i);
            if(gene.transcripts !=null){//more than one transcript
                for(int j = 0; j < gene.transcripts.size(); j++){
                    Transcript t = gene.transcripts.get(j);
                    gMin = min(gMin, t.tsp);
                    gMax = max(gMax, t.tep);
                }
            }else{ //only one transcript
                gMin = min(gMin, gene.tsp);
                gMax = max(gMax, gene.tep);
            }
        }
        //number of bp represented by 10 pixels
        float fraction = (float)(gMax - gMin) / (float)chr_display_width;
        geneGap = round(fraction * 10f);
        //add padding
        gMin = gMin - geneGap;
        gMax = gMax + geneGap;

        c_bp_start = gMin;
        c_bp_end = gMax;
        _gene_view_genes = _selected_genes;
    }
}
//called when the geneView range is changed
void updateGeneViewGenes(){
    // println("--- updateGeneViewGenes()");
    if(!_selected_genes.isEmpty()){
        //get all the genes between c_bp_start and c_bp_end
        //_CHROMOSOMES[_C_CHR_INDEX]
        ArrayList<Gene> geneList =(ArrayList<Gene>) data.geneChrMap.get(_C_CHR_INDEX);
        //find the first gene covered 
        Gene first = _selected_genes.get(0);
        int first_index = geneList.indexOf(first);
        // if(first_index > 0){
        // println("debug: while loop start ---");
        while(first_index > 0){
            // println("debug: while loop --- -----------  "+first_index);
            Gene prev = geneList.get(first_index-1);
            if(isOverlapping(prev.start, prev.end, c_bp_start, c_bp_end)){
                //overlapping with display range
                first_index --;
            }else{
                //des not overlap
                break;
            }
        }
        // println("debug: while loop end ---");
        // }
        //end
        Gene end = _selected_genes.get((_selected_genes.size()-1));
        int end_index = geneList.indexOf(end);
        while(end_index  < geneList.size()){
            Gene next = geneList.get(end_index +1);
            if(isOverlapping(next.start, next.end, c_bp_start, c_bp_end)){
                end_index ++;
            }else{
                break;
            }
        }

        //add all genes
        if(_gene_view_genes == null){
            _gene_view_genes = new ArrayList<Gene>();
        }else{
            _gene_view_genes.clear();
        }
        for(int i = first_index; i <= end_index; i++){
            _gene_view_genes.add(geneList.get(i));
        }
    }
}

//checks if two ranges overlaps
boolean isOverlapping(int s1, int e1, int s2, int e2){
    if(e2 < s1) return false;
    if(e1 < s2) return false;
    return true;
}



void  updateGeneView(){

	geneView.textFont(font);
    geneView.smooth();
	geneView.beginDraw();
	geneView.background(color_geneView_background);
	//text
	geneView.fill(color_dark_gray);
	geneView.textAlign(LEFT, TOP);
	geneView.text(_CHROMOSOMES[_C_CHR_INDEX], chr_display_x, _margin/2);
	//draw chromosome
	//outline
    geneView.strokeWeight(11f);
    geneView.stroke(color_gray);
    geneView.strokeCap(ROUND);
    geneView.line(chr_display_x, chr_display_mid_y, chr_display_x2, chr_display_mid_y);
    //white inside
    geneView.strokeWeight(10f);
    geneView.stroke(255);
    geneView.line(chr_display_x, chr_display_mid_y, chr_display_x2, chr_display_mid_y);

    //draw genes and transcripts
    if(!_selected_genes.isEmpty()){
        showGeneView = true;
        //find the first and end
        int gMin = c_bp_start;// Integer.MAX_VALUE;
        int gMax = c_bp_end;// 0;
        // for (int i = 0; i < _selected_genes.size(); i++) {
        //     Gene gene = _selected_genes.get(i);
        //     if(gene.transcripts !=null){//more than one transcript
        //         for(int j = 0; j < gene.transcripts.size(); j++){
        //             Transcript t = gene.transcripts.get(j);
        //             gMin = min(gMin, t.tsp);
        //             gMax = max(gMax, t.tep);
        //         }
        //     }else{ //only one transcript
        //         gMin = min(gMin, gene.tsp);
        //         gMax = max(gMax, gene.tep);
        //     }
        // }
        // //number of bp represented by 10 pixels
        // float fraction = (float)(gMax - gMin) / (float)chr_display_width;
        // int geneGap = round(fraction * 10f);
        // //add padding
        // gMin = gMin - geneGap;
        // gMax = gMax + geneGap;
        int gWidth = gMax - gMin;

        //draw cytobands -------------------------------
        ArrayList<Band> bands = (ArrayList<Band>)data.cytobands.get(_C_CHR_INDEX);
    	int lastPos = bands.get(bands.size()-1).end;
        geneView.rectMode(CORNERS);
        for(int i = 0; i<bands.size(); i++){
            Band b = bands.get(i);
            float bx1 = map(b.start, 0, lastPos, chr_display_x, chr_display_x2);
            float bx2 = map(b.end, 0, lastPos,  chr_display_x, chr_display_x2);
            int fillColor = round(map(b.stain, 0, 100, 255, 150));
            geneView.stroke(color_gray);
            geneView.strokeWeight(1f);
            geneView.fill(fillColor);
            geneView.rect(bx1, chr_display_y, bx2, chr_display_y+_margin);
            //draw ends
            if(i==0 ){
                //beginning
                geneView.strokeWeight(10f);
                geneView.stroke(fillColor);
                geneView.strokeCap(ROUND);
                geneView.point(chr_display_x, chr_display_mid_y);
            }else if(i == bands.size()-1){
                //end
                geneView.strokeWeight(9.5f);
                geneView.stroke(fillColor);
                geneView.strokeCap(ROUND);
                geneView.point(chr_display_x+chr_display_width, chr_display_mid_y);
            }


            //check if they are currently selected rang
            if(b.end < gMin){
            }else{
                if(b.start > gMax){
                }else{
                    //overlap
                    geneView.fill(color_gray);
                    geneView.textAlign(CENTER, BOTTOM);
                    geneView.text(b.name, (bx1+bx2)/2, chr_display_y);
                }
            }
        }

        //draw centromere
        float cStart  = 0f;
        float cEnd  = 0f;
        for(int i = 0; i<bands.size(); i++){
            Band b = bands.get(i);
            if(b.isCentromere){
                float bx1 = map(b.start, 0,lastPos,  chr_display_x, chr_display_x2);
                float bx2 = map(b.end, 0, lastPos,  chr_display_x, chr_display_x2);
                if(cStart == 0f){
                    cStart = bx1;
                    cEnd = bx2;
                }else{
                    cEnd = max(cEnd, bx2);
                }
            }
        }
        float cMid = (cStart + cEnd)/2;
        geneView.stroke(color_gray);
        geneView.strokeWeight(1f);
        geneView.noFill();
        geneView.line(cStart, chr_display_y, cEnd, chr_display_y+_margin);
        geneView.line(cStart, chr_display_y+_margin, cEnd, chr_display_y);

        //draw genes -----------------------------------
        int y_pos;
        int y_gap = _margin;
        ArrayList<Integer> geneEnds = new ArrayList<Integer>();

        //initicalize the rect 
        if(_gene_view_genes.size() < 6){
            gene_rects = new Rectangle[_gene_view_genes.size()];
        }else{
            gene_rects = null;
        }
        //draw genes
        for(int i = 0; i <_gene_view_genes.size(); i++){
        	Gene gene = _gene_view_genes.get(i);
            int geneLabel_x = 0;
            int geneLabel_y = 0;
        	if(geneEnds.isEmpty()){
        		geneEnds.add(gene.end);
        		y_pos = gene_display_y; //initial y pos
        	}else{
        		//find y index
        		int y_index = -1;
        		for(int j = 0; j <geneEnds.size(); j++){
        			if((geneEnds.get(j)+geneGap) > gene.start){
        				//overlap
        			}else{
        				y_index = j;
        				break;
        			}
        		}
        		if(y_index == -1){
        			//new line
        			y_pos = gene_display_y + (geneEnds.size()*y_gap);
        			geneEnds.add(gene.end);
        		}else{
        			//define y_pos
        			y_pos = gene_display_y + (y_index*y_gap);
        			geneEnds.set(y_index, gene.end);
        		}
        	}
            geneLabel_y = y_pos;

        	//check if more than one transcripts
        	if(gene.transcripts == null || gene.transcripts.isEmpty()){
        		//only one trascripts
        		Transcript t = gene.transcript;
        		float t_start = t.tsp - gMin;
        		float t_end = t.tep- gMin;
        		//draw transcript
        		float t_x1 = map(t_start, 0, gWidth, chr_display_x, chr_display_x2);
        		float t_x2 = map(t_end, 0, gWidth, chr_display_x, chr_display_x2);
                geneLabel_x = round((t_x1+t_x2)/2);
        		geneView.stroke(180);
        		geneView.strokeWeight(2);
        		geneView.line(t_x1, y_pos, t_x2, y_pos);

        		//exons
        		int[] exonStart = t.exonStart;
        		int[] exonEnd = t.exonEnd;
	            geneView.stroke(this.color_dark_gray);
	            geneView.strokeWeight(3f);
	            geneView.strokeCap(SQUARE);
        		for (int j = 0; j < exonStart.length; j++) {
		            float eX1 = map(exonStart[j]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
		            float eX2 = map(exonEnd[j]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
		            //for really tiny exomes
		            if((eX2-eX1) < 1f){
		                eX2 = eX1 +1f;
		            }
		            geneView.line(eX1, y_pos, eX2, y_pos);
		        }
    		}else{
    			//multiple transcripts
	        	for(int j = 0; j < gene.transcripts.size(); j++ ){
	        		Transcript t = gene.transcripts.get(j);
	        		//draw transcript
	        		float t_start = t.tsp - gMin;
	        		float t_end = t.tep - gMin;
	        		//draw transcript
	        		float t_x1 = map(t_start, 0, gWidth, chr_display_x, chr_display_x2);
	        		float t_x2 = map(t_end, 0, gWidth, chr_display_x, chr_display_x2);
                    if(j==0){
                        geneLabel_x = round((t_x1+t_x2)/2);
                    }
	        		geneView.stroke(180);
	        		geneView.strokeWeight(2);
	        		geneView.line(t_x1, y_pos, t_x2, y_pos);

	        		//draw exomes
	        		//exons
	        		int[] exonStart = t.exonStart;
	        		int[] exonEnd = t.exonEnd;
		            geneView.stroke(this.color_dark_gray);
		            geneView.strokeWeight(3f);
		            geneView.strokeCap(SQUARE);
	        		for (int k = 0; k < exonStart.length; k++) {
			            float eX1 = map(exonStart[k]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
			            float eX2 = map(exonEnd[k]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
			            //for really tiny exomes
			            if((eX2-eX1) < 1f){
			                eX2 = eX1 +1f;
			            }
			            geneView.line(eX1, y_pos, eX2, y_pos);
			        }
			        y_pos += 3;//stack transcripts
	        	}
    		}
            //gene symbol
            //record gene_rect positions
            if(_gene_view_genes.size() < 6 ){
                //record rect
                int label_width = round(textWidth(gene.geneSymbol)+_margin);
                int label_x = geneLabel_x - (label_width/2);
                int label_y = geneLabel_y - _margin;
                gene_rects[i] = new Rectangle(label_x, label_y, label_width, _margin);


                geneView.textAlign(CENTER, BOTTOM);
                //hover
                if(gene_rects[i].contains(mouseX, (mouseY-geneView_y))){
                    geneView.fill(color_pink);
                }else{
                    geneView.fill(color_dark_dark_gray);
                }
                geneView.text(gene.geneSymbol, geneLabel_x, geneLabel_y);

            }           
        }

        //draw events
        int event_y = gene_display_y + (_margin*geneEnds.size() + _margin);
        //collect all unique events
        HashSet<Event> allEvents = new HashSet<Event>();
        for (int i = 0; i < _gene_view_genes.size(); i++) {
            Gene gene = _gene_view_genes.get(i);
            if(gene.affectingEvents != null){
                allEvents.addAll(gene.affectingEvents);
            }
        }
        ArrayList<Event> events = new ArrayList<Event>();
        events.addAll(allEvents);
        Collections.sort(events);
        // println("debug: events count ="+events.size());
        //start drawing events
        ArrayList<Integer> eventEnds = new ArrayList<Integer>();
        for(int i = 0; i < events.size(); i++){
        	Event e = events.get(i);
        	if(eventEnds.isEmpty()){
        		eventEnds.add(e.end);
        		y_pos = event_y;
        	}else{
        		//find y index
        		int y_index = -1;
        		for(int j = 0; j < eventEnds.size(); j++){
        			if(eventEnds.get(j)+geneGap > e.start){
        				//overlap
        			}else{
        				y_index = j;
        				break;
        			}
        		}
        		if(y_index == -1){
        			//new line
        			y_pos = event_y + (eventEnds.size()*event_height);
        			eventEnds.add(e.end);
        		}else{
        			y_pos = event_y + (y_index *event_height);
        			eventEnds.set(y_index, e.end);
        		}
        	}
        	//draw event at y_pos
        	float ex1 = map(e.start, gMin, gMax, chr_display_x, chr_display_x2);
        	float ex2 = map(e.end, gMin, gMax, chr_display_x, chr_display_x2);
        	geneView.stroke(color_sv_array3[e.svIndex]);
        	geneView.strokeWeight(1);
        	geneView.fill(color_sv_array[e.svIndex]);

        	geneView.rectMode(CORNERS);
        	geneView.rect(ex1, y_pos, ex2, y_pos+event_height);

            // println("debug: e.start="+e.start+"  e.end="+e.end);

            //positions
            // geneView.fill(255);
            // geneView.textAlign(LEFT, TOP);
            // geneView.text(e.start, ex1, y_pos);
            // geneView.textAlign(RIGHT, TOP);
            // geneView.text(e.end, ex2, y_pos);


            //

        }
        //hide event overflow
        geneView.noStroke();
        geneView.fill(color_geneView_background);
        geneView.rectMode(CORNERS);
        geneView.rect(0, gene_display_y - _margin, chr_display_x, _HEIGHT);  //left
        geneView.rect(chr_display_x2, gene_display_y - _margin, _WIDTH, _HEIGHT);//right

        //draw zoom area
        int veryBottomOfEvents = event_y + (eventEnds.size() *event_height) +_margin;
        //on chromosome representaiton
        float sx1 = map(gMin, 0, lastPos,  chr_display_x, chr_display_x2);
        float sx2 = map(gMax, 0, lastPos,  chr_display_x, chr_display_x2);
        if(sx2-sx1 <1){
            sx2 = sx1+1f;
        }
        geneView.stroke(color_cyan_trans);
        geneView.strokeWeight(1);
        geneView.noFill();
        // geneView.fill(240);
        // geneView.fill(color_main_background);
        geneView.beginShape();
        geneView.vertex(chr_display_x, veryBottomOfEvents);
        geneView.vertex(chr_display_x, gene_display_y - _margin);
        geneView.vertex(sx1, gene_display_y - _margin);
        geneView.vertex(sx1, chr_display_y);
        geneView.vertex(sx2, chr_display_y);
        geneView.vertex(sx2, gene_display_y - _margin);
        geneView.vertex(chr_display_x2, gene_display_y - _margin);
        geneView.vertex(chr_display_x2, veryBottomOfEvents);
        geneView.vertex(chr_display_x, veryBottomOfEvents);
        geneView.endShape();

        //draw selected region bp positions
        geneView.fill(color_cyan);
        geneView.textAlign(LEFT, BOTTOM);
        geneView.text(c_bp_start, chr_display_x, gene_display_y - _margin);
        geneView.textAlign(RIGHT, BOTTOM);
        geneView.text(c_bp_end, chr_display_x2, gene_display_y - _margin);

        //mouse over //left button
        if(gene_view_left_btn.contains(mouseX, mouseY)){
            geneView.fill(color_cyan_trans);
            geneView.noStroke();
        }else{
            geneView.noFill();
            geneView.stroke(color_cyan_trans);
            geneView.strokeWeight(1);
        }
        geneView.beginShape();
        geneView.vertex(chr_display_x - _margin, veryBottomOfEvents);
        geneView.vertex(chr_display_x - _margin, gene_display_y - _margin);
        geneView.vertex(chr_display_x - _margin*2, (veryBottomOfEvents + gene_display_y - _margin)/2);
        geneView.vertex(chr_display_x - _margin, veryBottomOfEvents);
        geneView.endShape();

        //right button
        if(gene_view_right_btn.contains(mouseX, mouseY)){
            geneView.fill(color_cyan_trans);
            geneView.noStroke();
        }else{
            geneView.noFill();
            geneView.stroke(color_cyan_trans);
            geneView.strokeWeight(1);
        }
        geneView.beginShape();
        geneView.vertex(chr_display_x2 + _margin, veryBottomOfEvents);
        geneView.vertex(chr_display_x2 + _margin, gene_display_y - _margin);
        geneView.vertex(chr_display_x2 + _margin*2, (veryBottomOfEvents + gene_display_y - _margin)/2);
        geneView.vertex(chr_display_x2 + _margin, veryBottomOfEvents);
        geneView.endShape();



        geneView_height = veryBottomOfEvents +_margin*2;
	}else{
		geneView_height = _margin;
        showGeneView = false;
	}
	geneView.endDraw();

	//update the position
	// Ani.to(this, 1.5, "geneView_y", (_HEIGHT-geneView_height), Ani.EXPO_IN_OUT);
    animation_geneView = new AniSequence(this);
    animation_geneView.beginSequence();
    animation_geneView.beginStep();
    animation_geneView.add(Ani.to(this, 1.5, "geneView_y", (_HEIGHT-geneView_height), Ani.EXPO_IN_OUT));
    animation_geneView.endStep();
    animation_geneView.endSequence();
    animation_geneView.start();
}

void drawGeneViewdPDF(){
    pushMatrix();
    translate(0, geneView_y);

    textFont(font);
    fill(color_geneView_background);
    noStroke();
    rect(0,0, _WIDTH, geneView_height);
    //test
    fill(color_dark_gray);
    textAlign(LEFT, TOP);
    text(_CHROMOSOMES[_C_CHR_INDEX], chr_display_x, _margin/2);
    //draw chromosome
    //outline
    strokeWeight(11f);
    stroke(color_gray);
    strokeCap(ROUND);
    line(chr_display_x, chr_display_mid_y, chr_display_x2, chr_display_mid_y);
    //white inside
    strokeWeight(10f);
    stroke(255);
    line(chr_display_x, chr_display_mid_y, chr_display_x2, chr_display_mid_y);

    //draw genes and transcripts
    if(!_selected_genes.isEmpty()){
        showGeneView = true;
        //find the first and end
        int gMin = c_bp_start;// Integer.MAX_VALUE;
        int gMax = c_bp_end;// 0;
        int gWidth = gMax - gMin;

        //draw cytobands -------------------------------
        ArrayList<Band> bands = (ArrayList<Band>)data.cytobands.get(_C_CHR_INDEX);
        int lastPos = bands.get(bands.size()-1).end;
        rectMode(CORNERS);
        for(int i = 0; i<bands.size(); i++){
            Band b = bands.get(i);
            float bx1 = map(b.start, 0, lastPos, chr_display_x, chr_display_x2);
            float bx2 = map(b.end, 0, lastPos,  chr_display_x, chr_display_x2);
            int fillColor = round(map(b.stain, 0, 100, 255, 150));
            stroke(color_gray);
            strokeWeight(1f);
            fill(fillColor);
            rect(bx1, chr_display_y, bx2, chr_display_y+_margin);
            //draw ends
            if(i==0 ){
                //beginning
                strokeWeight(10f);
                stroke(fillColor);
                strokeCap(ROUND);
                point(chr_display_x, chr_display_mid_y);
            }else if(i == bands.size()-1){
                //end
                strokeWeight(9.5f);
                stroke(fillColor);
                strokeCap(ROUND);
                point(chr_display_x+chr_display_width, chr_display_mid_y);
            }


            //check if they are currently selected range
            if(b.end < gMin){
            }else{
                if(b.start > gMax){
                }else{
                    //overlap
                    fill(color_gray);
                    textAlign(CENTER, BOTTOM);
                    text(b.name, (bx1+bx2)/2, chr_display_y);
                }
            }
        }

        //draw centromere
        float cStart  = 0f;
        float cEnd  = 0f;
        for(int i = 0; i<bands.size(); i++){
            Band b = bands.get(i);
            if(b.isCentromere){
                float bx1 = map(b.start, 0,lastPos,  chr_display_x, chr_display_x2);
                float bx2 = map(b.end, 0, lastPos,  chr_display_x, chr_display_x2);
                if(cStart == 0f){
                    cStart = bx1;
                    cEnd = bx2;
                }else{
                    cEnd = max(cEnd, bx2);
                }
            }
        }
        float cMid = (cStart + cEnd)/2;
        stroke(color_gray);
        strokeWeight(1f);
        noFill();
        line(cStart, chr_display_y, cEnd, chr_display_y+_margin);
        line(cStart, chr_display_y+_margin, cEnd, chr_display_y);

        //draw genes -----------------------------------
        int y_pos;
        int y_gap = _margin;
        ArrayList<Integer> geneEnds = new ArrayList<Integer>();

        //initicalize the rect 
        if(_selected_genes.size() < 6){
            gene_rects = new Rectangle[_selected_genes.size()];
        }else{
            gene_rects = null;
        }
        //draw genes
        for(int i = 0; i <_selected_genes.size(); i++){
            Gene gene = _selected_genes.get(i);
            int geneLabel_x = 0;
            int geneLabel_y = 0;
            if(geneEnds.isEmpty()){
                geneEnds.add(gene.end);
                y_pos = gene_display_y; //initial y pos
            }else{
                //find y index
                int y_index = -1;
                for(int j = 0; j <geneEnds.size(); j++){
                    if((geneEnds.get(j)+geneGap) > gene.start){
                        //overlap
                    }else{
                        y_index = j;
                        break;
                    }
                }
                if(y_index == -1){
                    //new line
                    y_pos = gene_display_y + (geneEnds.size()*y_gap);
                    geneEnds.add(gene.end);
                }else{
                    //define y_pos
                    y_pos = gene_display_y + (y_index*y_gap);
                    geneEnds.set(y_index, gene.end);
                }
            }
            geneLabel_y = y_pos;

            //check if more than one transcripts
            if(gene.transcripts == null || gene.transcripts.isEmpty()){
                //only one trascripts
                Transcript t = gene.transcript;
                float t_start = t.tsp - gMin;
                float t_end = t.tep- gMin;
                //draw transcript
                float t_x1 = map(t_start, 0, gWidth, chr_display_x, chr_display_x2);
                float t_x2 = map(t_end, 0, gWidth, chr_display_x, chr_display_x2);
                geneLabel_x = round((t_x1+t_x2)/2);
                stroke(180);
                strokeWeight(2);
                line(t_x1, y_pos, t_x2, y_pos);

                //exons
                int[] exonStart = t.exonStart;
                int[] exonEnd = t.exonEnd;
                stroke(this.color_dark_gray);
                strokeWeight(3f);
                strokeCap(SQUARE);
                for (int j = 0; j < exonStart.length; j++) {
                    float eX1 = map(exonStart[j]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
                    float eX2 = map(exonEnd[j]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
                    //for really tiny exomes
                    if((eX2-eX1) < 1f){
                        eX2 = eX1 +1f;
                    }
                    line(eX1, y_pos, eX2, y_pos);
                }
            }else{
                //multiple transcripts
                for(int j = 0; j < gene.transcripts.size(); j++ ){
                    Transcript t = gene.transcripts.get(j);
                    //draw transcript
                    float t_start = t.tsp - gMin;
                    float t_end = t.tep - gMin;
                    //draw transcript
                    float t_x1 = map(t_start, 0, gWidth, chr_display_x, chr_display_x2);
                    float t_x2 = map(t_end, 0, gWidth, chr_display_x, chr_display_x2);
                    if(j==0){
                        geneLabel_x = round((t_x1+t_x2)/2);
                    }
                    stroke(180);
                    strokeWeight(2);
                    line(t_x1, y_pos, t_x2, y_pos);

                    //draw exomes
                    //exons
                    int[] exonStart = t.exonStart;
                    int[] exonEnd = t.exonEnd;
                    stroke(this.color_dark_gray);
                    strokeWeight(3f);
                    strokeCap(SQUARE);
                    for (int k = 0; k < exonStart.length; k++) {
                        float eX1 = map(exonStart[k]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
                        float eX2 = map(exonEnd[k]-gMin, 0, gWidth, chr_display_x, chr_display_x2);
                        //for really tiny exomes
                        if((eX2-eX1) < 1f){
                            eX2 = eX1 +1f;
                        }
                        line(eX1, y_pos, eX2, y_pos);
                    }
                    y_pos += 3;//stack transcripts
                }
            }
            //gene symbol
            //record gene_rect positions
            if(_selected_genes.size() < 6 ){
                //record rect
                int label_width = round(textWidth(gene.geneSymbol)+_margin);
                int label_x = geneLabel_x - (label_width/2);
                int label_y = geneLabel_y - _margin;
                gene_rects[i] = new Rectangle(label_x, label_y, label_width, _margin);


                textAlign(CENTER, BOTTOM);
                //hover
                if(gene_rects[i].contains(mouseX, (mouseY-geneView_y))){
                    fill(color_pink);
                }else{
                    fill(color_dark_dark_gray);
                }
                text(gene.geneSymbol, geneLabel_x, geneLabel_y);

            }           
        }

        //draw events
        int event_y = gene_display_y + (_margin*geneEnds.size() + _margin);
        //collect all unique events
        HashSet<Event> allEvents = new HashSet<Event>();
        for (int i = 0; i < _selected_genes.size(); i++) {
            Gene gene = _selected_genes.get(i);
            if(gene.affectingEvents != null){
                allEvents.addAll(gene.affectingEvents);
            }
        }
        ArrayList<Event> events = new ArrayList<Event>();
        events.addAll(allEvents);
        Collections.sort(events);
        // println("debug: events count ="+events.size());
        //start drawing events
        ArrayList<Integer> eventEnds = new ArrayList<Integer>();
        for(int i = 0; i < events.size(); i++){
            Event e = events.get(i);
            if(eventEnds.isEmpty()){
                eventEnds.add(e.end);
                y_pos = event_y;
            }else{
                //find y index
                int y_index = -1;
                for(int j = 0; j < eventEnds.size(); j++){
                    if(eventEnds.get(j)+geneGap > e.start){
                        //overlap
                    }else{
                        y_index = j;
                        break;
                    }
                }
                if(y_index == -1){
                    //new line
                    y_pos = event_y + (eventEnds.size()*event_height);
                    eventEnds.add(e.end);
                }else{
                    y_pos = event_y + (y_index *event_height);
                    eventEnds.set(y_index, e.end);
                }
            }
            //draw event at y_pos
            float ex1 = map(e.start, gMin, gMax, chr_display_x, chr_display_x2);
            float ex2 = map(e.end, gMin, gMax, chr_display_x, chr_display_x2);
            stroke(color_sv_array3[e.svIndex]);
            strokeWeight(1);
            fill(color_sv_array[e.svIndex]);

            rectMode(CORNERS);
            rect(ex1, y_pos, ex2, y_pos+event_height);

            // println("debug: e.start="+e.start+"  e.end="+e.end);

            //positions
            // geneView.fill(255);
            // geneView.textAlign(LEFT, TOP);
            // geneView.text(e.start, ex1, y_pos);
            // geneView.textAlign(RIGHT, TOP);
            // geneView.text(e.end, ex2, y_pos);


            //

        }
        //hide event overflow
        noStroke();
        fill(color_geneView_background);
        rectMode(CORNERS);
        // rect(0, event_y, chr_display_x, _HEIGHT);  //left
        // rect(chr_display_x2, event_y, _WIDTH, _HEIGHT);//right gene_display_y
        rect(0, gene_display_y - _margin, chr_display_x, _HEIGHT);  //left
        rect(chr_display_x2, gene_display_y - _margin, _WIDTH, _HEIGHT);

        //draw zoom area
        int veryBottomOfEvents = event_y + (eventEnds.size() *event_height) +_margin;
        //on chromosome representaiton
        float sx1 = map(gMin, 0, lastPos,  chr_display_x, chr_display_x2);
        float sx2 = map(gMax, 0, lastPos,  chr_display_x, chr_display_x2);
        if(sx2-sx1 <1){
            sx2 = sx1+1f;
        }
        stroke(color_cyan_trans);
        strokeWeight(1);
        noFill();
        //  fill(240);
        //  fill(color_main_background);
        beginShape();
        vertex(chr_display_x, veryBottomOfEvents);
        vertex(chr_display_x, gene_display_y - _margin);
        vertex(sx1, gene_display_y - _margin);
        vertex(sx1, chr_display_y);
        vertex(sx2, chr_display_y);
        vertex(sx2, gene_display_y - _margin);
        vertex(chr_display_x2, gene_display_y - _margin);
        vertex(chr_display_x2, veryBottomOfEvents);
        vertex(chr_display_x, veryBottomOfEvents);
        endShape();

        //mouse over //left button
        if(gene_view_left_btn.contains(mouseX, mouseY)){
            fill(color_cyan_trans);
            noStroke();
        }else{
            noFill();
            stroke(color_cyan_trans);
            strokeWeight(1);
        }
        beginShape();
        vertex(chr_display_x - _margin, veryBottomOfEvents);
        vertex(chr_display_x - _margin, gene_display_y - _margin);
        vertex(chr_display_x - _margin*2, (veryBottomOfEvents + gene_display_y - _margin)/2);
        vertex(chr_display_x - _margin, veryBottomOfEvents);
        endShape();

        //right button
        if(gene_view_right_btn.contains(mouseX, mouseY)){
            fill(color_cyan_trans);
            noStroke();
        }else{
            noFill();
            stroke(color_cyan_trans);
            strokeWeight(1);
        }
        beginShape();
        vertex(chr_display_x2 + _margin, veryBottomOfEvents);
        vertex(chr_display_x2 + _margin, gene_display_y - _margin);
        vertex(chr_display_x2 + _margin*2, (veryBottomOfEvents + gene_display_y - _margin)/2);
        vertex(chr_display_x2 + _margin, veryBottomOfEvents);
        endShape();

        //draw selected region bp positions
        fill(color_cyan);
        textAlign(LEFT, BOTTOM);
        text(c_bp_start, chr_display_x, gene_display_y - _margin);
        textAlign(RIGHT, BOTTOM);
        text(c_bp_end, chr_display_x2, gene_display_y - _margin);

        geneView_height = veryBottomOfEvents +_margin*2;
    }else{
        geneView_height = _margin;
        showGeneView = false;
    }

    popMatrix();
}
