class Event implements Comparable{
	int start, end;
	int chrIndex, svIndex;

	//constructor
	Event(int chrIndex, int svIndex, int s_pos, int e_pos){
		start = s_pos;
		end = e_pos;
		this.chrIndex = chrIndex;
		this.svIndex = svIndex;
	}

	int getStartPosition(){
		return start;
	}
	int getEndPosition(){
		return end;
	}
	int getChrIndex(){
		return chrIndex;
	}



	 public int compareTo(Object obj) {
        Event e = (Event) obj;
        if(chrIndex < e.getChrIndex()){
        	return -1;
        }else if(chrIndex > e.getChrIndex()){
        	return 1;
        }else{
	        if (start < e.getStartPosition()) {
	            return -1;
	        } else if (start > e.getStartPosition()) {
	            return 1;
	        }
	        return 0;
        }
    }
}
