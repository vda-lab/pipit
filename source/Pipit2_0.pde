import java.awt.Rectangle;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Collections;
import java.util.Arrays;
import processing.pdf.*;
import processing.opengl.*;
// import controlP5.*;

static final int _WIDTH = 1200;
static final int _HEIGHT = 700;

String[] _H_CHROMOSOMES = {"chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14",
        "chr15", "chr16", "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrx", "chry"};
String[] _M_CHROMOSOMES = {"chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9", "chr10", "chr11", "chr12", "chr13", "chr14",
        "chr15", "chr16", "chr17", "chr18", "chr19", "chrx", "chry", "chrm"};    
String[] _CHROMOSOMES;
String[] _H_EVENTS = {"deletion","inversion","tandem_duplication", "copy_number_gain", "copy_number_variation","complex_structural_alteration"}; //,"insertion",  "mobile_element_insertion"};
String[] _M_EVENTS = {"deletion","inversion","tandem_duplication", "copy_number_gain", "copy_number_variation","complex_structural_alteration"}; //,"insertion",  "mobile_element_insertion"};
String[] _EXCLUDED_EVENTS = {"insertion",  "mobile_element_insertion"};
String[] _EVENTS;
ArrayList<String> _FOUND_EVENTS;
boolean[] _VALID_EVENT;
boolean _show_multi_event = true;
// String mouseFile = "estd185_Yalcin_et_al_2012.2012-08-30.MGSCv37.gvf";//"mouseData.gvf";
String mouseFile = "mouseData.gvf";
File _DATA_FILE;
String _CYTOBAND_FILE;
String _GENETRACK_FILE;
String _GO_FILE;

int _PROMOTER_LENGTH = 10; // basepair length of promoter
boolean _ONLY_EXOMES = true; //consider only the structural variation that involves Exomes, and located within a gene track

int _VIEW_MODE = 0;   //0= GeneBlock, 1=UnitPlot, 2=BpView, 3 =Expanded, 4=UnitPlot2
String[] _VIEW_MODE_LABEL ={"Collapsed", "Expanded", "Chromosome", "Unit"};
Rectangle[] _VIEW_MODE_RECT = null;
int _C_CHR_INDEX = 0;

String _LINK;
String _SPECIES;


DataSet data;
PFont font;
PShape full_circle, right_circle, left_circle, middle_circle;
// PGraphics topView, bottomView;
PGraphics geneView;
PGraphics legendView;

ArrayList<Gene> _selected_genes;
HashSet<Go> _selected_goTerms = new HashSet<Go>(); /// 
Go _selected_go = null; //searching gene from go

String typedString = "";

//genes selected by go terms
ArrayList<Gene> _selected_genes_by_go;


//optional
ArrayList<Gene> selected_genes_by_category;

//Animation
AniSequence animation;
//saving PDF
boolean saveFrame = false;

//scrollbar interaction
boolean isScrolling = false;
int scrolling_y  = 0;
int beforeScrollIndex = 0;

//flags
boolean isLoadingData = false; // while loading data
boolean dataLoaded = false; //check if the data is loaded
boolean hasOptionalTable = false;//whether user is loading external csv

boolean isTestMode = false;


void setup(){
    // println("setup() ---------");
	// size(_WIDTH, _HEIGHT, OPENGL);
    size(_WIDTH, _HEIGHT);
	font = createFont("Monospaced", 10);
	textFont(font);
    smooth();
    frameRate(30);
    frame.setIconImage( getToolkit().getImage("sketch.icns") );

    //Animation
    Ani.init(this);
    animation = new AniSequence(this);
    animation_geneView = new AniSequence(this);
    animation_legendView = new AniSequence(this);	

    if(isTestMode){
        // isHuman = false;
        // isFirstBuild = true;
        // _DATA_FILE = new File("/Users/Ryo/Desktop/DataSet/DGVa/mouse/estd118_Keane_et_al_2011_MGSCv37-2011_10_19.129P2_OlaHsd.gvf");
        // selectedOptional = new File("/Users/Ryo/Desktop/DataSet/DGVa/mouse/mouseGeneFunction.csv");
//        isHuman = true;
//        isFirstBuild = true;
//        _DATA_FILE = new File("/Users/Ryo/Desktop/DataSet/DGVa/human/estd180_Pang_et_al_2010.2012-04-18.NCBI36.gvf");
//        selectedOptional = new File("/Users/Ryo/Desktop/DataSet/DGVa/human/human_37_haploinsufficiency_score.csv");
//        drawStartUpPage();
//        startLoading();
    }

}

void startLoading(){
    isLoadingData = true;
    Runnable loadingFile  = new FileLoader();
    new Thread(loadingFile).start();   
}


//find the chromosome index from a text label
int _getChromosomeIndex(String chr){
	for (int i = 0; i < _CHROMOSOMES.length; i++) {
        String s = _CHROMOSOMES[i];
        if (s.equals(chr)) {
            return i;
        }
    }
    //debug
    // println("debug: chrIndex = -1:"+chr);
    return -1;
}

//find the event index from the event id
int _getEventIndex(String eventID) {
    // for (int i = 0; i < _M_EVENTS.length; i++) {
    //     String e = _M_EVENTS[i];
    //     if (e.equals(eventID)) {
    //         return i;
    //     }
    // }
    // return -1;

    //first time
    if(_FOUND_EVENTS == null){
        _FOUND_EVENTS = new ArrayList<String>();
    }
    //check if the new event is already included
    if(_FOUND_EVENTS.contains(eventID)){
        return _FOUND_EVENTS.indexOf(eventID);
    }else{
        //check if it is _EXCLUDED_EVENTS
        for(int i = 0; i< _EXCLUDED_EVENTS.length; i++ ){
            if(eventID.equals(_EXCLUDED_EVENTS[i])){
                return -1;
            }
        }   
        //new event
        _FOUND_EVENTS.add(eventID);

        //debug
        // println("debug: eventID="+eventID);

        return _FOUND_EVENTS.indexOf(eventID);
    }
}

//check if the sv type is to show
boolean _isValidSvEvent(String status){
    for(int i = 0 ; i<_EVENTS.length; i++){
        if(status.startsWith(_EVENTS[i])){
            return _VALID_EVENT[i];
        }
    }
    return false;
}



void keyPressed(){
    if(dataLoaded){
        if(textBoxActive){
            //typing gene name
            if((key == BACKSPACE)||(key == DELETE)){
                if(typedString.length() >0){
                    typedString = typedString.substring(0, typedString.length()-1);
                }
            }else if((key >='.') && (key <= 'z')){
                typedString += key;
            }else if((key == ENTER)||(key==RETURN)){
                if(typedString.length() >0){
                    //search for the gene
                    searchGene(typedString);
                }
            }
            updateLegendView();
            // image(legendView, legendView_x, legendView_y);
            loop();
        }else{
            if(key == '1'){
                _VIEW_MODE = 0;
            }else if(key == '2'){
                _VIEW_MODE = 3;
            }else if(key == '3'){
                _VIEW_MODE =2;
            }else if(key == '4'){
                _VIEW_MODE =1;
            }else if(key == '5'){
                _VIEW_MODE =4;
            }else if (key == 'p') {
                println("print");
                saveFrame = true;
            }else if(key == CODED){
                //change bubble alpha
                if(keyCode == UP){
                    _bubble_alpha = min(255, (_bubble_alpha +40));
                }else if(keyCode == DOWN){
                    _bubble_alpha = max(0 , (_bubble_alpha -40));
                }
                println("debug: bubble_alpha ="+_bubble_alpha);
            }   
            updateBubbles();
        }
    }else{
        if(promoter_input_active){
            if((key  >= '0')&&(key <= '9')){
                promoter_input += key;
            }else if((key == BACKSPACE)||(key==DELETE)){
                if(promoter_input.length() >0){
                    promoter_input = promoter_input.substring(0, promoter_input.length()-1);
                }
            }else if((key == ENTER)||(key == RETURN)){
                promoter_input_active = false;
                _PROMOTER_LENGTH = Integer.parseInt(promoter_input);
            }
        }
    }
}

//---------------- mouse interaction -------------------------
void mousePressed(){
    if(dataLoaded){
        //main area
        if(mouseX < legendView_x && mouseY < geneView_y){
            //Main area
            if(mouseY  < view_mode_y+ _margin){ // _VIEW_MODE
                // println("top bar area");
                //check for buttons
                for(int i = 0; i < _VIEW_MODE_RECT.length; i++){
                    Rectangle r = _VIEW_MODE_RECT[i];
                    if(r.contains(mouseX, mouseY)){
                        // println("click: "+i);
                        if(i == 0){
                            _VIEW_MODE = 0;
                        }else if(i == 1){
                            _VIEW_MODE = 3;
                        }else if(i == 2){
                            _VIEW_MODE = 2;
                        }else if(i == 3){
                            _VIEW_MODE = 1;
                        }
                        updateBubbles();
                        return;
                    }
                }
            }

            if(_VIEW_MODE == 0){  //GeneBlock view
                //check for GeneGroup Selection
                boolean selected = false;
                outloop:
                for(int i = 0; i<data.affectedGeneGroup.size(); i++){
                    GeneGroup gg = data.affectedGeneGroup.get(i);
                    if(gg.rect.contains(mouseX, mouseY)){
                        // println("debug: hit! "+gg.genes.get(0).geneSymbol +" "+gg.genes.size());
                        //add or remove genes
                        if(_selected_genes.contains(gg.genes.get(0))){
                            //unselect
                            _selected_genes.removeAll(gg.genes);
                        }else{
                            //select
                            if(keyPressed && keyCode  == SHIFT){
                                //check Chromosome
                                if(_C_CHR_INDEX != gg.genes.get(0).chrIndex){
                                    _selected_genes.clear();
                                }
                            }else{
                                //clear array
                                _selected_genes.clear();
                            }
                            for(Gene g:gg.genes){
                                if(g.isSelectable){
                                    _selected_genes.add(g);
                                    selected = true;
                                    // println(g.geneSymbol +" is selectable!");
                                }else{
                                    // println(g.geneSymbol +" is NOT selectable!");
                                }

                            }
                            // _selected_genes.addAll(gg.genes);
                        }
                        if(selected){
                            _C_CHR_INDEX = gg.genes.get(0).chrIndex; // for geneView
                            break  outloop;
                        }
                    }
                }
                //check unaffectedGeneGroup if none of affected genegroup was selected
                //update view 
                if(selected){
                    //update gene view
                    updateCurrentBP();
                    updateGeneView();
                    //search Go Term
                    searchGoTerm();
                    updateLegendView();

                }else{
                    //unselect all
                    _selected_genes.clear();
                    _selected_goTerms.clear();
                    updateGeneView();
                    updateLegendView();
                }
            }else if(_VIEW_MODE == 1){ //Unit Plot view
                boolean selected = false;
                outloop:
                for(int i = 0; i<data.affectedGeneGroup.size(); i++){
                    GeneGroup gg = data.affectedGeneGroup.get(i);
                    for(int j = 0; j < gg.genes.size(); j++){
                        Gene g = gg.genes.get(j);
                        for(int k = 0; k < g.bubbles.size(); k++){
                            Bubble b = g.bubbles.get(k);
                            if(mouseX > b.newPosition.x && mouseX < (b.newPosition.x+_bubble_d)){
                                if(mouseY >b.newPosition.y && mouseY < (b.newPosition.y +_bubble_d)){
                                    if(_VALID_EVENT[b.event.svIndex]){
                                        //hit
                                        selected = true;
                                        if(_selected_genes.contains(g)){
                                            _selected_genes.remove(g);
                                        }else{
                                            _selected_genes.clear();
                                            _selected_genes.add(g);
                                        }
                                        _C_CHR_INDEX = gg.genes.get(0).chrIndex; // for geneView
                                        break  outloop;
                                    }
                                }
                            }
                        }
                    }
                }
                //update view 
                if(selected){
                    //update main view
                    //update gene view
                    updateCurrentBP();
                    updateGeneView();
                    //search Go Term
                    searchGoTerm();
                    updateLegendView();
                }else{
                    //unselect all
                    _selected_genes.clear();
                    _selected_goTerms.clear();
                    updateGeneView();
                    updateLegendView();
                }
            }else if(_VIEW_MODE == 2){ //BP view
                boolean selected = false;
                if (keyPressed && keyCode == SHIFT) {
                }else{
                    _selected_genes.clear();
                }
                // outloop:
                for(int i = 0; i<data.affectedGeneGroup.size(); i++){
                    GeneGroup gg = data.affectedGeneGroup.get(i);
                    for(int j = 0; j < gg.genes.size(); j++){
                        Gene g = gg.genes.get(j);
                        if(g.isSelectable){
                            Bubble b = g.bubbles.get(0);
                            if(mouseX > b.bpView.x && mouseX < (b.bpView.x+_bubble_d)){
                                if(mouseY >b.bpView.y && mouseY < (b.bpView.y +_bubble_d)){
                                    //hit
                                    selected = true;
                                    if(_selected_genes.contains(g)){
                                        _selected_genes.remove(g);
                                    }else{
                                        if(_C_CHR_INDEX != gg.genes.get(0).chrIndex){
                                            _selected_genes.clear();
                                        }
                                        _selected_genes.add(g);
                                    }
                                    _C_CHR_INDEX = gg.genes.get(0).chrIndex; // for geneView
                                    // break  outloop;
                                   
                                }
                            }   
                        }
                    }
                }
                //update view 
                if(selected){
                    //update main view
                    //update gene view
                    updateCurrentBP();
                    updateGeneView();
                    //search Go Term
                    searchGoTerm();
                    updateLegendView();
                }else{
                    //unselect all
                    _selected_genes.clear();
                    _selected_goTerms.clear();
                    updateGeneView();
                    updateLegendView();
                }
            }else if(_VIEW_MODE == 3){ //Expanded view
                boolean selected = false;
                outloop:
                for(int i = 0; i<data.affectedGeneGroup.size(); i++){
                    GeneGroup gg = data.affectedGeneGroup.get(i);
                    for(int j = 0; j < gg.genes.size(); j++){
                        Gene g = gg.genes.get(j);
                        if(g.isSelectable){
                            Bubble b = g.bubbles.get(0);
                            if(mouseX > b.newPosition.x && mouseX < (b.newPosition.x+_bubble_d)){
                                if(mouseY >b.newPosition.y && mouseY < (b.newPosition.y +_bubble_d)){
                                    //hit
                                    selected = true;
                                    if(_selected_genes.contains(g)){
                                        _selected_genes.remove(g);
                                    }else{
                                        if (keyPressed && keyCode == SHIFT) {
                                            if(_C_CHR_INDEX != gg.genes.get(0).chrIndex){
                                                _selected_genes.clear();
                                            }
                                        }else{
                                            _selected_genes.clear();
                                        }
                                        _selected_genes.add(g);
                                    }
                                    _C_CHR_INDEX = gg.genes.get(0).chrIndex; // for geneView
                                    break  outloop;
                                }
                            }   
                        }
                    }
                }
                //update view 
                if(selected){
                    //update main view
                    //update gene view
                    updateCurrentBP();
                    updateGeneView();
                    //search Go Term
                    searchGoTerm();
                    updateLegendView();
                }else{
                    //unselect all
                    _selected_genes.clear();
                    _selected_goTerms.clear();
                    updateGeneView();
                    updateLegendView();
                }
            }
        }else if(mouseX > legendView_x && mouseY < geneView_y){
            //Legend area
            //legendView
            if(legendView_x < mouseX && (legendView_y+legend_h) > mouseY){
                if(legend_close_rect.contains(mouseX, mouseY)){
                    // println("legend rectangle!");
                    showLegend = !showLegend;
                    if(showLegend){
                        showLegendView();
                    }else{
                        hideLegendView();
                    }
                }else{
                    if(showLegend){ //when legend is visible
                        //radio button
                        int mapped_x =( mouseX - legendView_x);
                        int mapped_y = (mouseY - legendView_y);
                        for(int i = 0; i < radio_btns.length; i++){
                            if(radio_btns[i].contains(mapped_x, mapped_y)){
                                //toggle
                                _VALID_EVENT[i] = !_VALID_EVENT[i];
                                updateLegendView();     //update legend look
                                updateGeneGroups();     //assign new positions based on _VIEW_MODE
                                updateBubbles();
                                break;
                            }else if(multi_event_btn.contains(mapped_x, mapped_y)){
                                // println("debug show multi event click!");
                                _show_multi_event = !_show_multi_event;
                                updateLegendView();     //update legend look
                                updateBubbles();
                                break;
                            }
                        }

                        //optional table
                        if(hasOptionalTable){
                            //check if any box clicked
                            ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(data.optionalTable.values());
                            for(int i = 0; i<categories.size(); i++){
                                OptionalCategory oc = categories.get(i);
                                if(oc.rect.contains(mapped_x, mapped_y)){
                                    // println("!!!!!!!!!!!!!OC click: "+oc.name);
                                    oc.isSelected = !oc.isSelected;
                                    updateLegendView();

                                    if(oc.isSelected){
                                        //add to selected gene list
                                        if(selected_genes_by_category == null){
                                            selected_genes_by_category = new ArrayList<Gene>();
                                        }
                                        selected_genes_by_category.addAll(oc.genes);
                                    }else{
                                        //remove from seleged gene
                                        selected_genes_by_category.removeAll(oc.genes);
                                    }



                                    // println("Debug: selected gene count = "+selected_genes_by_category.size());

                                    break;
                                }
                            }
                        }

                        //textBox
                        if(textBox.contains(mapped_x, mapped_y)){
                            textBoxActive = !textBoxActive;
                            updateLegendView();
                            return;
                        }
                        textBoxActive = false;

                        //check if it is scrollbar
                        if(scrollBar != null && scrollBar.contains(mapped_x, mapped_y)){
                            // println("scroll bar!");
                            isScrolling = true;
                            scrolling_y = mouseY;
                            beforeScrollIndex = goTermIndex;

                            // println("degbug: index before ="+goTermIndex);
                        }else if(goTermRect.contains(mapped_x, mapped_y)){ //go term
                            //find the index
                            int index = findGoRectIndex(mouseX, mouseY);
                            if(index == -1){
                                println("out side of goTermRectangle area");
                            }else{
                                Go selected = goToDisplay[index];
                                if(selected != null){
                                    // println("selected:"+selected.toString());
                                    if(selected.isSuperclass){
                                        //type
                                        GoType gs = (GoType) selected;
                                        int typeIndex = getGoTypeIndex(gs.type);
                                        foldedGoTypes[typeIndex] =  !foldedGoTypes[typeIndex];
                                        _selected_genes_by_go.clear();//reset
                                        // _selected_goTerms.clear();
                                        _selected_go = null;
                                    }else{
                                        //selecting go term
                                        if(_selected_go != selected){
                                            _selected_genes_by_go.clear();//reset
                                            _selected_genes_by_go.addAll(selected.genes);
                                            if(_selected_genes.size() >0){
                                                //nothing
                                            }else{
                                                _selected_goTerms.clear();
                                                
                                            }
                                            
                                            _selected_go = selected;
                                        }else{
                                            //unselect
                                            _selected_genes_by_go.clear();//reset
                                            // _selected_goTerms.clear();
                                            _selected_go = null;
                                        }
                                        // _selected_goTerms.add(selected);
                                    }
                                }
                            }
                            updateLegendView();
                        }
                    }
                }
            }else{
                textBoxActive = false;
                updateLegendView();
            }
        }else if(showGeneView && mouseY > geneView_y){
            //check if it is over gene_rects
            if(gene_rects != null){
                for(int i = 0; i<gene_rects.length; i++){
                    Rectangle rect = gene_rects[i];
                    if(rect.contains(mouseX, (mouseY - geneView_y))){
                        // get gene and pop on browser
                        Gene gene = _selected_genes.get(i);
                        String chr = gene.chromosome.toUpperCase().replace("CHR", "");
                        // println("pop up for "+gene.geneSymbol);
                        open(_LINK+_SPECIES+"/Location/View?r="+chr+":"+gene.tsp+"-"+gene.tep);
                        // new BrowserLauncher().openURLinBrowser("http://www.google.com");
                        return;
                    }
                }
                //no mouse over gene names
            }
            if(gene_view_left_btn.contains(mouseX, mouseY)){
                // println("debug: to the left!");
                c_bp_start -= 1000;
                updateGeneViewGenes();
                updateGeneView();

            }else if(gene_view_right_btn.contains(mouseX, mouseY)){
                c_bp_end += 1000;
                updateGeneViewGenes();
                updateGeneView();
            }

        }else{
            //the rest
        }
    }else{
        //check mouse positions
        if(model_one_rect.contains(mouseX, mouseY)){
            isHuman = true;
            isUserDefinedBuild = false;
        }else if( model_two_rect.contains(mouseX, mouseY)){
            isHuman = false;
            isUserDefinedBuild = false;
        }else if(build_one_rect.contains(mouseX, mouseY)){
            isFirstBuild = true;
        }else if(build_two_rect.contains(mouseX, mouseY)){
            isFirstBuild = false;
        }else if(file_btn.contains(mouseX, mouseY)){
            println("Select a file");
            selectInput("Select a GVF file:", "gvfSelection");
            // loop();
        }else if(optional_btn.contains(mouseX, mouseY)){
            println("select a optionalfile");
            selectInput("Select an optional csv file:", "optionalSelection");

        }else if(load_btn.contains(mouseX, mouseY)){
            //start loading
            if(selectedGVF != null){
                _DATA_FILE = selectedGVF;
                isLoadingData = true;
                startLoading();
            }
        }else if(promoter_length_text_box.contains(mouseX, mouseY)){
            promoter_input_active = !promoter_input_active;

            if(promoter_input_active){
                //input mode
                if(promoter_input.equals("0")){
                    promoter_input = "";
                }
            }else{
                //done
                if(promoter_input.equals("")){
                    promoter_input = "0";
                    _PROMOTER_LENGTH = Integer.parseInt(promoter_input);
                }else{
                    _PROMOTER_LENGTH = Integer.parseInt(promoter_input);
                }
            }
            loop();
        }else if(userDefined_rect.contains(mouseX, mouseY)){
            isUserDefinedBuild = !isUserDefinedBuild;
            selectInput("Select a gene track file:", "userGenetrack");
            loop();

        }else{
            promoter_input_active = false;
            if(promoter_input.equals("")){
                promoter_input = "0";
                _PROMOTER_LENGTH = Integer.parseInt(promoter_input);
            }else{
                _PROMOTER_LENGTH = Integer.parseInt(promoter_input);
            }
            loop();
        }


    }
}

void mouseDragged(){
    if(isScrolling){
        int difference = mouseY - scrolling_y;// - mouseY;
        int change = round(map(difference, -1*(goTerm_h - scrollBar.height), goTerm_h - scrollBar.height, -1*(totalGoCount-1), totalGoCount-1)); //-1*(maxGoRowCount-1), (maxGoRowCount-1)));
        
        goTermIndex = constrain(beforeScrollIndex + change, 0, abs(totalGoCount - maxGoRowCount));
        // println("drag action difference= "+difference+"  change="+change +"  goTerm index="+goTermIndex);
        //update goTermIndex;
        updateLegendView();
        loop();
    }
}

void mouseReleased(){
    if(dataLoaded){
        isScrolling = false;
        loop();
    }
}
void mouseMoved(){
    if(dataLoaded){
        if(showLegend && legendView_rect.contains(mouseX, mouseY)){
            //updateLegendView();
        }else if(showGeneView && mouseY > geneView_y){
            if(gene_view_right_btn.contains(mouseX, mouseY) || gene_view_left_btn.contains(mouseX, mouseY)){
                cursor(HAND);
                updateGeneView();
                loop();
                return;
            }
            //check if it is over gene_rects
            if(gene_rects != null){
                for(int i = 0; i<gene_rects.length; i++){
                    Rectangle rect = gene_rects[i];
                    if(rect.contains(mouseX, (mouseY - geneView_y))){
                        cursor(HAND);
                        updateGeneView();// draw the view
                        loop();
                        return;
                    }
                }
                //no mouse over gene names
                cursor(ARROW);
                updateGeneView();
                loop();
                return;
            }
        }
    }else{
        if(model_one_rect.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if( model_two_rect.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(build_one_rect.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(build_two_rect.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(file_btn.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(load_btn.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(promoter_length_text_box.contains(mouseX, mouseY)){
            cursor(HAND);
        }else if(userDefined_rect.contains(mouseX, mouseY)){
            cursor(HAND);
        }else{
            cursor(ARROW);
        }
    }

}
//called once at the beginning
void initBubbles(){
    for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
        GeneGroup gg = data.affectedGeneGroup.get(i);
        for(int j = 0; j< gg.genes.size(); j++){
            Gene gene = gg.genes.get(j);
            for(int k = 0; k < gene.bubbles.size(); k++){
                Bubble b = gene.bubbles.get(k);
                b.display_x = b.newPosition.x;
                b.display_y = b.newPosition.y;
            }
        }
    }
}

//called when the _VIEW_MODE changes
void updateBubbles(){
    updateGeneGroups(); //
    animation  = new AniSequence(this);
    animation.beginSequence();
    animation.beginStep();
    for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
        GeneGroup gg = data.affectedGeneGroup.get(i);
        for(int j = 0; j< gg.genes.size(); j++){
            Gene gene = gg.genes.get(j);
            for(int k = 0; k < gene.bubbles.size(); k++){
                Bubble b = gene.bubbles.get(k);
                b.update();
            }
        }
    }
    animation.endStep();
    animation.endSequence();
    animation.start();
    loop();
}

void searchGene(String geneName){
    println("searchGene(): "+geneName);
    for(int  i = 0; i<data.affectedGeneGroup.size(); i++){
        GeneGroup gg = data.affectedGeneGroup.get(i);
        for(int j = 0; j<gg.genes.size(); j++){
            Gene g = gg.genes.get(j);
            if(g.geneSymbol.equals(geneName)){
                //gene match
                _selected_genes.clear();
                _selected_genes.add(g);
                updateGeneView();
                textBoxActive = false;// make the text box inactive
                //search Go Term
                searchGoTerm();
                updateLegendView();
                break;
            }
        }
    }
    println("gene not found");

}


void searchGoTerm(){
    //for all the selected Genes
    _selected_goTerms.clear();
    for(int i = 0; i<_selected_genes.size();i++){
        Gene gene = _selected_genes.get(i);
        ArrayList<Go> goArray = gene.goTerms;
        if(goArray != null){
            _selected_goTerms.addAll(goArray);
        }
    }
    // println("number of associated Go terms are "+_selected_goTerms.size());
}
//called when file is selected
void gvfSelection(File selection){
    if(selection == null){
        // println("file was not selected");
        selectedGVF = null;
        _DATA_FILE = selectedGVF;
    }else{
        _DATA_FILE = selection;
        selectedGVF = _DATA_FILE;
        file_textbox_w = round(textWidth(_DATA_FILE.getName())+_margin);
        file_name_rect.width = file_textbox_w;
        //check genome assembly
        checkGenomeAssembly();
    }
}
//called when optinal file is selected
void optionalSelection(File selection){
    if(selection == null){
        // println("file was not selected");
        selectedOptional = null;
    }else{
        selectedOptional = selection;
    }
}
//called when user selects GVF file
void checkGenomeAssembly(){
    // println("checkGenomeAssembly()");
    try{
        //Get the file from the data folder
        BufferedReader reader = createReader(_DATA_FILE);
        //Loop to read the file one line at a time
        String line = null;
        while((line = reader.readLine()) != null) {
            if(line.startsWith("##")){
                String[] contents = splitTokens(line);
                if(contents[0].equals("##genome-build")){
                    // println("genomebuild:" + Arrays.toString(contents));
                    if(contents[2].equals("NCBI36")){
                        isHuman = true;
                        isFirstBuild = true;
                    }else if(contents[2].equals("NCBI37")){
                        isHuman = true;
                        isFirstBuild = false;
                    }else if(contents[2].equals("MGSCv37")){
                        isHuman = false;
                        isFirstBuild = true;
                    }else if(contents[2].equals("NCBI38")){
                        isHuman = false;
                        isFirstBuild = false;
                    }else{
                        println("Error: unknown genome build:"+contents[2]);
                    }
                }
            } else {
                break;
            }
        }
    }catch(IOException e){
        e.printStackTrace();
    }
}

//user Defined function
void userGenetrack(File selection){
    if(selection == null){
        isUserDefinedBuild = false;
    }else{
        _GENETRACK_FILE = selection.getAbsolutePath();
        selectInput("Select a cytoband file:", "userCytoband");
    }
}
void userCytoband(File selection){
    if(selection == null){
        isUserDefinedBuild = false;
    }else{
        _CYTOBAND_FILE = selection.getAbsolutePath();
        selectInput("Select a go term file:", "userGo");
    }
}

void userGo(File selection){
    if(selection == null){
        isUserDefinedBuild = false;
    }else{
        _GO_FILE = selection.getAbsolutePath();
    }
}


