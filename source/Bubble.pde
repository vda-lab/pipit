class Bubble{
	float display_x, display_y;
	PVector bpView;
	PVector newPosition;
	Event event;
	Gene parentGene;
	String type;
	// PShape pshape;

	//constructor
	Bubble(Gene gene, Event e, String t){
		event = e;
		parentGene = gene;
		type = t;
	}
	
	void render(){
		// set color
		int svIndex = event.svIndex;
		fill(color_sv_array3[svIndex], _bubble_alpha);
		noStroke();
		if(type.endsWith("all")){
			_drawCircle(display_x, display_y, _bubble_d, _bubble_d);
		}else if(type.endsWith("left")){
			_drawHalfCircle(display_x, display_y, _bubble_d, _bubble_d, true);
		}else if(type.endsWith("middle")){
			_drawMiddleOfCircle(display_x, display_y, _bubble_d, _bubble_d);
		}else if(type.endsWith("right")){
			_drawHalfCircle(display_x, display_y, _bubble_d, _bubble_d, false);
		}else{
			println("debug:GUI: unexpected bubble type: "+type);
		}

		// _drawCircle(display_x, display_y, _bubble_d, _bubble_d);
		// shape(pshape, display_x, display_y, _bubble_d, _bubble_d);

		//outline
		// if(_VIEW_MODE != 2 && !parentGene.isMultiEvent){
		if(_VIEW_MODE != 2 ){

			stroke(color_gray);
			strokeWeight(0.5f);
			noFill();
			ellipse( display_x, display_y, _bubble_d, _bubble_d);
		}
	}
	//highlight selected bubble
	void highlight(){
		stroke(color_pink);
		strokeWeight(2f);
		noFill();
		ellipse( display_x, display_y, _bubble_d, _bubble_d);
	}
	void highlight_soft(){
		stroke(color_cyan, 80);
		strokeWeight(8f);
		noFill();
		ellipse( display_x, display_y, _bubble_d, _bubble_d);
	}

	//highlight selected bubble by category
	void highlight_by_category(){
		stroke(color_dark_dark_gray);
		// stroke(color_pink);
		strokeWeight(3f);
		noFill();
		ellipse( display_x-1, display_y-1, _bubble_d+2, _bubble_d+2);
	}

	void drawBackground(){
		// println("drawbackground()");
		noStroke();
		fill(color_bubble_background);
		ellipse( display_x, display_y, _bubble_d, _bubble_d);
	}

	void update(){
		float targetX =0;
		float targetY = 0;
		if(_VIEW_MODE == 0){
			// targetX = geneBlock.x;
			// targetY = geneBlock.y;
			targetX = newPosition.x;
			targetY = newPosition.y;
		}else if(_VIEW_MODE == 1){
			// targetX = unitPlot.x;
			// targetY = unitPlot.y;
			targetX = newPosition.x;
			targetY = newPosition.y;
		}else if(_VIEW_MODE == 2){
			targetX = bpView.x;
			targetY = bpView.y;
		}else if(_VIEW_MODE == 3){
			targetX = newPosition.x;
			targetY = newPosition.y;
			// targetX = expanded.x;
			// targetY = expanded.y;
		}else if(_VIEW_MODE == 4){
			//unit plot 2
			targetX = newPosition.x;
			targetY = newPosition.y;
		}


		animation.add(Ani.to(this, 1.5, "display_x", targetX, Ani.EXPO_IN_OUT));
		animation.add(Ani.to(this, 1.5, "display_y", targetY, Ani.EXPO_IN_OUT));
	}
	//multievent
	void renderMultiEvent(){
		fill(60);
		noStroke();
		// dot in the middle
		// ellipseMode(CENTER);
		// float cx = display_x +((float)_bubble_d/2f);
		// float cy = display_y +((float)_bubble_d/2f);
		// ellipse(cx, cy, 4, 4);
		// ellipseMode(CORNER);

		// ellipseMode(CORNER);
		float cx = display_x +((float)_bubble_d/2f) -2;
		float cy = display_y +((float)_bubble_d/2f) -2;
		ellipse(cx, cy, 4, 4);
	}

}
