

Rectangle goTermRect;
int maxGoRowCount = 30;
int goRowHeight = 10;
int goTerm_w = _margin*19;
int goTerm_h = maxGoRowCount*goRowHeight;// _margin*50;
int goTermScroller_w = _margin;
Rectangle scrollBar = null;

boolean[] foldedGoTypes = null; //flags for go terms
// String[] goTypes = null;


int legend_x = _margin;
int legend_label_x = legend_x+_margin*2;
int legend_top_margin = _margin;
int legend_w = _margin*22;
int legend_h = _HEIGHT - _margin*15;//_margin*15 + goTerm_h;
int legend_label_gap = _margin/2;

int legendView_x = _WIDTH - legend_w;// - _margin; //position on actual display
int legendView_y = 0;						
Rectangle legend_close_rect = new Rectangle(legendView_x, 0, legend_w, _margin);
Rectangle legendView_rect = new Rectangle(legendView_x, 0, legend_w, legend_h);
boolean showLegend = true;
Rectangle[] radio_btns;

//multi event toggle
Rectangle multi_event_btn;

Rectangle textBox;
boolean textBoxActive = false;

//optional categories
int category_label_y = 0;

int goTermIndex = 0; //starting point of Go
int totalGoCount = 0;
Rectangle[] goRectangles = new Rectangle[maxGoRowCount];
Go[] goToDisplay = new Go[maxGoRowCount];

// PFont goTermFont;
PFont legendFont = createFont("Monospaced", 10);

AniSequence animation_legendView;


//setup coordinates
void initLegendView(){
	radio_btns = new Rectangle[_EVENTS.length];
	int running_y = legend_top_margin;
	for(int i = 0; i<_EVENTS.length; i++){
		radio_btns[i] = new Rectangle(legend_x, running_y, _margin,_margin);
		running_y += _margin+legend_label_gap;
	}
	//multi event toggle
	multi_event_btn = new Rectangle(legend_x, running_y, _margin, _margin);
	running_y +=  _margin+legend_label_gap;
	//textBox
	running_y+= _margin*2;
	textBox = new Rectangle(legend_x, running_y, _margin*18, 15);
	running_y+= 15 +_margin;

	//optional
	//categories
	if(hasOptionalTable){
		category_label_y = running_y;
		running_y += _margin;
		ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(data.optionalTable.values());
		Collections.sort(categories);
		for(int i = 0; i<categories.size(); i++){
			OptionalCategory oc = categories.get(i);
			oc.rect = new Rectangle(legend_x, running_y, _margin, _margin);
			running_y+= _margin + legend_label_gap;
		}
		running_y += _margin;
	}

	//remaining height
	int remainingHegiht = legend_h - running_y;
	maxGoRowCount = remainingHegiht / goRowHeight -1;
	goTerm_h = goRowHeight * maxGoRowCount;
	goTermRect = new Rectangle(legend_x, running_y, goTerm_w, goTerm_h);

	goRectangles = new Rectangle[maxGoRowCount];
	goToDisplay = new Go[maxGoRowCount];


	// // //calculate how much space left
	// int remaining_h = legend_h - running_y;
	// maxGoRowCount = remaining_h / goRowHeight -1;
	// goTerm_h = maxGoRowCount * goRowHeight;

	// println("debug: GoRowCount ="+maxGoRowCount+"  running_y ="+running_y+" legend_h="+legend_h);

	// goTermRect = new Rectangle(legend_x, running_y, goTerm_w, goTerm_h);

	//goTermFont
	// goTermFont = loadFont("XLMonoAlt-10.vlw");
}

void reinitialiseLegendView(){
	if(hasOptionalTable){
		//reorganize the position
		radio_btns = new Rectangle[_EVENTS.length];
		int running_y = legend_top_margin;
		for(int i = 0; i<_EVENTS.length; i++){
			radio_btns[i] = new Rectangle(legend_x, running_y, _margin,_margin);
			running_y += _margin+legend_label_gap;
		}
		//multi event toggle
		multi_event_btn = new Rectangle(legend_x, running_y, _margin, _margin);
		running_y +=  _margin+legend_label_gap;

		//textBox
		running_y+= _margin;
		textBox = new Rectangle(legend_x, running_y, _margin*18, 15);
		running_y+= 15 +_margin;

		if(hasOptionalTable){
			//categories
			category_label_y = running_y;
			running_y += _margin;
			ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(data.optionalTable.values());
			Collections.sort(categories);
			for(int i = 0; i<categories.size(); i++){
				OptionalCategory oc = categories.get(i);
				oc.rect = new Rectangle(legend_x, running_y, _margin, _margin);
				running_y+= _margin + legend_label_gap;
			}
			running_y += _margin;
			
		}

		//remaining height
		int remainingHegiht = legend_h - running_y;
		maxGoRowCount = remainingHegiht / goRowHeight -1;
		goTerm_h = goRowHeight * maxGoRowCount;
		goTermRect = new Rectangle(legend_x, running_y, goTerm_w, goTerm_h);

		goRectangles = new Rectangle[maxGoRowCount];
		goToDisplay = new Go[maxGoRowCount];


		println("debug: GoRowCount ="+maxGoRowCount+"  running_y ="+running_y+" legend_h="+legend_h);
	}
}

void updateLegendView(){
	legendView.beginDraw();
	legendView.textFont(legendFont);
	legendView.textSize(10);
	legendView.smooth();
	legendView.background(240);
	//event toggle buttons
	for(int i = 0; i<_EVENTS.length; i++){
		//draw 
		Rectangle rect = radio_btns[i];
		if(_VALID_EVENT[i]){
			legendView.stroke(color_sv_array3[i]);
			legendView.fill(color_sv_array[i]);
		}else{
			legendView.stroke(color_sv_array3[i]);
			legendView.noFill();
		}
		legendView.rectMode(CORNER);
		legendView.rect(rect.x, rect.y, rect.width, rect.height);

		legendView.fill(color_dark_gray);	
		legendView.textAlign(LEFT, CENTER);
		legendView.text(_EVENTS[i], legend_label_x, rect.y +5);
	}

	//multi event
	//circle
	legendView.stroke(color_dark_gray);
	legendView.noFill();
	legendView.ellipseMode(CORNER);
	legendView.ellipse(multi_event_btn.x, multi_event_btn.y, multi_event_btn.width, multi_event_btn.height);
	
	if(_show_multi_event){
		legendView.strokeWeight(4);
		legendView.point((multi_event_btn.x +(multi_event_btn.width/2)), (multi_event_btn.y+(multi_event_btn.height/2)));
	}
	//text
	legendView.fill(color_dark_gray);	
	legendView.textAlign(LEFT, CENTER);
	legendView.text("multiple events", legend_label_x, multi_event_btn.y+5);


	//draw textBox_______________________________________________________
	legendView.rectMode(CORNER);
	if(textBoxActive){
		legendView.stroke(color_blue);
	}else{
		legendView.stroke(color_gray);
	}
	legendView.fill(color_white);
	legendView.strokeWeight(1);
	legendView.rect(textBox.x, textBox.y, textBox.width, textBox.height);
	//text inside box
	legendView.textAlign(LEFT, CENTER);
	legendView.fill(color_dark_gray);
	legendView.text(typedString, textBox.x +2, textBox.y+textBox.height/2);

	//drawing Go term
	updateGoDisplay();
	drawGoTerm();

	//optional table
	if(hasOptionalTable){
		//table name
		legendView.fill(color_dark_gray);	
		legendView.textAlign(LEFT, TOP);
		legendView.text(_optional_category, legend_x, category_label_y-1); //adjusting the position
		ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(data.optionalTable.values());
		for(int i = 0; i<categories.size(); i++){
			OptionalCategory oc = categories.get(i);
			Rectangle rect = oc.rect;
			//box
			legendView.stroke(color_dark_gray);
			if(oc.isSelected){
				legendView.fill(color_gray);
			}else{
				legendView.noFill();
			}
			legendView.rect(rect.x, rect.y, rect.width, rect.height);
			//text
			legendView.fill(color_dark_gray);	
			legendView.textAlign(LEFT, CENTER);
			legendView.text(oc.name+"("+oc.genes.size()+")", rect.x+rect.width+_margin, rect.y +5);
		}
	}
	legendView.endDraw();
}

//update which Go to dsiplay
void updateGoDisplay(){
	//first time set rectangles
	if(foldedGoTypes == null){
		foldedGoTypes = new boolean[data.goTypes.size()];
		Arrays.fill(foldedGoTypes, Boolean.TRUE);
		int running_y = goTermRect.y;
		for(int i = 0; i< maxGoRowCount; i++){
			goRectangles[i] = new Rectangle(goTermRect.x, running_y, goTerm_w, goRowHeight);
			running_y+=goRowHeight;
		}
	}
	//find total count
	ArrayList<Go> totalGo = new ArrayList<Go>();

	for(int i = 0; i<data.goTypes.size(); i++){
		GoType goType = new GoType(data.goTypes.get(i));//superclass
		totalGo.add(goType);
		if(foldedGoTypes[i]){
			//folded
			//add selected go term
			ArrayList<Go> array = (ArrayList<Go>) data.all_goMap_type.get(data.goTypes.get(i));
			for(int j = 0; j<array.size(); j++){
				Go go = array.get(j);
				if(_selected_goTerms.contains(go)){
					totalGo.add(go);
				}
			}
		}else{
			//unfolded, add all Go terms
			ArrayList<Go> array = (ArrayList<Go>) data.all_goMap_type.get(data.goTypes.get(i));
			totalGo.addAll(array);
		}
	}	

	//add to goToDisplay
	goToDisplay = new Go[maxGoRowCount];
	for(int i = 0; i<maxGoRowCount; i++){
		int index = goTermIndex + i;
		if(index < totalGo.size() && index >= 0 ){
			goToDisplay[i] = totalGo.get(index);
		}else{
			goToDisplay[i] = null;
		}
	}
	totalGoCount = totalGo.size();
}

void drawGoTerm(){
	legendView.rectMode(CORNER);
	for(int i = 0; i< maxGoRowCount; i++){
		Rectangle rect = goRectangles[i];
		Go go = goToDisplay[i];
		if(go != null){
			//check if it is style
			if(go.isSuperclass){
				GoType gs = (GoType)go;
				//draw style
				String typeLabel = gs.type;
				int typeIndex = getGoTypeIndex(gs.type);
				//draw the type 
				if(foldedGoTypes[typeIndex]){
					//folded
					typeLabel = "> "+gs.type;
				}else{
					//unfolded
					typeLabel = "v "+gs.type;
				}
				legendView.fill(color_dark_gray);
				legendView.noStroke();
				legendView.rect(rect.x, rect.y, rect.width, rect.height);
				legendView.fill(240);
				legendView.textAlign(LEFT, TOP);

				legendView.text(typeLabel, rect.x, rect.y);
				// println("debug: go style ="+gs.style);
			}else{
				// //draw Go
				if(_selected_go == go){
					legendView.fill(color_cyan);
				}else if(_selected_goTerms.contains(go)){
					legendView.fill(color_pink);
				}else{
					legendView.fill(color_dark_gray);
				}
				legendView.textAlign(LEFT, TOP);
				legendView.text(go.name, rect.x, rect.y);
			}
		}else{
			break;
		}
	}

	//hide over flow
	legendView.fill(240);
	legendView.noStroke();
	legendView.rectMode(CORNER);
	legendView.rect(goTermRect.x +goTerm_w, goTermRect.y, goTermScroller_w+_margin, goTerm_h);

	//draw scroller
	if(totalGoCount > maxGoRowCount){
		legendView.rectMode(CORNER);
		legendView.fill(255);
		legendView.stroke(color_dark_gray);
		legendView.rect((goTermRect.x+goTerm_w), goTermRect.y, goTermScroller_w, goTerm_h);

		//draw displayed region
		int startIndex = goTermIndex;
		int endIndex = goTermIndex +(maxGoRowCount-1);
		int dy = round(map(startIndex, 0, totalGoCount-1, goTermRect.y, goTermRect.y+goTerm_h));
		int dh = round(map(maxGoRowCount-1, 0, totalGoCount-1, 0, goTerm_h));
		scrollBar = new Rectangle(goTermRect.x + goTerm_w, dy, goTermScroller_w, dh);

		legendView.fill(color_gray);
		legendView.rect(scrollBar.x, scrollBar.y, scrollBar.width, scrollBar.height);
	}

	// legendView.textFont(legendFont);

}

void drawGoTermPDF(){
	rectMode(CORNER);
	for(int i = 0; i< maxGoRowCount; i++){
		Rectangle rect = goRectangles[i];
		Go go = goToDisplay[i];
		if(go != null){
			//check if it is style
			if(go.isSuperclass){
				GoType gs = (GoType)go;
				//draw style
				String typeLabel = gs.type;
				int typeIndex = getGoTypeIndex(gs.type);
				//draw the type 
				if(foldedGoTypes[typeIndex]){
					//folded
					typeLabel = "> "+gs.type;
				}else{
					//unfolded
					typeLabel = "v "+gs.type;
				}
				fill(color_dark_gray);
				noStroke();
				rect(rect.x, rect.y, rect.width, rect.height);
				fill(240);
				textAlign(LEFT, TOP);

				text(typeLabel, rect.x, rect.y);
				// println("debug: go style ="+gs.style);
			}else{
				// //draw Go
				if(_selected_go == go){
					fill(color_cyan);
				}else if(_selected_goTerms.contains(go)){
					fill(color_pink);
				}else{
					fill(color_dark_gray);
				}
				textAlign(LEFT, TOP);
				text(go.name, rect.x, rect.y);
			}
		}else{
			break;
		}
	}

	//hide over flow
	fill(240);
	noStroke();
	rectMode(CORNER);
	rect(goTermRect.x +goTerm_w, goTermRect.y, goTermScroller_w+_margin, goTerm_h);

	//draw scroller
	if(totalGoCount > maxGoRowCount){
		rectMode(CORNER);
		fill(255);
		stroke(color_dark_gray);
		rect((goTermRect.x+goTerm_w), goTermRect.y, goTermScroller_w, goTerm_h);

		//draw displayed region
		int startIndex = goTermIndex;
		int endIndex = goTermIndex +(maxGoRowCount-1);
		int dy = round(map(startIndex, 0, totalGoCount-1, goTermRect.y, goTermRect.y+goTerm_h));
		int dh = round(map(maxGoRowCount-1, 0, totalGoCount-1, 0, goTerm_h));
		scrollBar = new Rectangle(goTermRect.x + goTerm_w, dy, goTermScroller_w, dh);

		fill(color_gray);
		rect(scrollBar.x, scrollBar.y, scrollBar.width, scrollBar.height);
	}

}

void hideLegendView(){
	animation_legendView = new AniSequence(this);
    animation_legendView.beginSequence();
    animation_legendView.beginStep();
    animation_legendView.add(Ani.to(this, 1.5, "legendView_y", (_margin - legend_h), Ani.EXPO_IN_OUT));
    animation_legendView.endStep();
    animation_legendView.endSequence();
    animation_legendView.start();
	// Ani.to(this, 1.5, "legendView_y", (_margin - legend_h), Ani.EXPO_IN_OUT);
}
void showLegendView(){
	animation_legendView = new AniSequence(this);
    animation_legendView.beginSequence();
    animation_legendView.beginStep();
    animation_legendView.add(Ani.to(this, 1.5, "legendView_y", 0, Ani.EXPO_IN_OUT));
    animation_legendView.endStep();
    animation_legendView.endSequence();
    animation_legendView.start();
	// Ani.to(this, 1.5, "legendView_y", 0, Ani.EXPO_IN_OUT);
}

int findGoRectIndex(int x, int y){
	int relative_x = x - legendView_x;
	int relative_y = y - legendView_y;
	for(int i = 0; i < goRectangles.length; i++){
		if(goRectangles[i].contains(relative_x, relative_y)){
			return i;
		}
	}
	return -1;
}

int getGoTypeIndex(String s){
	return data.goTypes.indexOf(s);
}

//pdf export
void drawLegendViewPDF(){
	textFont(legendFont);
	fill(240);
	noStroke();
	pushMatrix();
	translate(legendView_x, legendView_y);
	rect(0, 0, legend_w, legend_h);
	strokeWeight(1);
	for(int i = 0; i<_EVENTS.length; i++){
		//draw 
		Rectangle rect = radio_btns[i];
		if(_VALID_EVENT[i]){
			stroke(color_sv_array3[i]);
			fill(color_sv_array[i]);
		}else{
			stroke(color_sv_array3[i]);
			noFill();
		}
		rectMode(CORNER);
		rect(rect.x, rect.y, rect.width, rect.height);

		fill(color_dark_gray);	
		textAlign(LEFT, CENTER);
		text(_EVENTS[i], legend_label_x, rect.y +5);
	}
	//multi event
	//circle
	stroke(color_dark_gray);
	noFill();
	ellipseMode(CORNER);
	ellipse(multi_event_btn.x, multi_event_btn.y, multi_event_btn.width, multi_event_btn.height);
	if(_show_multi_event){
		fill(color_dark_gray);
		noStroke();
		ellipseMode(CENTER);
		ellipse((multi_event_btn.x +(multi_event_btn.width/2)), (multi_event_btn.y+(multi_event_btn.height/2)), 4, 4);
		ellipseMode(CORNER);
		// point((multi_event_btn.x +(multi_event_btn.width/2)), (multi_event_btn.y+(multi_event_btn.height/2)));
	}
	//text
	fill(color_dark_gray);	
	textAlign(LEFT, CENTER);
	text("multiple events", legend_label_x, multi_event_btn.y+5);
	strokeWeight(1);

	//draw textBox
	rectMode(CORNER);
	if(textBoxActive){
		stroke(color_blue);
	}else{
		stroke(color_gray);
	}
	fill(color_white);
	rect(textBox.x, textBox.y, textBox.width, textBox.height);
	//text inside box
	textAlign(LEFT, CENTER);
	fill(color_dark_gray);
	text(typedString, textBox.x +2, textBox.y+textBox.height/2);

	//drawing Go term
	updateGoDisplay();
	drawGoTermPDF();

	//optional table
	if(hasOptionalTable){
		//table name
		fill(color_dark_gray);	
		textAlign(LEFT, TOP);
		text(_optional_category, legend_x, category_label_y-1); //adjusting the position
		ArrayList<OptionalCategory> categories = new ArrayList<OptionalCategory>(data.optionalTable.values());
		for(int i = 0; i<categories.size(); i++){
			OptionalCategory oc = categories.get(i);
			Rectangle rect = oc.rect;
			//box
			stroke(color_dark_gray);
			if(oc.isSelected){
				fill(color_gray);
			}else{
				noFill();
			}
			rect(rect.x, rect.y, rect.width, rect.height);
			//text
			fill(color_dark_gray);	
			textAlign(LEFT, CENTER);
			text(oc.name+"("+oc.genes.size()+")", rect.x+rect.width+_margin, rect.y +5);
		}
	}
	// translate(0, 0); //set back to origin
	popMatrix();
}
