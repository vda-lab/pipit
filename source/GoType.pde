class GoType extends Go{
	String type;

	GoType(String s){
		super(null);
		type = s;
		isSuperclass = true;
	}
}
