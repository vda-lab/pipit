
int _margin = 10;
int title_x = _margin*3;
int title_y = _margin*3;
int title_height = _margin*3;

int view_mode_x;
int view_mode_y = title_y;


int label_x = _margin;
int label_y = title_height + _margin*2;
int label_width = _margin * 5;
int[] label_y_pos; //keep track of label positions
int[] label_y_pos_expanded; //expanded view
int[] unit_plot_label_y;//keep track of label positions for unit plot

int chr_x = label_x+label_width+_margin;
int chr_x_end = _WIDTH - _margin;
int chr_y = label_y;


int _bubble_d = _margin;
int _connector = _bubble_d/2;
int _bubble_gap = _margin;
int _bubble_left_end = _WIDTH -_margin*25; //how far bubble goes to left
int _bubble_alpha = 255;

String[] unitplot2_labels;


Rectangle[] chromosomeDisplaySpace; //keep track of chromosome length for bp display


// Assign positions for each view
void assignDisplayPositions(){
	updateGeneGroupArray(); //sort GeneGroup to affected and unaffected
	createBubbles();
	assignGeneBlockViewPositions();
	initBpViewPositions(); //this value does not change dynamically
}

//sorts to arrays based on if the GeneGroup is affected
void updateGeneGroupArray(){
	data.resetGeneGroup(); //reset
	for(int i = 0; i < data.geneGroupArrays.length; i++){
		//per chromosome
		ArrayList<GeneGroup> geneGroups = data.geneGroupArrays[i];
		for(int j = 0; j<geneGroups.size(); j++){
			GeneGroup gg = geneGroups.get(j);
			//check status
			if(gg.status != null){
					data.affectedGeneGroup.add(gg); //add to ArrayList
			}else{
				//unaffected genes
				data.unaffectedGeneGroup.add(gg); // add to ArrayList
			}
		}
	}
}
//create bubble for all affected GeneGroup
void createBubbles(){
	for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
		GeneGroup gg = data.affectedGeneGroup.get(i);
		for(int j = 0; j< gg.genes.size(); j++){
			Gene gene = gg.genes.get(j);
			gene.bubbles = new ArrayList<Bubble>();
			for(int k = 0; k<gene.affectingEvents.size(); k++){
				Event event = gene.affectingEvents.get(k);
				//sort bubble by sv type
				String sv = _EVENTS[event.svIndex];
				String consequence = gene.eventConsequence.get(k);
				String type = sv+"_"+consequence;
				Bubble b = new Bubble(gene, event, type);
				//record Bubble in Gene
				gene.bubbles.add(b);
				//hashmap for unit plot
				ArrayList<Bubble> bubbles =(ArrayList<Bubble>) data.bubbleBySvType.get(type);
				if(bubbles == null){
					bubbles = new ArrayList<Bubble>();
					data.bubbleBySvType.put(type, bubbles);
				}
				bubbles.add(b);
			}
		}
	}
}
//BP positions are assigned only once
void initBpViewPositions(){
	//BpPlot coordinates -------------------------------
	//find chromosome display space
	chromosomeDisplaySpace = new Rectangle[_CHROMOSOMES.length];
	for(int i = 0; i< _CHROMOSOMES.length; i++){
		int chr_start = 0; 
		int chr_end = data.maxLength[i];
		int display_x1 = round(map(chr_start, 0, data.maxLength[0], chr_x, _bubble_left_end));
		int display_x2 = round(map(chr_end, 0, data.maxLength[0], chr_x, _bubble_left_end)); 
		int display_w = display_x2 - display_x1;
		chromosomeDisplaySpace[i] = new Rectangle(display_x1, label_y_pos[i], display_w, _bubble_d);
	}
	//per chromosome
	int running_x = chr_x;
	int running_y = label_y;  // use same y coordinates as GeneBlock view
	for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
		GeneGroup gg = data.affectedGeneGroup.get(i);
		for(int j = 0; j< gg.genes.size(); j++){
			Gene gene = gg.genes.get(j);
			int gene_start = gene.tsp;
			int gene_end = gene.tep;
			float gene_mid = (gene_end+gene_start)/2;
			int chr_index = gene.chrIndex;
			int chr_start = 0; 
			int chr_end = data.maxLength[chr_index];
			Rectangle chr_rect = chromosomeDisplaySpace[chr_index];
			int x_pos = round(map(gene_mid, chr_start, chr_end, chr_rect.x, chr_rect.x+chr_rect.width));

			//set posisiton for all bubbles
			for(int k = 0; k< gene.bubbles.size(); k++){
				Bubble b = gene.bubbles.get(k);
				b.bpView = new PVector(x_pos, chr_rect.y);
			}
		}
	}
}

//called when the view changes
void updateGeneGroups(){
    if(_VIEW_MODE == 0){
        assignGeneBlockViewPositions();
    }else if(_VIEW_MODE == 1){
        assignUnitPlotViewPositions();
    }else if(_VIEW_MODE == 2){
    	//position is already in memory
    }else if(_VIEW_MODE == 3){
        assignGeneExpandedViewPositions();
    }else if(_VIEW_MODE == 4){
    	assignUnitPlot2ViewPositions();
    }
}
//dynamically set positions
void assignGeneBlockViewPositions(){
	// println("assignGeneBlockViewPositions----");
	//GeneBlock view -----------------------------------
	int running_x = chr_x; 
	int running_y = label_y;
	label_y_pos = new int[_CHROMOSOMES.length];
	for(int i = 0; i < data.geneGroupArrays.length; i++){
		//per chromosome
		label_y_pos[i] = running_y;
		running_x = chr_x;
		GeneGroup previousUnaffected = null;// check if the previous is unaffected
		ArrayList<GeneGroup> geneGroups = data.geneGroupArrays[i];
		for(int j = 0; j<geneGroups.size(); j++){
			GeneGroup gg = geneGroups.get(j);
			//check status
			if(gg.status != null){
				if(checkValidGeneGroup(gg.status)){
					Rectangle rect = new Rectangle(running_x, running_y, _bubble_d, _bubble_d);
					gg.rect  = rect;
					for(int k = 0; k < gg.genes.size(); k++){
						Gene gene = gg.genes.get(k);
						ArrayList<Bubble> bubbles = gene.bubbles;
						//assgin position for the bubbles representing the gene group
						for(Bubble b:bubbles){
							b.newPosition = new PVector(running_x, running_y);
						}
					}
					running_x += _bubble_d;
					previousUnaffected = null;
					gg.isShown = true;
				}else{
					//hidden chromosome
					gg.isShown = false;
				}
			}else if(gg.status == null){
				//unaffected genes
				if(previousUnaffected == null){
					Rectangle rect = new Rectangle(running_x, running_y, _connector, _bubble_d);
					gg.rect = rect;
					running_x += _connector; //shorter than full length
					previousUnaffected = gg;
				}else{
					//stack unaffected ones
					gg.rect = previousUnaffected.rect;
				}	
			}
			//check the current running_x
			if(running_x >= _bubble_left_end){	
				running_x = chr_x;
				running_y += _bubble_d;	//go to next line
			}
		}
		//next chromosome
		running_y += _bubble_d+_bubble_gap;
	}
}
void assignUnitPlotViewPositions(){
	//UnitPlot coordinates ------------------------------
	//per sv type
	int running_x = chr_x;
	int running_y = label_y;
	unit_plot_label_y = new int[_EVENTS.length];
	int currentEventCounter = 0;
	unit_plot_label_y[0] = running_y; //set the first one

	// int midDeletionCounter  = 0;
	for(int i = 0; i < _UNIT_PLOT_TYPES.length; i++){
		running_x = chr_x;
		String type = _UNIT_PLOT_TYPES[i];
		//check for label y position
		if(type.startsWith(_EVENTS[currentEventCounter])){
			//same
		}else{
			currentEventCounter ++;
			unit_plot_label_y[currentEventCounter] = running_y;
		}
		ArrayList<Bubble> bubbles = (ArrayList<Bubble>) data.bubbleBySvType.get(type);
		if(bubbles == null){
			//no bubble for this type of event
			//println("no bubble for "+type +" i ="+i);
		}else{
			String prevGeneSymbol ="";
			for(int j = 0; j<bubbles.size(); j++){
				Bubble b = bubbles.get(j);
				String newGeneSymbol = b.parentGene.geneSymbol;
				//check if it overlaps with the previous one
				boolean isOverlapping = false;
				if(prevGeneSymbol.equals("")){
					//first time
					prevGeneSymbol = newGeneSymbol;
				}else if(newGeneSymbol.equals(prevGeneSymbol)){
					prevGeneSymbol = newGeneSymbol;
					isOverlapping = true;
				}else{
					prevGeneSymbol = newGeneSymbol;
				}
				//assign position
				if(isOverlapping){
					//previous position
					b.newPosition = new PVector(running_x, running_y);
				}else{
					//check the current running_x
					if(running_x >= _bubble_left_end){
						//go to next line
						running_x = chr_x;
						running_y += _bubble_d;
					}
					running_x += _bubble_d;
					b.newPosition = new PVector(running_x, running_y);
				}
			}
			//next type
			running_y += _bubble_d +_bubble_gap;
		}
	}
}
void assignUnitPlot2ViewPositions(){
	//create hashmap table first
	HashMap<String, ArrayList<Gene>> unitplot2 = new HashMap<String, ArrayList<Gene>>(); //status, array
	for(int i = 0; i<data.affectedGeneGroup.size(); i++){
		GeneGroup gg = data.affectedGeneGroup.get(i);
		ArrayList<Gene> genes = gg.genes;
		for(int j = 0; j<genes.size(); j++){
			Gene gene = genes.get(j);
			ArrayList<String> status = new ArrayList<String>();
			status.addAll(gene.status);
			if(status.size() == 1){
				//one event
				String svName = status.get(0);
				ArrayList<Gene> geneArray = unitplot2.get(svName);
				if(geneArray == null){
					geneArray = new ArrayList<Gene>();
					unitplot2.put(svName, geneArray);
				}
				geneArray.add(gene);
			}else{
				//multiple event
				String svName = "multi_events";
				ArrayList<Gene> geneArray = unitplot2.get(svName);
				if(geneArray == null){
					geneArray = new ArrayList<Gene>();
					unitplot2.put(svName, geneArray);
				}
				geneArray.add(gene);
			}
		}
	}

	//debug
	Iterator ite = unitplot2.entrySet().iterator();

	while(ite.hasNext()){
		Map.Entry entry = (Map.Entry)ite.next();
		String svType = (String)entry.getKey();
		ArrayList<Gene> genes = (ArrayList<Gene>)entry.getValue();
		println(svType+":"+genes.size()+" genes");
	}



	//UnitPlot2 coordinates ------------------------------
	//per sv type
	int running_x = chr_x;
	int running_y = label_y;
	unit_plot_label_y = new int[unitplot2.size()];
	unitplot2_labels = new String[unitplot2.size()];
	int currentEventCounter = 0;
	// unit_plot_label_y[0] = running_y; //set the first one
	for(int i = 0; i < _UNIT_PLOT_TYPES.length; i++){
		String type = _UNIT_PLOT_TYPES[i];
		//check if this type is in the table
		ArrayList<Gene> geneArray = (ArrayList<Gene>)unitplot2.get(type);
		if(geneArray == null){
			//no gene of this event type
			println("-- no match:"+type);
		}else{
			println("type:"+type);
			running_x = chr_x;
			unit_plot_label_y[currentEventCounter] = running_y;
			unitplot2_labels[currentEventCounter] = type; 
			//iterate though the array
			for(int j = 0; j<geneArray.size(); j++){
				Gene gene = geneArray.get(j);
				//assign position to all bubbles
				ArrayList<Bubble> bubbles = gene.bubbles;

				for(Bubble b: bubbles){
					b.newPosition = new PVector(running_x, running_y);
				}
				//check the current running_x
				running_x += _bubble_d;
				if(running_x >= _bubble_left_end){
					//go to next line
					running_x = chr_x;
					running_y += _bubble_d;
				}
			}
			//next type
			running_y += _bubble_d +_bubble_gap;
			currentEventCounter ++;
		}
	}
	//mixed one
	ArrayList<Gene> geneArray = (ArrayList<Gene>)unitplot2.get("multi_events");
	if(geneArray == null){
		//no gene of this event type
	}else{
		running_x = chr_x;
		unit_plot_label_y[currentEventCounter] = running_y;
		unitplot2_labels[currentEventCounter] = "multi_events"; 
		//iterate though the array
		for(int j = 0; j<geneArray.size(); j++){
			Gene gene = geneArray.get(j);
			//assign position to all bubbles
			ArrayList<Bubble> bubbles = gene.bubbles;

			for(Bubble b: bubbles){
				b.newPosition = new PVector(running_x, running_y);
			}
			//check the current running_x
			running_x += _bubble_d;
			if(running_x >= _bubble_left_end){
				//go to next line
				running_x = chr_x;
				running_y += _bubble_d;
			}
		}
		//next type
		running_y += _bubble_d +_bubble_gap;
	}
}

void assignGeneExpandedViewPositions(){
	int running_x = chr_x;
	int running_y = label_y;
	label_y_pos_expanded = new int[_CHROMOSOMES.length];
	for(int i = 0; i < data.geneGroupArrays.length; i++){
		//per chromosome
		label_y_pos_expanded[i] = running_y;
		running_x = chr_x;
		ArrayList<GeneGroup> geneGroups = data.geneGroupArrays[i];
		GeneGroup previousUnaffected = null;// check if the previous is unaffected
		for(int j = 0; j<geneGroups.size(); j++){
			GeneGroup gg = geneGroups.get(j);		
			//check status
			if(gg.status != null){
				if(checkValidGeneGroup(gg.status)){
					int startOfExpanded = running_x;
					for(int k = 0; k < gg.genes.size(); k++){
						Gene gene = gg.genes.get(k);
						ArrayList<Bubble> bubbles = gene.bubbles;
						for(int m = 0; m <bubbles.size(); m++){
							Bubble b = bubbles.get(m);
							b.newPosition = new PVector(running_x, running_y);
						//check the current running_x
						}
						//increment per bubble
						running_x += _bubble_d;
						if(running_x >= _bubble_left_end){
							//go to next line
							running_x = chr_x;
							running_y += _bubble_d;
						}
					}
					gg.rect = new Rectangle(startOfExpanded, running_y, (running_x- startOfExpanded), _bubble_d);
					previousUnaffected = null;
					gg.isShown = true;
				}else{
					gg.isShown = false;
				}

			}else{
				//unaffected genes
				if(previousUnaffected == null){
					Rectangle rect = new Rectangle(running_x, running_y, _connector, _bubble_d);
					gg.rect = rect;
					running_x += _connector; //shorter than full length
					previousUnaffected = gg;
				}else{
					//stack unaffected ones
					gg.rect = previousUnaffected.rect;
				}
			}
		}
		//next chromosome
		running_y += _bubble_d+_bubble_gap;
	}
}

boolean checkValidGeneGroup(String[] status){
	//check if at least one of events is valid/true
	boolean isValidGeneGroup  = false;
	for(int m = 0; m < _EVENTS.length; m++){
		if(_VALID_EVENT[m]){
			// if(m == 1){
			// 	println("***  inversion event:"+Arrays.toString(status));
			// }
			for(int k = 0; k < status.length; k++){
				if(status[k].startsWith(_EVENTS[m])){
					//valid
					isValidGeneGroup = true;

					return isValidGeneGroup;
				}
			}
		}
	}
	return isValidGeneGroup;
}



void draw(){
    if (saveFrame) {
        println("start of saving");
        beginRecord(PDF, "Plot-####.pdf");
    }    
    background(color_main_background);
    ellipseMode(CORNER);
    rectMode(CORNER);

    if(dataLoaded){
        if(animation.isPlaying()|| animation_geneView.isPlaying() || animation_legendView.isPlaying()){
            // println("anaimation playing:"+animation.getSeek() + " "+animation_geneView.getSeek()+" "+ animation_legendView.getSeek() );
            if(animation.getSeek() == 1.0f  && animation_geneView.getSeek() == 1.0f && animation_legendView.getSeek() == 1.0f){
                animation.pause();
                animation_geneView.pause();
                animation_legendView.pause();
                noLoop();
                // println("Stop looping");
            }
        }else if (animation.isEnded() ){//&& animation_geneView.isEnded() && animation_legendView.isEnded()){
            // println("animation ended");
            noLoop();
        }else{
            // noLoop();
            if(animation.getSeek() == 1.0f  && animation_geneView.getSeek() == 1.0f && animation_legendView.getSeek() == 1.0f){
                animation.pause();
                animation_geneView.pause();
                animation_legendView.pause();
                noLoop();
                // println("Stop looping");
            }
            // println("draw() else:"+animation.getSeek()+" "+animation_geneView.getSeek()+" "+ animation_legendView.getSeek());
        }

        drawBaseLine(); //draw baseline, un affected genes

        //highlight selected genes by go term
        for(int i = 0; i < _selected_genes_by_go.size(); i++){
            Gene gene = _selected_genes_by_go.get(i);
            if(gene.bubbles != null){
                for(int k = 0; k < gene.bubbles.size(); k++){
                    Bubble b = gene.bubbles.get(k);
                    if(_VALID_EVENT[b.event.svIndex]){
	                    b.highlight_soft();
	                }
                } 
            }
        }


        //draw bubbles
        for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
            GeneGroup gg = data.affectedGeneGroup.get(i);
            if(gg.isShown){
                if(_VIEW_MODE == 0){ 
                    //draw one overall background
                    gg.genes.get(0).bubbles.get(0).drawBackground();
                }
                //draw individual bubbles
                for(int j = 0; j< gg.genes.size(); j++){
                    Gene gene = gg.genes.get(j);
                    gene.isSelectable = false;
                    //0= GeneBlock, 1=UnitPlot, 2=BpView, 3 =Expanded
                    for(int k = 0; k < gene.bubbles.size(); k++){
                        Bubble b = gene.bubbles.get(k);
                        if(_VALID_EVENT[b.event.svIndex]){
                        	//valid event
                        	gene.isSelectable = true;

                            if(_VIEW_MODE == 0){
                                // if(animation.isPlaying()){
                                //     // b.drawBackground();
                                // }
                            }else if(_VIEW_MODE == 1){
                                if(animation.isPlaying()){
                                    //transition to unit plot
                                    b.drawBackground();
                                }else{
                                    if(k == 0){
                                        b.drawBackground();
                                    }
                                }
                            }else if(_VIEW_MODE == 2){
                                if(animation.isPlaying()){
                                    //transition to BP
                                }else{
                                }
                            }else if(_VIEW_MODE == 3){
                                //expanded view
                                if(k== 0){
                                    b.drawBackground();
                                }
                            }
                            //drawing bubble
                            b.render();
                        }
                    }

                }    
            }else{
            	for(int j = 0; j< gg.genes.size(); j++){
            	    Gene gene = gg.genes.get(j);
            	    gene.isSelectable = false;
            	    //0= GeneBlock, 1=UnitPlot, 2=BpView, 3 =Expanded
            	    for(int k = 0; k < gene.bubbles.size(); k++){
            	        Bubble b = gene.bubbles.get(k);
            	        if(_VALID_EVENT[b.event.svIndex]){
            	        	//valid event
            	        	gene.isSelectable = true;
            	        }
            	    }
            	}

            }
             //end of if
        }

        //draw multievents
        // println("debug_____drawMultievents!!!");
        if(_show_multi_event){
	        for(int i  = 0; i < data.affectedGeneGroup.size(); i++){
	            GeneGroup gg = data.affectedGeneGroup.get(i);
	            if(gg.isShown){
	                for(int j = 0; j< gg.genes.size(); j++){
	                    Gene gene = gg.genes.get(j);
	                    if(_VIEW_MODE == 0){
	                        //GeneBlock
	                        if(gene.isMultiEvent){
	                        	gene.renderMultiEvent();
	                        }
	                    }else if(_VIEW_MODE == 1){
	                    	//UnitPlot
	                        if(gene.isMultiEvent){
	                        	gene.renderMultiEvent_all();
	                        }
	                    }else if(_VIEW_MODE == 2){
	                    	//BpView
	                        if(animation.isPlaying()){
	                        }else{
	                        }
	                    }else if(_VIEW_MODE == 3){
	                        //expanded view
	                        if(gene.isMultiEvent){
	                        	gene.renderMultiEvent();
	                        }
	                    }       
	                }    
	            }
	        }
        }

        if(hasOptionalTable && selected_genes_by_category != null){
            //highlight selected genes by category
            for(int i = 0; i < selected_genes_by_category.size(); i++){
                Gene gene = selected_genes_by_category.get(i);
                if(gene.bubbles != null){
                    for(int k = 0; k < gene.bubbles.size(); k++){
                        Bubble b = gene.bubbles.get(k);
                        if(_VALID_EVENT[b.event.svIndex]){
                            b.highlight_by_category();
                        }
                    }
                    
                }
            }
        }

        // //highlight selected genes by go term
        // for(int i = 0; i < _selected_genes_by_go.size(); i++){
        //     Gene gene = _selected_genes_by_go.get(i);
        //     if(gene.bubbles != null){
        //         for(int k = 0; k < gene.bubbles.size(); k++){
        //             Bubble b = gene.bubbles.get(k);
        //             b.highlight_soft();
        //         } 
        //     }
        // }

        //highlight selected genes
        for(int i = 0; i < _selected_genes.size(); i++){
            Gene gene = _selected_genes.get(i);
            if(gene.bubbles != null){
                for(int k = 0; k < gene.bubbles.size(); k++){
                    Bubble b = gene.bubbles.get(k);
                    // if(_VALID_EVENT[b.event.svIndex]){
                    	b.highlight();
                	// }
                }
                
            }
        }
        drawLabels(); //draw Labels
        drawTitle();

        drawViewModeBtns();

        if (saveFrame) {
            drawLegendViewPDF();
            drawGeneViewdPDF();
        }else{
            //show legend view
            image(legendView, legendView_x, legendView_y); //when the data is loaded
            //show gene view
            image(geneView, 0, geneView_y);
        }

    }else{ //choosing file, start up page
        if(isLoadingData){
            drawLoadingPage();
        }else{
            drawStartUpPage();
        }
    }

    if (saveFrame) {
        // this.control.draw();
        endRecord();
        saveFrame = false;
        println("end of saving");
    }
}

//how each bubble is drawn
void _drawCircle(float x1,float  y1, int w, int h){
	ellipseMode(CORNER);
    ellipse(x1, y1, w, h);
}
void _drawHalfCircle(float x1, float y1, int w, int h, boolean isLeft){
	float ang1 = isLeft ? PI / 2 : 3 * PI / 2;
    float ang2 = isLeft ? 3 * PI / 2 : 5 * PI / 2;
    arc(x1, y1, w, h, ang1, ang2);
}
void _drawMiddleOfCircle(float x1, float y1, int w, int h){
	float x2 = x1 +w;
	float y2 = y1 +h;
	float gx = (x1 + x2) / 2;
    float gy = (y1 + y2) / 2;
    float r = _bubble_d / 2;
    float mr = 1f;//mid line thickness
    float d = (r * 0.55f);
    float d2 = d;

    beginShape();
    vertex(gx, gy - r);
    bezierVertex(gx + mr, gy - r, gx + mr, gy - r + d - d2, gx + mr, gy - r + d);
    vertex(gx + mr, gy + r - d);
    bezierVertex(gx + mr, gy + r - d + d2, gx + mr, gy + r, gx, gy + r);
    bezierVertex(gx - mr, gy + r, gx - mr, gy + r - d + d2, gx - mr, gy + r - d);
    vertex(gx - mr, gy - r + d);
    bezierVertex(gx - mr, gy - r + d - d2, gx - mr, gy - r, gx, gy - r);
    endShape();
}
void _drawMultiEvent(float x1, float y1, int w, int h){
	float ang1 = 3*PI/2;
	float ang2 = TWO_PI;
	float ang3 = PI/2;
	float ang4 = PI;
	arc(x1, y1, w, h, ang1, ang2);
	arc(x1, y1, w, h, ang3, ang4);

}
//draw base lines
void drawBaseLine(){
	noFill();
	strokeWeight(1);
	stroke(color_gray);
	strokeCap(SQUARE);
	if(_VIEW_MODE == 0){
		//GeneBlock view
		//draw unaffected GeneGroups
		for (int i = 0; i<data.unaffectedGeneGroup.size(); i++){
			GeneGroup gg = data.unaffectedGeneGroup.get(i);
			Rectangle rect = gg.rect;
			float mid_y = rect.y + (rect.height/2);
			line(rect.x, mid_y, rect.x+rect.width, mid_y);
		}

	}else if(_VIEW_MODE == 1){
		//UnitPlot
	}else if(_VIEW_MODE == 2){
		//bpView
		for (int i = 0; i<chromosomeDisplaySpace.length; i++){
			Rectangle rect = chromosomeDisplaySpace[i];
			float mid_y = rect.y + (rect.height/2);
			line(rect.x, mid_y, rect.x+rect.width, mid_y);
		}
	}else if(_VIEW_MODE == 3){
		//GeneBlock Expandedview
		//draw unaffected GeneGroups
		for (int i = 0; i<data.unaffectedGeneGroup.size(); i++){
			GeneGroup gg = data.unaffectedGeneGroup.get(i);
			Rectangle rect = gg.rect;
			float mid_y = rect.y + (rect.height/2);
			line(rect.x, mid_y, rect.x+rect.width, mid_y);
		}
	}
}

//draw labels
void drawLabels(){
	textAlign(RIGHT, TOP);
	textSize(10);
	fill(color_dark_gray);
	if(_VIEW_MODE == 0){
		//GeneBlock view
		//chromosome label
		for (int i = 0; i<_CHROMOSOMES.length; i++){
			text(_CHROMOSOMES[i], chr_x - _margin, label_y_pos[i]);
		}
	}else if(_VIEW_MODE == 1){
		//UnitPlot
		//sv types
		for(int i = 0; i< _EVENTS.length; i++){
			String label = _EVENTS[i];
			label = label.replace("_", "\n");
			textLeading(8);
			text(label, chr_x - _margin, unit_plot_label_y[i]);
		}
	}else if(_VIEW_MODE == 2){
		//bpView
		//chromosome label
		for (int i = 0; i<_CHROMOSOMES.length; i++){
			text(_CHROMOSOMES[i], chr_x - _margin, label_y_pos[i]);
		}
	}else if(_VIEW_MODE == 3){
		//GeneBlock expanded view
		//chromosome label
		for (int i = 0; i<_CHROMOSOMES.length; i++){
			text(_CHROMOSOMES[i], chr_x - _margin, label_y_pos_expanded[i]);
		}
	}else if(_VIEW_MODE == 4){
		// //unitplot2
		// println("labels "+ unitplot2_labels.length+" :"+Arrays.toString(unitplot2_labels));
		// println("ypos "+unit_plot_label_y.length+" : "+Arrays.toString(unit_plot_label_y));
		String currentType = "test";
		for(int i = 0; i<unit_plot_label_y.length; i++){
			String label = unitplot2_labels[i];
			if(label.startsWith(currentType)){
				//do not draw
			}else{
				String[] words = label.split("_");
				currentType = words[0];
				if(currentType.startsWith("tandem")){
					currentType = "tandem_duplication";
				}else if(currentType.startsWith("copy")){
					currentType = words[0]+"_"+words[1]+"_"+words[2];
				}
				label = currentType.replace("_", "\n");
				textLeading(8);
				text(label, chr_x- _margin, unit_plot_label_y[i]);
			}

			// label = label.replace("_", "\n");
			// text(label, chr_x- _margin, unit_plot_label_y[i]);
		}
	}
}

void drawTitle(){
	textAlign(LEFT, BOTTOM);
	textSize(15);
	fill(color_dark_gray);
	text(_DATA_FILE.getName(), title_x, title_y);
	view_mode_x = (int)textWidth(_DATA_FILE.getName()) +title_x +_margin;   //gap betwen file name and buttons
}


void drawViewModeBtns(){
	textSize(12);
	//setup rectangle
	if(_VIEW_MODE_RECT == null){
		_VIEW_MODE_RECT = new Rectangle[_VIEW_MODE_LABEL.length];
		float running_x = view_mode_x;
		for(int i = 0; i<_VIEW_MODE_RECT.length; i++){
			String label = _VIEW_MODE_LABEL[i];
			int w = round(textWidth(label));
			_VIEW_MODE_RECT[i] = new Rectangle(round(running_x), view_mode_y-_margin*2, w, _margin*2);
			running_x += w +_margin;
		}
	}
	//draw buttons
	textAlign(LEFT, BOTTOM);
	for(int i = 0; i<_VIEW_MODE_RECT.length; i++){
		String label = _VIEW_MODE_LABEL[i];
		Rectangle r = _VIEW_MODE_RECT[i];
		if(_VIEW_MODE == 0 && i == 0){
			fill(color_pink);
		}else if(_VIEW_MODE == 1 && i == 3){
			fill(color_pink);
		}else if(_VIEW_MODE == 2 && i == 2){
			fill(color_pink);
		}else if(_VIEW_MODE == 3 && i == 1){
			fill(color_pink);
		}else{
			fill(color_gray);
		}
		text(label, r.x, r.y+r.height);
	}
}
