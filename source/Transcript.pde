class Transcript{
	String kgID;
	int crs; //coding region start
    int cre; //coding region end
    int tsp;
    int tep;
    int exonNum; //number of exons
    int[] exonStart;
    int[] exonEnd;

    Transcript(String id, int t_start, int t_end, int start, int end, int count, int[]starts, int[] ends){
    	kgID = id;
    	crs = start;
    	cre = end;
    	exonNum = count;
    	exonStart = starts;
    	exonEnd = ends;
        tsp = t_start;
        tep = t_end;
    }
}
