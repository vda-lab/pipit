class Span {
    int start, end;
    ArrayList<Span> cuts;
    
    Span(int leftSide, int rightSide) {
        start = leftSide;
        end = rightSide;
    }
    
    //cut this span by cutSpan
    public ArrayList<Span> cut(Span cutSpan){
        ArrayList<Span> spans= new ArrayList<Span>();
        int cutStart = cutSpan.getStart();
        int cutEnd = cutSpan.getEnd();
        
        
        if(cutStart < start){
            if(cutEnd < start){
                // no overlap
                Span newSpan = new Span(start, end);
                spans.add(newSpan);
            }else if(cutEnd <= end){
                //left cut
                Span newSpan = new Span(cutEnd+1, end);
                spans.add(newSpan);
            }else if(cutEnd > end){
                //whole gene
            }else{
                System.out.println("Debug: cut() error");
            }
        }else if(cutStart <= end){
            if(cutEnd >= end){
                //right cut
                Span newSpan = new Span(start, cutStart-1);
                spans.add(newSpan);
            }else if(cutEnd < end){
                //mid cut
                Span newSpanLeft = new Span(start, cutStart-1);
                Span newSpanRight = new Span(cutEnd+1, end);
                spans.add(newSpanLeft);
                spans.add(newSpanRight);
            }else{
                System.out.println("Dubuyg: cut() error2");
            }
        }else if(cutStart > end){
            //no overlap
            Span newSpan = new Span(start, end);
            spans.add(newSpan);
        }else{
            System.out.println("Debug: cut() error 3:");
        }
        return spans;
    }
    
    int getStart(){
        return this.start;
    }
    int getEnd(){
        return this.end;
    }


    void addCutSpan(Span span) {
        if(cuts == null){
            cuts = new ArrayList<Span>();
        }
        cuts.add(span);
    }
    
    //checks if the entire span is affected.  affected = true, not affected = false
    boolean getCutResult() {
        //cut the original range and see
        Span temp = new Span(this.start, this.end);
        ArrayList<Span> result = new ArrayList<Span>();
        result.add(temp);
        //iterate through cuts
        for(int  i= 0; i<cuts.size(); i++){
            Span cut = cuts.get(i);
            result = cut.applyToArray(result);
        }
        
        //analyzse result
        if(result.isEmpty()){
            //System.out.println("----Completely deleted.");
            return true;
        }else{
            for(int i = 0; i< result.size(); i++){
                Span s = result.get(i);
               // System.out.println("----Span.getCutResult"+i+":"+s.toString());
            }
            return false;
            
        }
    }

    //called from cut span, update Spans in the array
    private ArrayList<Span> applyToArray(ArrayList<Span> array) {
        ArrayList<Span> result = new ArrayList<Span>();
        for(int i = 0; i< array.size(); i++){
            Span toBeCut = array.get(i);
            ArrayList<Span> afterCut = toBeCut.cut(this);
            if(afterCut != null || !afterCut.isEmpty()){
                result.addAll(afterCut);
            }
        }
        return result;
    }
    
    public String toString(){
        return this.start+"-"+this.end+"("+(end - start)+")";
    }
    
}
