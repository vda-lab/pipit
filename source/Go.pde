class Go implements Comparable{
	String name;
	ArrayList<Gene> genes;
	boolean isSuperclass = false;


	Go(String n){
		name = n;
		genes = new ArrayList<Gene>();
	}

	void addGene(Gene g){
		if(genes == null){
			genes = new ArrayList<Gene>();
		}
		genes.add(g);
	}

	String toString(){
		if(genes != null){
			return name+"("+genes.size()+")";
		}else{
			return name+"(1)";		
		}
	}

	public int compareTo(Object obj){
		Go go2 = (Go) obj;
		String goName1 = name.toUpperCase();
		String goName2 = go2.name.toUpperCase();
		return goName1.compareTo(goName2);
	}
}
