class Gene implements Comparable{
    String geneSymbol;
    String ensemblID;
    int chrIndex;
    String chromosome;
    boolean strand;  // + true,  - false
    int tsp; //transcription start position
    int tep; //transcription end position
    boolean isCoding; //coding or non-coding
    boolean isHit = false; //false by default
    ArrayList<Event> affectingEvents = null; 
    ArrayList<String> eventConsequence = null;
    ArrayList<Transcript> transcripts = null;
    ArrayList<Bubble> bubbles = null;
    Transcript transcript;

    String[] rawData;
    HashSet<String> status;

    int start, end;

    //multievent
    boolean isMultiEvent = false;

    //whether if they can be selected
    boolean isSelectable = true;

    ArrayList<Go> goTerms;

    //constructor
    Gene(String[] s){
        ensemblID = s[0].trim();
        geneSymbol = s[1].trim();
        chromosome = s[2].toLowerCase().trim();
        chrIndex = _getChromosomeIndex(chromosome);
        strand = (s[3].trim().equals("+") ? true : false);
        tsp = Integer.parseInt(s[4]);
        tep = Integer.parseInt(s[5]);

        //transcript information 
        int crs; //coding region start
        int cre; //coding region end
        int exonNum; //number of exons
        int[] exonStart;
        int[] exonEnd;
        crs = Integer.parseInt(s[6]);
        cre = Integer.parseInt(s[7]);
        exonNum = Integer.parseInt(s[8]);
        exonStart = parseExons(s[9]);
        exonEnd = parseExons(s[10]);
        transcript = new Transcript(ensemblID, tsp, tep, crs, cre, exonNum, exonStart, exonEnd);
        isCoding = (crs == cre ? false : true);

        start = tsp;
        end = tep;
    }

    void addTranscript(Gene g){
        if(transcripts == null){
            //first time
            transcripts = new ArrayList<Transcript>();
            //add the first transcript
            transcripts.add(transcript);
        }
        transcripts.add(g.transcript);
        start = min(start, g.start);
        end = max(end, g.end);
    }

    int[] parseExons(String s){
    	//splitTokens ignores the last comma
    	int[] result = int(splitTokens(s, ","));
    	return result;
    }

    public int compareTo(Object obj) {
        Gene e = (Gene) obj;
        //compare chromosome first
        int e_chr = e.chrIndex;
        if(this.chrIndex < e_chr){
            return -1;
        }else if(this.chrIndex > e_chr){
            return 1;
        }else{
            //if they are the same chromosome
            if (this.tsp < e.tsp ){
                return -1;
            } else if (this.tsp > e.tsp) {
                return 1;
            }
            return 0;
        }
    }

    //record events that affect this gene
    void addEvent(Event e, String con) {
        if (affectingEvents == null) {
            affectingEvents = new ArrayList<Event>();
            eventConsequence = new ArrayList<String>();
        }
        affectingEvents.add(e);
        eventConsequence.add(con);
    }

    //check if there is an exon within this range
    boolean containsExons(int start, int end) {
        boolean hasExons = false;
        //first check against the gene
        for (int i = 0; i < transcript.exonStart.length; i++) {
            int testStart = transcript.exonStart[i];
            int testEnd = transcript.exonEnd[i];
            //check
            if (testEnd < start) {
                //below range
                continue;
            }
            if (testStart > end) {
                //above range
                break;
            }
            //if it gets to point, there is over lap 
            return true;
        }
        //if more than one transcipts
        if(transcripts != null){
            for(int j = 1; j< transcripts.size(); j++){
                Transcript t = transcripts.get(j);
                for (int i = 0; i < t.exonStart.length; i++) {
                    int testStart = t.exonStart[i];
                    int testEnd = t.exonEnd[i];
                    //check
                    if (testEnd < start) {
                        //below range
                        continue;
                    }
                    if (testStart > end) {
                        //above range
                        break;
                    }
                    //if it gets to point, there is over lap 
                    return true;
                }            
            }
        }
        return hasExons;
    }
    //return the status of the gene
    String[] getStatus(){
    	if (affectingEvents == null) {
    		//no event
            return null;
        }else{
            if(status == null){
            	//only done once
                setStatus();
            }
            if (status.isEmpty()) {
                //affecting events are not encoded yet, so the status is empty
                return null;
            } else {
                //compare against flags from radio buttons
                ArrayList<String> resultArray = new ArrayList<String>();
                Iterator ite = status.iterator();
                while(ite.hasNext()){
                    String s = (String) ite.next();
                    if(_isValidSvEvent(s)){
                        resultArray.add(s);
                    }
                }
                String[] result = resultArray.toArray(new String[resultArray.size()]);
                return result;
            }
        }
    }

    //check per sv events for all affecting events
    void setStatus(){
        status = new HashSet<String>();
    	for (int i = 0; i<_EVENTS.length; i++){
    		boolean isLeft = false;
    		boolean isRight = false;
    		boolean isMiddle = false;
    		boolean isAll = false;

    		//iterate though affectingEvents
    		for(int j = 0; j<affectingEvents.size(); j++){
    			Event e = affectingEvents.get(j);
    			if(e.svIndex == i){
                    //same event
                    String conseq = eventConsequence.get(j);
                    if (conseq.equals("all")) {
	                    isAll = true;
	                } else if (conseq.equals("left")) {
	                    isLeft = true;
	                } else if (conseq.equals("right")) {
	                    isRight = true;
	                } else if (conseq.equals("middle")) {
	                    isMiddle = true;
	                }
    			}
    		}
    		//check flags and decide on its overall effect
    		if (isAll) {
	            //whole gene is affected
	            status.add(_EVENTS[i]+"_all");
	        } else if (isLeft && !isRight) {
	            //just left
	            status.add(_EVENTS[i]+"_left");
	        } else if (!isLeft && isRight) {
	            //just right
	            status.add(_EVENTS[i]+"_right");
	        } else if (!isLeft && !isRight && isMiddle) {
                //just middle
                status.add(_EVENTS[i]+"_middle");
	        } else if (isLeft && isRight) {
	            //check if there is a gap inbetween
	            int leftSide = tsp;
	            int rightSide = tep;
	            if (this.strand) {
	                leftSide = max(0, leftSide - _PROMOTER_LENGTH);
	            } else {
	                rightSide = rightSide+ _PROMOTER_LENGTH;
	            }

	            Span range = new Span(leftSide, rightSide);
	            //record the span of affecting events of sv type of the interest
	            for (int j = 0; j < this.affectingEvents.size(); j++) {
	                Event e = affectingEvents.get(j);
	                if (e.svIndex == i) {
	                    range.addCutSpan(new Span(e.start, e.end));
	                }
	            }
	            //check the result
	            boolean isAllDeleted = range.getCutResult();
	            if (isAllDeleted) {
	                status.add(_EVENTS[i]+"_all");
	            } else {
	                status.add(_EVENTS[i]+"_both_sides");
                    println("Debug:Gene: _both_sides deleted "+geneSymbol);
	            }
	        }

            // // // //inversion
            // if(i == 1){
            //     println("--inversion: all:"+isAll+"\tleft:"+isLeft+"\tright:"+isRight+"\tmiddle:"+isMiddle);
            // }
    	}
    }


    void renderMultiEvent(){
        //get the first bubble
        Bubble b = bubbles.get(0);
        if(_VALID_EVENT[b.event.svIndex]){
            b.renderMultiEvent();
        }
    }
    // for unit plot
    void renderMultiEvent_all(){
        for(Bubble b: bubbles){
            if(_VALID_EVENT[b.event.svIndex]){
                b.renderMultiEvent();
            }
        }
    }

    void addGoTerm(Go go){
        if(goTerms == null){
            goTerms = new ArrayList<Go>();
        }
        goTerms.add(go);
    }

}
