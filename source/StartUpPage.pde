String[] modelOrganisms = {"Homo sapiens", "Mus musculus"};
String[] human_build = {"NCBI build 36", "NCBI build 37"};
String[] mouse_build = {"NCBI build 37/mm9", "GRCm build 38/mm10"};

boolean isHuman = true;
boolean isFirstBuild = true;
boolean coordinateIsSet = false;

//optinal build
boolean isUserDefinedBuild = false;

File selectedGVF = null;
File selectedOptional = null;


//coordinates
int logo_x = _margin*10;
int logo_y = _margin*10;
int logo_w = 160;
int logo_h = 100;

int model_organism_text_x = logo_x +logo_w+_margin*5 ;
int model_organism_text_y =  _margin*12;
int model_btn_w = _margin*15;
int model_btn_h = _margin*2;
Rectangle model_one_rect;// = new Rectangle(model_organism_text_x, model_organism_text_y+model_organism_text_h, model_btn_w, model_btn_h);
Rectangle model_two_rect;// = new Rectangle
Rectangle build_one_rect, build_two_rect;
Rectangle file_name_rect, file_btn;
Rectangle optional_name_rect, optional_btn;
int file_textbox_w = _margin*30;
int file_btn_w = _margin*10;

PShape logo;
Rectangle load_btn;

Rectangle promoter_length_text_box;
String promoter_input = "0";
boolean promoter_input_active = false;


//user defined build
Rectangle userDefined_rect;
Rectangle ud_genetrack_rect, ud_cytoband_rect, ud_go_rect;



void drawStartUpPage(){
	if(!coordinateIsSet){
		int runningX = model_organism_text_x;
		int runningY = model_organism_text_y;
		model_one_rect = new Rectangle(runningX,runningY, model_btn_w, model_btn_h);
		runningX += model_btn_w+_margin;
		model_two_rect = new Rectangle(runningX,runningY, model_btn_w, model_btn_h);
		runningX += model_btn_w+_margin;
		userDefined_rect = new Rectangle(runningX,runningY, file_btn_w, model_btn_h);
		
		//hidden optinal area
		int temp_y = runningY;
		int temp_x = runningX += file_btn_w +_margin*3;
		ud_genetrack_rect = new Rectangle(temp_x, temp_y, _margin*30, model_btn_h);
		temp_y += model_btn_h + _margin*2;
		ud_cytoband_rect = new Rectangle(temp_x, temp_y, _margin*30, model_btn_h);
		temp_y += model_btn_h + _margin*2;
		ud_go_rect = new Rectangle(temp_x, temp_y, _margin*30, model_btn_h);

		//genome build btns
		runningX = model_organism_text_x;
		runningY += model_btn_h+_margin*5;
		build_one_rect = new Rectangle(runningX,runningY, model_btn_w, model_btn_h);
		runningX += model_btn_w+_margin;
		build_two_rect = new Rectangle(runningX,runningY, model_btn_w, model_btn_h);
		//file
		runningX = model_organism_text_x;
		runningY += model_btn_h+_margin*5;
		file_name_rect = new Rectangle(runningX,runningY, file_textbox_w, model_btn_h);
		runningX += file_textbox_w+_margin;
		file_btn = new Rectangle(runningX,runningY, file_btn_w, model_btn_h);
		//optional csv
		runningX = model_organism_text_x;
		runningY += model_btn_h+_margin*5;
		optional_name_rect = new Rectangle(runningX, runningY, file_textbox_w, model_btn_h);
		runningX += file_textbox_w+_margin;
		optional_btn = new Rectangle(runningX, runningY, file_btn_w, model_btn_h);
		//promoter length
		runningX = model_organism_text_x;
		runningY += model_btn_h+_margin*5;
		promoter_length_text_box = new Rectangle(runningX, runningY, model_btn_w, model_btn_h);
		//load
		runningX = model_organism_text_x;
		runningY += model_btn_h+_margin*5;
		load_btn = new Rectangle(runningX, runningY, model_btn_w, model_btn_h);
		//load logo
		logo = loadShape("logo.svg");

	}

	fill(60);
	textAlign(LEFT, BOTTOM);
	textSize(14);
	//model organism
	text("Select an organism:", model_one_rect.x, model_one_rect.y);
	//genome build
	text("Select a build:", build_one_rect.x, build_one_rect.y);
	//file
	text("Select a GVF file:", file_name_rect.x, file_name_rect.y);
	//optional
	text("Optional: Select a csv file:", optional_name_rect.x, optional_name_rect.y);
	//promoter
	text("Define a promoter length:", promoter_length_text_box.x, promoter_length_text_box.y);

	//draw buttons background
	rectMode(CORNER);
	fill(240);
	noStroke();
	rect(model_one_rect.x, model_one_rect.y, model_one_rect.width, model_one_rect.height);
	rect(model_two_rect.x, model_two_rect.y, model_two_rect.width, model_two_rect.height);
	rect(build_one_rect.x, build_one_rect.y, build_one_rect.width, build_one_rect.height);
	rect(build_two_rect.x, build_two_rect.y, build_two_rect.width, build_two_rect.height);
	rect(file_btn.x, file_btn.y, file_btn.width, file_btn.height);
	rect(optional_btn.x, optional_btn.y, optional_btn.width, optional_btn.height);
	rect(load_btn.x, load_btn.y, load_btn.width, load_btn.height);
	//user defined rect
	rect(userDefined_rect.x, userDefined_rect.y, userDefined_rect.width, userDefined_rect.height);
	noFill();
	stroke(200);
	rect(file_name_rect.x, file_name_rect.y, file_name_rect.width, file_name_rect.height);
	rect(optional_name_rect.x, optional_name_rect.y, optional_name_rect.width, optional_name_rect.height);
	//promoter
	rect(promoter_length_text_box.x, promoter_length_text_box.y, promoter_length_text_box.width, promoter_length_text_box.height);

	//draw btn text
	fill(120);
	noStroke();
	textAlign(CENTER, CENTER);
	textSize(12);
	if(isUserDefinedBuild){
		stroke(80);
		text(modelOrganisms[0], (float)model_one_rect.getCenterX(), (float)model_one_rect.getCenterY());
		text(modelOrganisms[1], (float)model_two_rect.getCenterX(), (float)model_two_rect.getCenterY());
		fill(color_pink);
		text("Other", (float)userDefined_rect.getCenterX(), (float) userDefined_rect.getCenterY());
	}else{
		if(isHuman){
			fill(color_pink); 
			text(modelOrganisms[0], (float)model_one_rect.getCenterX(), (float)model_one_rect.getCenterY());
			fill(80);
			text(modelOrganisms[1], (float)model_two_rect.getCenterX(), (float)model_two_rect.getCenterY());
			text("Other", (float)userDefined_rect.getCenterX(), (float) userDefined_rect.getCenterY());
		}else{
			fill(80);
			text(modelOrganisms[0], (float)model_one_rect.getCenterX(), (float)model_one_rect.getCenterY());
			fill(color_pink); 
			text(modelOrganisms[1], (float)model_two_rect.getCenterX(), (float)model_two_rect.getCenterY());
			fill(80);
			text("Other", (float)userDefined_rect.getCenterX(), (float) userDefined_rect.getCenterY());
		}

	}
	if(isUserDefinedBuild){
		//external file info
		stroke(200);
		noFill();
		rect(ud_genetrack_rect.x, ud_genetrack_rect.y, ud_genetrack_rect.width, ud_genetrack_rect.height);
		rect(ud_cytoband_rect.x, ud_cytoband_rect.y, ud_cytoband_rect.width, ud_cytoband_rect.height);
		rect(ud_go_rect.x, ud_go_rect.y, ud_go_rect.width, ud_go_rect.height);

		//box name
		fill(60);
		textAlign(LEFT, BOTTOM);
		textSize(14);
		text("Gene track:", ud_genetrack_rect.x, ud_genetrack_rect.y);
		text("Cytobands:", ud_cytoband_rect.x, ud_cytoband_rect.y);
		text("Go terms:", ud_go_rect.x, ud_go_rect.y);

		textSize(12);
		textAlign(CENTER, CENTER);

	}else{
		if(isHuman){
			fill((isFirstBuild? color_pink:80));
			text(human_build[0], (float)build_one_rect.getCenterX(), (float)build_one_rect.getCenterY());
			fill((!isFirstBuild? color_pink:80));
			text(human_build[1], (float)build_two_rect.getCenterX(), (float)build_two_rect.getCenterY());
		}else{
			fill((isFirstBuild? color_pink:80));
			text(mouse_build[0], (float)build_one_rect.getCenterX(), (float)build_one_rect.getCenterY());
			fill((!isFirstBuild? color_pink:80));
			text(mouse_build[1], (float)build_two_rect.getCenterX(), (float)build_two_rect.getCenterY());
		}
	}

	//load button text
	if(selectedGVF == null){
		fill(100);
	}else{
		fill(load_btn.contains(mouseX, mouseY)? color_pink:80);
	}
	text("start loading", (float)load_btn.getCenterX(), (float)load_btn.getCenterY());

	//select button
	fill( file_btn.contains(mouseX, mouseY)? color_pink:80);
	text("select", (float)file_btn.getCenterX(), (float)file_btn.getCenterY());

	//opitonal select buttion
	fill( optional_btn.contains(mouseX, mouseY)? color_pink:80);
	text("select", (float)optional_btn.getCenterX(), (float)optional_btn.getCenterY());

	//draw selected files path
	if(selectedGVF != null){
		fill(80);
		textAlign(LEFT, CENTER);
		text(" "+selectedGVF.getName(), file_name_rect.x, (float)file_name_rect.getCenterY());
	}
	//draw the selected optional file path
	if(selectedOptional != null){
		fill(80);
		textAlign(LEFT, CENTER);
		text(" "+selectedOptional.getName(), optional_name_rect.x, (float)optional_name_rect.getCenterY());
	}


	//logo
	shapeMode(CORNER);
	shape(logo, logo_x, logo_y, logo_w, logo_h);

	//promoter
	if(promoter_input_active){
		stroke(color_pink);
		strokeWeight(2);
		noFill();
		rect(promoter_length_text_box.x, promoter_length_text_box.y, promoter_length_text_box.width, promoter_length_text_box.height);
		strokeWeight(1);
		//
		fill(80);
		textAlign(RIGHT, CENTER);
		text(promoter_input +" ", promoter_length_text_box.x+promoter_length_text_box.width, (float)promoter_length_text_box.getCenterY());
	}else{
		fill(160);
		textAlign(RIGHT, CENTER);
		text(promoter_input+" ", promoter_length_text_box.x+promoter_length_text_box.width, (float)promoter_length_text_box.getCenterY());
	}
	

}




















