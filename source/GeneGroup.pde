class GeneGroup{
	ArrayList<Gene> genes;
	Rectangle rect, rect_expanded;
	String[] status;
	int startPos, endPos;
    boolean isSelected = false;
    boolean isShown = true;

    boolean isMultiEvent = false;

	GeneGroup(Gene g){
		genes  = new ArrayList<Gene>();
		genes.add(g);
		status = g.getStatus();
		startPos = g.tsp;
		endPos = g.tep;
	}

	//check it it has the same status
	boolean hasSameStatus(String[] s) {
        if (status == null || status.length == 0) { //current GeneDisplay is no hit
            if (s == null || s.length == 0) {
                //so as the next gene
                return true;
            } else {
                //new status
                return false;
            }
        } else {
            if (s == null) { 	//next gene is not hit
                return false;
            } else {
                if (status.length == s.length) {	//same length
                    for(int i = 0; i< status.length; i++){
                        String s1 = status[i];
                        boolean match = false;
                        for(int j = 0; j<s.length;j++){
                            if(s1.equals(s[j])){
                                match = true;
                            } 
                        }
                        if(!match){
                            return false;
                        }
                    }
                    return true;
                } else { 	//different length
                    String[] longArray, shortArray;
                    if(status.length > s.length){
                        longArray = status;
                        shortArray = s;
                    }else{
                        longArray = s;
                        shortArray = status;
                    }
                    for(int i = 0; i< longArray.length; i++){
                        String s1 = longArray[i];
                        boolean match = false;
                        for(int j = 0; j<shortArray.length;j++){
                            if(s1.equals(shortArray[j])){
                                match = true;
                            } 
                        } 
                        if(!match){
                            return false;
                        }
                    }
                    return true;  
                }
            }
        }
    }

    void addGene(Gene g){
    	genes.add(g);
    	startPos = min(startPos, g.tsp);
    	endPos = max(endPos, g.tep);
    }


    public String toString(){
    	String output = "GeneGroup:";
    	if(status == null){
    		output +=" null";
    		return output;
    	}else{
    		output += Arrays.toString(status);
    		output += "\t"+this.genes.get(0).geneSymbol;
	    	return output;
    	}
    }

    void saveRectangle(Rectangle rect){
        this.rect = rect;
    }

    boolean containsGene(String gSymbol){
        for(int i = 0; i<genes.size(); i++){
            Gene g = genes.get(i);
            if(g.geneSymbol.equals(gSymbol)){
                return true;
            }
        }
        return false;
    }
    Gene findGene(String gSymbol){
        for(int i = 0; i<genes.size(); i++){
            Gene g = genes.get(i);
            if(g.geneSymbol.equals(gSymbol)){
                return g;
            }
        }
        return null;
    }
	}
