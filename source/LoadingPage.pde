

int loading_msg_x = _WIDTH/2;
int loading_msg_y = _HEIGHT/2 - _margin*5;

int loading_bar_w  = 300;
int loading_bar_h = _margin;
int loading_bar_x  = loading_msg_x -(loading_bar_w/2);
int loading_bar_y = loading_msg_y +_margin*3;


void drawLoadingPage(){
	fill(80);
	textSize(14);
	textAlign(CENTER, BOTTOM);
	text(_current_load_message, loading_msg_x, loading_msg_y);


	//loading bar
	rectMode(CORNER);
	fill(80);
	rect(loading_bar_x, loading_bar_y, loading_bar_w, loading_bar_h);
    fill(160);
    float w = map(_current_data, 0, _total_data, 0, loading_bar_w);
    rect(loading_bar_x, loading_bar_y, w, loading_bar_h);
    textSize(14);
    textAlign(CENTER);



	//logo
	shapeMode(CORNER);
	shape(logo, logo_x, logo_y, logo_w, logo_h);

}
